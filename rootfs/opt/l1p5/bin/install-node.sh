#!/bin/bash

set -euxo pipefail

export NODE_MAJOR=16

echo "Install node.js and npm"

apt-get update
apt-get install -y ca-certificates curl gnupg curl
mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
apt-get update

apt install -t nodistro nodejs


