# 6.0.7

## GES1.5

- survey: fix statistics (it displyed a wrong number of answers) ([17db2](https://framagit.org/labos1point5/l1p5-vuejs/-/commit/17db2a4a525190c19b9104316035fcbf94e124bb))
- Fix CSRF issue ([#384](https://framagit.org/labos1point5/l1p5-vuejs/-/issues/384))

## Transition

- various fixes ([#390](https://framagit.org/labos1point5/l1p5-vuejs/-/issues/390),[391](https://framagit.org/labos1point5/l1p5-vuejs/-/issues/391),[#392](https://framagit.org/labos1point5/l1p5-vuejs/-/issues/391),[#393](https://framagit.org/labos1point5/l1p5-vuejs/-/issues/394),[#394](https://framagit.org/labos1point5/l1p5-vuejs/-/issues/394))


## DevOps

- Add a variable to configure the warning about non available Emission Factors: `L1P5_WARNING_YEARS` ([fd45e](https://framagit.org/labos1point5/l1p5-vuejs/-/commit/fd45ebd4beddef1943905f1c14d7efd09704f5df))
- Add the variable `L1P5_SIGNATURE` to configure the signature of Emails ([1cff6](https://framagit.org/labos1point5/l1p5-vuejs/-/commit/1cff6d7f2f762c2ce21657ede6e11a80e598e890))
- Add the variable `L1P5_EXTRA_ACTIVATE_EMAIL` to add specific context in emails (registration and reset). 
- Add scripts to extract data for `ges1point-data-analyze` ([efb20](https://framagit.org/labos1point5/l1p5-vuejs/-/commit/efb2041e5606495f9cf77bcfc70aadfd985af04f))
- Add `homefs/bin/switch.sh` script ([1f39f](https://framagit.org/labos1point5/l1p5-vuejs/-/commit/1f39fe85e509a0ef7ba3a97ce7f1f19288aa1128))
- Update quality-of-life for docker-compose dev deployment ([797e9](https://framagit.org/labos1point5/l1p5-vuejs/-/commit/797e995f527843f493816dc5872a98af9b4c76c8))
- Change `python manage.py init` script behaviour ([de304](https://framagit.org/labos1point5/l1p5-vuejs/-/commit/de3049ae000ab5cc9edd2f9eac658b6ac9914414))

# 6.0.6

- carbon: In surveys, fix the warning about extraneous answers received

## DevOps


- Add a dumps directory to record/version all the scripts to extract the data for use in the GDR

# 6.0.5

## GES1.5

- carbon: Fix campaign EFs. [changes](https://framagit.org/labos1point5/l1p5-vuejs/-/commit/1dce2d379a86d0717e50e693411b86d3d5f2255b)


## Admin

- Fix copy data for vehicles

## DevOps

- Add an option to enable/disable full app (`L1P5_ACTIVE_APPS`)

# 6.0.4

## GES1.5

- carbon: fix save functions when data contain tags (regression from 6.0.2)

## Ecolabware

Make it accessible from the main page.

## Maintenance:

- 

# 6.0.3

## GES1.5

- Add electricity factors for Arizona (US.AZ), Hawaii (US.HI) and Chile (CL) 
- Travels: imported file can now contain country names in English

## Ecolabware

Update Ecolabware

# 6.0.2

## GES1.5

- Superadmin: Fix email update form and filter
- New API endpoint: `/api/get_fake_ghgi` to returns a (fake)GHGI consistent with
the configuration (position labels, travel purposes ...).

## Maintenance

- `MyTable` has been renamed `MyModel` and the props have been changed (refer to the code)
- Playwright tests for Firefox have been removed

# 6.0.1

## GES1.5


- Boundary: Improve UI
- Commutes: Add cable car mean of transportation
- Purchases: When importing a file, the amount field values are cleaned before being converted to float
  e.g `12 345` is now accepted and will be parsed as `12345` (was parsed as `12` before)
- Devices: prevent edition when module is submitted
- Results: fix submission button
- Superadmin: Paginate Users (before the Users page was unusable)
- Superadmin: Fix export
 
 ## Transition1.5

- Introduction: Fix div overflow on the fresk.
- Reviewer: Fix message when publishing an action
- Reviewer: Actions to review table: change the default order used

## Configuration

- Introduce the `L1P5_ACTIVE_SCENARIO` to enable/disable some Scenario of Scenario1.5 
- add `CSRF_TRUSTED_ORIGINS` in default settings to avoid form submission errors 

## Maintenance

- Document how to run the pipeline localy using `gitlab-ci-local`
- Introduce backend `@modify_conf` in the backend to override the configuration on-the-fly (for unit tests)


The following changes have been made to facilitate the vue3 migration

- Factorize `vue2-editor` code in a single component
- Remove `lodash.clonedeep` dependency (redundant with `lodash`)
# 6.0.0

## GES1.5

- Augment Module definition with the corresponding vue entrypoint, javascript model to use, serde functions.
  As a side effect the setting `ACTIVE_MODULES` names have changed (see the DevOps notes).

- Configuration has been moved in the backend (see DevOps notes)

- Import:
  - Better error handling: the app now opens a modal with more detailed about import errors (missing columns, missing/extra values ...)
  - Purpose and Position accepted values are now consistent with the configuration
  - Make SIFAC doc consistent with MOP document

## DevOps notes

- Settings: `ACTIVE_MODULES` names changed.
  - In `core_settings` table, remove the `ACTIVE_MODULES` row.

- The application is now fully configured using a set of django settings
  - The configuration is a 2 steps process: first create a
  `backend/settings/l1p5/l1p5.py` file that contains all your configuration and
  then call `python manage.py init`
  - For the lab-fr instance call `python manage.py init` after migrating the
  code + DB and you can remove the unused settings keys: In `core_settings`
  table, remove the `NUMBER_OF_WEEKS.{default,2020,2021}` rows.

# 5.0.10

## Transition1.5:

- Add a new tag: `researchtopic`

## Misc:

- Fixed typos here and there
- 404 page is now translated to english

# 5.0.9

## GES1.5

- Purchases: fix ill-typing error (``amount`` is now coerced to a float)
- Export: Entity and boundary are now part of the exported file

## Misc

- Fixed typos here and there

# 5.0.8

## GES1.5

travels: Improve geslab format transportation modes
  now supports 'Transports en commun', 'Véhicule de location', 'Covoiturage', 'Véhicule de service'


# 5.0.7


## GES1.5

superadmin: Allows to filter admin ghgis on administration names
travels: relax constraints on delimiters in travel file formats (havas, geslab, sifac)
graph export: add Referrer policy
introduction: make it clear that the data aren't saved when using the anonymous mode

## Dev

Code maintenance:
- make the Collection agnostic to the underlying stored object
- Factorize code in the Model class

# 5.0.6


## Carbon, Scenario, Transition

- GES1.5|travels: parsing now supports 'Autocar - trajets intercités' and derivatives
- Transition1.5: Fix filter (wrong entity model was used)
- Transition1.5: Redirect to an error page when the user isn't authorized to view/review an action

## Code maintenance

- Factorize okToSubmit methods

# 5.0.5

## Ecolabware

Improve version

## Carbon, Scenario, Transition

- Transition1.5: Fix shortuts to actions

# 5.0.4

## Carbon, Scenario, Transition

- Transition1.5: Fix action creation issue (entity wasn't created properly)

# 5.0.3

## Ecolabware

- Add first beta version of the simulator


# 5.0.2

## Carbon, Scenario, Transition

- Transition1.5: Fix review page to support the entity model

## Development

- Fix research activities tests


# 5.0.1

## Carbon, Scenario, Transition

- GES1.5: vehicles's export trim tabulation
- GES1.5: we now export research activities
- Scenario1.5: fix fake GHGI


# 5.0.0

## Carbon, Scenario, Transition

- Define Boundary and Entity models to allow Apps 1point5 to be used outside of laboratories
- Reorganize and rewrite the documentation

## Transition

- Fix quota update display issue
- Add a quota per action (5MB) and a global quota per user (50MB)
- Allow LimeSurvey files to be attached to an action (.lss extension) (see DevOps note)

## Development

- Fix CI on tags


## DevOps notes


- In `core_settings` table, remove the `ACCEPTED_MIME_TYPES` row. 
- ⚠️ Upgrade Django to 4.2 (LTS)

# 4.4.13

## Carbon

- Buildings: Improve parsing (more robust regexp, avoid mismatch)
- Purchases: id
- Vehicles: id


# 4.4.12

## Carbon

- Travels: Fix Havas parser
- Travels: Add SIFAC help

## Development

- Travels: Playwright: increase coverage (add test for surveys, tags, research activities)

# 4.4.11


## Carbon

- Travel: update Havas parser format

# 4.4.10

## Carbon

- Food: fix a bug when adding in anonymous mode
- Travels: allow to retrieve geoname results based on locale
- Speed up the function that retrieve the ghgi consumption data (`get_ghgi_consumptions`)

# 4.4.9

## Carbon

- Buildings: add a pie chart
- Synthesis: add a treemap chart
- Purchases: Change some of the NACRES attribution
  - ba03 to buildings
  - ka12 to ractivities
  - ka13 to ractivities
  - ka14 to ractivities
  - ka15 to ractivities
  - ka21 to ractivities

Fix Charter

# 4.4.8

- Superadmin: allow to copy data from ui

# 4.4.7

- Core: fix bug when comparing tag
- Travels: Reorganize import format

# 4.4.6

DevOps: install logrotate

# 4.4.5

## Carbon

- Travels: Fix a bug with sortSections when called on a Travel coming from the database (and with many sections)
- Travels: Fix Havas import
- Synthesis: Fix synthesis batch calculation

# 4.4.4

## Carbon

- Commutes: Fix a bug in which the survey wasn't displaying the tags when the user was unauthenticated


## Transition

- Reviewer: Fix a bug that prevent reminder to be displayed

# 4.4.3

## Carbon

- Commutes: Fix bug when exporting with deleted answers
- Travels: Fix missins column error message display, Handle HAVAS file format
- Buildings: Buildings with electric, heating or water consumption to 0
are considered as incomplet and no longer invalid. 
- Synthesis: Fix reactivity (switching languages reset all the intensities)

## CI

- pre-commit all the files (except vue)

## Misc

- Django: mgmt command to create tags for a lab

# 4.4.2

## Carbon

- GHGI comparisons: add a waiting spinner (loading all the GHGIs can be slow)
- Fix public link loading
- Buildings: Fix construction year not loaded when building already in DB


# 4.4.1

## Carbon

- Devices: fix import error

## Transition

- Better handle the case where the attached file aren't found in the server filesystem (e.g opening the files lead to a 404)
- Review: Better highlight passed reminder in the reminder table

# 4.4.0

## Carbon

- Allows users to define and tag data imported in GES 1point5. This permits users to obtain differentiated GHGI by geographical locations, administrations, teams, and so on...
- Remove "Site" field from building object to replace it by a tag.
- Allows users to remove food survey answers to be consistant with commutes module.
- Handle SIFAC travels format when importing new travels.
- Add livestock emissions factors and documentation to farming activities.
- [Fix 322](https://framagit.org/labos1point5/l1p5-vuejs/-/issues/322): user of the travel simulator / editor can now mix one-way and round-trip sections.
- [Fix 324](https://framagit.org/labos1point5/l1p5-vuejs/-/issues/324) User of the
travel simulatore / editor can describe star travels (e.g Paris-Lille AR -
Paris-Rennes AR - Paris Toulouse AR)
- Review import procedure to better detect invalid date in user files
- Travels: geocoding requests are now restricted to "P" and "S"

## Transition

- Reviewer can now add a reminder date to an action.
  - Action with a reminder date will be removed from the reviewable actions
  - Action with a reminder date will be displayed in a dedicated tab in the reviewer page.


# 4.3.4

## Scenario

- Fix scenario list display, building rules were not displayed

# 4.3.3

## Carbon

- [Fix 318](https://framagit.org/labos1point5/l1p5-vuejs/-/issues/318): line with all empty fields are now ignored.

- Load GHGIs by year from the admininstration interface to load less data at once

# 4.3.2

## Development

- playwright:

  - test synthesis + travels
  - now firefox tests runs every time but chromium and webkit runs on tags only

- pre-commit:

    - enforce some initial [black](https://github.com/psf/black) and
    [prettier](https://github.com/prettier/prettier)/[eslint](https://github.com/eslint/eslint)
    formatting by default (can be used as pre commit -- see the README)

- docker-compose:
  We can now use [docker-compose](https://docs.docker.com/compose/) to setup a
  dev environment (see the README)

## Carbon

- Fix a bug in building part: emissions of construction were not computed
  correctly in anonymous mode (wrong datatype for GHGI year). Buildings were
  accounted even if there were too old.

- Various fixes in documentation (missing files, typos)

- Fix a bug in travel part: travels' purposes weren't detected correctly when
  the page when the language was set to english

- Contrails are now accounted by default


## Transition

- Factorize filter between the map and table
- Allow to filter on lab name

# v4.3.1

## CI

- Add an expiration date(1 month) to artifacts

## Carbon

- Fix bug in calculating GHG Protocol and V5 Reglementary results formats
- For the astronomy part, now correctly deal with pre-2018 GHGI

# v4.3.0

## Carbon

- Add research activities to ghgi boundaries

# v4.2.1

## CI

- Playwright: we now have "End-to-End" tests (tests that involve data in and out the database).
  Authenticated access is tested using sqlite3 as database backend.

## Core

- Frontend: fix collection mutation on update. We use `splice` on the inner
  array instead of mutating the object using `Object.assign` (which mutated the
  object pointed and could mess user code)

## Super administration

- Allows user account modification and deletion from administration interface

# 4.2.0

- Introduce `background jobs` to run long running tasks in the background
  - Design choice: wrap some management commands and allow to run them from the REST API
  - Tasks can thus be run from:
    - the management commands (by the adminisgtrator of the machine)
    - the REST API (by any superuser)
    - the administration area in the frontend (by any superuser)

## Simulators

- Purchases: fix spelling

## Transition

- Table of Actions:
  - Observers can now load more items (was limited to 50 previously)
  - Add some counters (#actions, #laboratories, #actions displayed)
- Filters:
  - Observers can now combine using `AND`/`OR` "administrations", "disciplines" and "tags"

## Core

- Add a management command to change an "Administration" label: `python manage.py administrations mv LABEL_SRC LABEL_DEST`

## Carbon

- Fix `populate_commutes` command according to the new model CommuteSection/Survey models

# 4.1.5

## Simulators

- Purchases:
  - Add a warning explainig why the purchase simulator disappeared
  - Remove corresponding route

## Carbon

- Frontend: fix french typo `lien publique -> lien public`
- Backend: allow superuser to access `views.get_ghgi_consumptions`

## Transition

- Frontend: change "intern" tag icon

# 4.1.4

## Simulators

- Food simulator: fix a bug

## Transition

- Add tags: `recrutement, numérique, stagiaire, cdd, permanent`
- Change left menu to keep only `Data/Les données` menu
