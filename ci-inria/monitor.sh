#!/usr/bin/env sh

set -ue

#
# Monitor a remote pipeline for the remote mirror
#
INRIA_PROJECT_ID=42823

PIPELINES_BASE_URL=https://gitlab.inria.fr/api/v4/projects/$INRIA_PROJECT_ID/pipelines

CURL="curl --silent"


# waiting for the pipeline to be created
# https://docs.gitlab.com/ee/user/project/repository/mirror/push.html
# In the default settings we wait 5min max, lets double the value
N=20
for i in $(seq 1 $N)
do
    # getting only the first pipeline that runs on this commit sha
    # there are indeed situation where the pipeline is run several times on the
    # same commit_sha (e.g on tags)
    pipeline="$($CURL $PIPELINES_BASE_URL | jq '. | map(select(.sha == env.CI_COMMIT_SHA)) | .[0]')"
    if [ ! "$pipeline" ]
    then
        echo "Pipeline not created yet for commit $CI_COMMIT_SHA [$i/$N]"
    else
        echo "Pipeline created .... monitoring it"
	break
    fi
    sleep 10
done

if [ ! "$pipeline" ]
then
    echo "Pipeline not created after $N attempts... failing"
    exit 1
fi

# pipeline has been created, we monitor its state
pipeline_id=$(echo $pipeline | jq -r .id)
web_url=$(echo $pipeline | jq -r .web_url)
while true
do

    echo "Updating the pipeline status: $web_url"
    status=$($CURL "$PIPELINES_BASE_URL/$pipeline_id" | jq -r .status)
    jobs=$($CURL "$PIPELINES_BASE_URL/$pipeline_id/jobs")

    case $status in

        pending)
            echo "Pipeline is pending"
            ;;

        created)
            echo "Pipeline created"
            ;;

        success)
            echo "Pipeline success"
            exit 0
            ;;

        running)
            echo "Pipeline is running"
            ;;

        *)
            echo "Pipeline failed"
            exit 1
            ;;
    esac
    # rationale of using printf:
    # don't interpret linebreaks, ... because there might be some on the returned json (e.g in the commit message)
    printf "%s" $jobs | jq -r 'map( "[" + .status + "]: " + .name + "|" +  .web_url )'
    sleep 3
done

