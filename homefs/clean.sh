#!/bin/bash

# Remove l1p5 website build products

# @author  Fabrice Jammes

set -euo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)

usage() {
    cat << EOD
Usage: $(basename "$0") [options]
Available options:
  -h            This message

Remove l1p5 website build products

EOD
}

while getopts h c ; do
    case $c in
        h) usage ; exit 0 ;;
        \?) usage ; exit 2 ;;
    esac
done
shift "$((OPTIND-1))"

if [ $# -ne 0 ] ; then
    usage
    exit 2
fi

echo "Remove l1p5 website build products"

BUILD_DIR="/home/labo15-au1/priv/wsgi-scripts"
echo "Move to $BUILD_DIR"
cd "$BUILD_DIR"

npm cache clean --force
rm -rf $BUILD_DIR/node_modules
rm -rf $BUILD_DIR/dist
