import { test, expect } from '@playwright/test'
import {
  authenticatedConnectionWithTags,
  logIn,
  createGHGIForEntity,
  uploadBuildings,
  uploadDevices,
  uploadPurchases,
  uploadTravels,
  uploadVehicles,
  addCommutesAndFoodsSurvey,
  addTags,
  addResearchActivities,
  legalStatements,
  cloneSurvey,
  GHGIPuclicLink
} from '../utils.js'

import { join } from 'path'
import { execSync } from 'child_process'

const homefsDir = join(__dirname, '..', '..')

test.afterEach(async () => {
  let stdout = execSync('python3 manage.py clean --yesiknowwhatiamdoing', {
    cwd: homefsDir,
    stdio: 'pipe',
    encoding: 'utf-8'
  })
  console.log(stdout)
})

test('[authenticated] add tags', async ({ page }) => {
  await logIn({ page })
  await addTags({ page })
  await expect(page.getByTestId('tags-table')).toHaveScreenshot(
    'tags-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated] upload building template file', async ({ page }) => {
  await logIn({ page })
  await legalStatements({ page })
  await createGHGIForEntity({ page })
  await uploadBuildings({ page })
  await expect(page.getByTestId('buildings-table')).toHaveScreenshot(
    'buildings-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]upload devices template file', async ({ page }) => {
  await logIn({ page })
  await legalStatements({ page })
  await createGHGIForEntity({ page })
  await uploadDevices({ page })
  await expect(page.getByTestId('devices-table')).toHaveScreenshot(
    'devices-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]upload purchases template file', async ({ page }) => {
  await logIn({ page })
  await legalStatements({ page })
  await createGHGIForEntity({ page })
  await uploadPurchases({ page })
  await expect(page.getByTestId('purchases-table')).toHaveScreenshot(
    'purchases-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]upload vechicles template file', async ({ page }) => {
  await logIn({ page })
  await legalStatements({ page })
  await createGHGIForEntity({ page })
  await uploadVehicles({ page })
  await expect(page.getByTestId('vehicles-table')).toHaveScreenshot(
    'vehicles-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]upload travels template file', async ({ page }) => {
  await logIn({ page })
  await legalStatements({ page })
  await createGHGIForEntity({ page })
  await uploadTravels({ page })
  await expect(page.getByTestId('travels-table')).toHaveScreenshot(
    'travels-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]add commutes and foods with survey', async ({ page }) => {
  test.setTimeout(60 * 1000)
  await authenticatedConnectionWithTags({ page })
  await createGHGIForEntity({ page })
  await addCommutesAndFoodsSurvey({
    page
    // positionLabel: 'Membre de la structure'
  })

  let mask = await page.getByTestId('maskedOnTest')
  await expect(page.getByTestId('commutes-table')).toHaveScreenshot(
    'commutes-table.png',
    {
      stylePath: join(__dirname, '../screenshot.css'),
      mask: [mask],
      maskColor: 'black'
    }
  )
  await page.locator('li').filter({ hasText: 'Alimentation' }).first().click()
  await expect(page.getByTestId('food-table')).toHaveScreenshot(
    'food-table.png',
    {
      stylePath: join(__dirname, '../screenshot.css'),
      mask: [mask],
      maskColor: 'black'
    }
  )
})

test('[authenticated]clone commutes', async ({ page }) => {
  test.setTimeout(60 * 1000)
  await authenticatedConnectionWithTags({ page })
  await createGHGIForEntity({ page, year: '2022' })
  await addCommutesAndFoodsSurvey({
    page
    // positionLabel: 'Membre de la structure'
  })

  await page.goto('http://localhost:8080/administration/all-ghgi')
  await page.getByTestId('menu-ghgis').click()
  await legalStatements({ page })
  await createGHGIForEntity({ page, year: '2023' })

  await cloneSurvey({ page, isCommute: true })
  // Hide ID on screenshots
  let mask = await page.getByTestId('maskedOnTest')
  await expect(page.getByTestId('commutes-table')).toHaveScreenshot(
    'commutes-table.png',
    {
      stylePath: join(__dirname, '../screenshot.css'),
      mask: [mask],
      maskColor: 'black'
    }
  )
})

test('[authenticated]clone foods', async ({ page }) => {
  test.setTimeout(60 * 1000)
  await authenticatedConnectionWithTags({ page })
  await createGHGIForEntity({ page })
  await addCommutesAndFoodsSurvey({
    page
    // positionLabel: 'Membre de la structure'
  })

  await page.goto('http://localhost:8080/administration/all-ghgi')
  await page.getByTestId('menu-ghgis').click()
  await legalStatements({ page })
  await createGHGIForEntity({ page, year: '2023' })

  await cloneSurvey({ page, isCommute: false })
  // Hide ID on screenshots
  let mask = await page.getByTestId('maskedOnTest')
  await expect(page.getByTestId('food-table')).toHaveScreenshot(
    'food-table.png',
    {
      stylePath: join(__dirname, '../screenshot.css'),
      mask: [mask],
      maskColor: 'black'
    }
  )
})

test('[authenticated]add research activities', async ({ page }) => {
  await authenticatedConnectionWithTags({ page })
  await createGHGIForEntity({ page })
  await addResearchActivities({ page, isAuthenticated: true })
  await expect(page.getByTestId('ractivities-table')).toHaveScreenshot(
    'ractivities-table.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]synthesis', async ({ page }) => {
  test.slow()
  await authenticatedConnectionWithTags({ page })
  await createGHGIForEntity({ page })
  // starting by handling food and survey
  // as it reloads the whole GHGI when reconnecting this might be slow if other
  // data are stored before
  await addCommutesAndFoodsSurvey({
    page,
    isAuthenticated: true
    // positionLabel: 'Membre de la structure'
  })
  await uploadPurchases({ page })
  await uploadDevices({ page })
  await uploadVehicles({ page })
  await uploadBuildings({ page })
  await uploadTravels({ page })
  await addResearchActivities({ page, isAuthenticated: true })
  await page
    .locator('li')
    .filter({ hasText: 'Empreinte carbone & soumission' })
    .click()
  await page.waitForTimeout(3000)
  await expect(page.getByTestId('ghgi-synthesis')).toHaveScreenshot(
    'ghgi-synhesis.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})

test('[authenticated]check public link', async ({ page }) => {
  test.slow()
  await authenticatedConnectionWithTags({ page })
  await createGHGIForEntity({ page })
  await uploadPurchases({ page })
  // spare some time and ony test the bare minimum
  // await uploadDevices({ page })
  // await uploadVehicles({ page })
  // await uploadBuildings({ page })
  // await uploadTravels({ page })
  // await addCommutesAndFoodsSurvey({ page })
  // await addResearchActivities({ page })
  await GHGIPuclicLink({ page })
  await expect(page.getByTestId('ghgi-synthesis')).toHaveScreenshot(
    'ghgi-public-synthesis.png',
    { stylePath: join(__dirname, '../screenshot.css') }
  )
})
