# Update the golden images

Golden images are tested on the CI infrastructure in a Linux environment.
So you need to make sure to generate them from a similar setup.

## Using VScode extension or local commands

If you are already on a Linux environment, you can use local command line (`npx
playwright ...`) or VScode extension.
Running the tests will generate any missing golden images.

## Using the provided docker image

We wrap the execution in a linux container that you can call with:

```
./tests/runnner.sh -u

# to reuse an existing container (named playwright)
./tests/runner.sh -u -n
```

Note: The (new) golden images are available in `homefs/tests/*-snapshots` but 
may be owned by `root`.


# Run the tests

Testing requires to be in the same environment where the golden images have been generated.
`runner.sh` allows to run everything in the same Linux environement.

```
./tests/runner.sh

# to reuse an existing container (named playwright)
./tests/runner.sh -u -n
```

# Limit to specific browsers

```
BROWSERS=firefox ./tests/runner.sh 
```

# If you know the entity

```
L1P5_TYPE=laboratory ./tests/runner.sh
```