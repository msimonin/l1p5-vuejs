#!/usr/bin/env bash


# fix the tabulations
# some building names aren't clean (contain several \t...)
sed -E -i "s/Alfonsi *\t+/Alfonsi\t/g" data/*/GES1point5_buildings_*.tsv
sed -E -i "s/Mini-Critt *\t+/Mini-Critt\t/g" data/*/GES1point5_buildings_*.tsv
sed -E -i "s/PPDB *\t+/PPDB\t/g" data/*/GES1point5_buildings_*.tsv
