#!/usr/bin/env bash

set -e

if [ -z $TOKEN ]
then
    echo "TOKEN must be set"
    exit 1
fi



if [ -z $BASE_URL ]
then
    BASE_URL=http://localhost:8000
fi

for year in {2019..2023}
do
    # relative to here
    EXPORT_DIR="data/$year"
    mkdir -p $EXPORT_DIR
    # relative to homefs
    BASE_URL=$BASE_URL EXPORT_DIR=dumps/$EXPORT_DIR YEAR=$year npm run db:export:ghgis
done
