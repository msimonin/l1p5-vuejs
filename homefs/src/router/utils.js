import router from './index'

/**
 * Handle some axios error by redirecting the app to dedicated error page
 *
 * @param {*} axios error
 */
function axiosRedirectError (error) {
  switch (error.response.status) {
    case 404:
      router.replace('/not-found').catch(() => {})
      break
    case 401:
      router.replace('/not-authorized').catch(() => {})
      break
  }
}

export { axiosRedirectError }
