import MainNavbar from '@/layout/MainNavbar.vue'
import MainFooter from '@/layout/MainFooter.vue'
import OnlyHeader from '@/layout/OnlyHeader.vue'
import { requireAuthenticated } from '@/router/utils'

export default [
  {
    path: '/ges-1point5/',
    name: 'ges-1point5',
    components: {
      default: () => import('@/views/GES1point5.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    meta: {
      titlefr: 'GES 1point5',
      titleen: 'GES 1point5'
    }
  },
  {
    path: '/ges-1point5/:id',
    name: 'ges-1point5-id',
    components: {
      default: () => import('@/views/GES1point5.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    beforeEnter: requireAuthenticated,
    meta: {
      titlefr: 'GES 1point5',
      titleen: 'GES 1point5'
    }
  },
  {
    path: '/ges-1point5-results/:uuid',
    name: 'ges-1point5-results',
    components: {
      default: () => import('@/views/GES1point5Results.vue'),
      header: MainNavbar,
      footer: MainFooter
    },
    meta: {
      titlefr: 'GES 1point5',
      titleen: 'GES 1point5'
    }
  },
  {
    path: '/survey/:uuid',
    name: 'survey',
    components: {
      default: () => import('@/views/Survey.vue'),
      header: OnlyHeader,
      footer: MainFooter
    },
    meta: {
      titlefr: 'Questionnaire empreinte',
      titleen: 'Footprint survey'
    }
  }
]
