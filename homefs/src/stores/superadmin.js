import { defineStore } from 'pinia'
import { useCoreStore } from './core'

import carbonService from '@/services/carbonService'
import scenarioService from '@/services/scenarioService'
import userService from '@/services/userService'
import Scenario from '@/models/scenario/Scenario.js'
import { User } from '@/models/user/user.js'

import { makeGHGI } from '@/utils/ghgi'

import _ from 'lodash'

export const useSuperadminStore = defineStore('superadmin', {
  state: () => ({
    allGHGI: [],
    allGHGIYears: [],
    scenarios: [],
    allUsers: [],
    jobsComputed: false,
    currentJob: null,
    totalUsers: 0,
    totalInactives: 0,
    totalReviewers: 0,
    totalAdmin: 0
  }),
  getters: {
    settings: (state) => {
      const core = useCoreStore()
      return core.setting('commute')
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    getAllGHGI (years) {
      const core = useCoreStore()
      let activeModulesObjs = core.activeModulesObjs
      let loadedYears = Array.from(
        new Set(this.allGHGI.map((ghgi) => ghgi.year))
      )
      let years2Load = years.filter((val) => !loadedYears.includes(val))

      return carbonService
        .getAllGHGIAdmin({
          years: years2Load
        })
        .then((data) => {
          for (let ghgi of data.ghgis) {
            let cghgi = makeGHGI(
              ghgi,
              activeModulesObjs,
              core.surveyLabelMapping
            )
            this.allGHGI.unshift(cghgi)
          }
          this.allGHGIYears = data.years
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    getScenarios (data) {
      return scenarioService
        .getScenariosAdmin(data)
        .then((scenarios) => {
          this.scenarios = []
          for (let scenario of scenarios) {
            let csenario = Scenario.createFromObj(scenario)
            this.scenarios.unshift(csenario)
          }
          return scenarios
        })
        .catch((error) => {
          throw error
        })
    },
    deleteGHGI (id) {
      return carbonService
        .deleteGHGI({
          ghgi_id: id
        })
        .then(() => {
          // remove in-place the current ghgi from the current list
          this.allGHGI = _.reject(this.allGHGI, (ghgi) => ghgi.id === id)
          return this.allGHGI
        })
        .catch((error) => {
          throw error
        })
    },
    /**
     * Get the all users
     * The data is cached
     * Might throw
     */
    getUsers (options) {
      // we cache the data anyway
      return userService
        .getUsers(options)
        .then((data) => {
          let users = data.results.map((d) => User.createFromObj(d))
          this.allUsers = users
          this.totalUsers = data.count_users
          this.totalInactives = data.count_inactives
          this.totalReviewers = data.count_reviewers
          this.totalAdmin = data.count_admins
          return this.allUsers
        })
        .catch((error) => {
          throw error
        })
    },
    updateUserIsAdmin ({ email, value }) {
      // make the request and update local count
      return userService
        .updateIsAdmin({ email, value })
        .then(() => (value ? this.totalAdmin++ : this.totalAdmin--))
    },
    deleteUser (data) {
      return userService
        .deleteUser({
          email: data.email
        })
        .then(() => {
          // remove in-place the current user from the current list
          this.allUsers = _.reject(this.allUsers, (u) => data.email === u.email)
          return this.allUsers
        })
        .catch((error) => {
          throw error
        })
    },
    updateUser (data) {
      return userService
        .updateUser(data)
        .then((user) => {
          // update in-place the current user from the current list
          let index = this.allUsers.findIndex((u) => data.email === u.email)
          this.allUsers.splice(index, 1, User.createFromObj(user))
          return this.allUsers
        })
        .catch((error) => {
          throw error
        })
    },
    sendActivationEmail (email) {
      return userService.sendActivationEmail(email)
    },
    updateRoles ({ email, roles }) {
      return userService.updateRoles({ email, roles }).then((data) => {
        let user = User.createFromObj(data)
        // update local user
        let index = this.allUsers.findIndex((u) => u.email === user.email)
        if (index >= 0) {
          // reactivity issue :( if
          // this.allUsers[index] = user
          this.allUsers[index].roles = user.roles
          return this.allUsers[index]
        }
      })
    }
  }
})
