/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import { defineStore } from 'pinia'
import { useCoreStore } from './core'

import scenarioService from '@/services/scenarioService'
import carbonService from '@/services/carbonService'
import coreService from '@/services/coreService'

import TagCategory from '@/models/core/TagCategory.js'
import Scenario from '@/models/scenario/Scenario.js'
import { entityFactory } from '@/models/core'

import _ from 'lodash'

import { updateAllGHGI } from '@/utils/ghgi'

export const useAdminStore = defineStore('admin', {
  state: () => ({
    entity: null,
    entityType: 'Entity',
    allTagCategories: [],
    allGHGI: [],
    allScenarios: [],
    allVehicles: [],
    allBuildings: []
  }),
  getters: {
    options: (state) => {
      const core = useCoreStore()
      return core.options
    },
    surveyLabelMapping: (state) => {
      const core = useCoreStore()
      return core.surveyLabelMapping
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    getEntity () {
      return coreService
        .getEntity()
        .then((data) => {
          if (data) {
            this.entityType = data.type
            const EntityClass = entityFactory(data.type)
            this.entity = EntityClass.createFromObj(data.data)
            return this.entity
          }
          return null
        })
        .catch((error) => {
          throw error
        })
    },
    saveEntity (entity) {
      return coreService
        .saveEntity(entity.toDatabase())
        .then((data) => {
          this.entityType = data.type
          const EntityClass = entityFactory(data.type)
          this.entity = EntityClass.createFromObj(data.data)
          return this.entity
        })
        .catch((error) => {
          throw error
        })
    },
    updateEntity (entity) {
      this.entity = entity
    },
    deleteGHGI (id) {
      return carbonService
        .deleteGHGI({ ghgi_id: id })
        .then(() => {
          // remove in-place the current ghgi from the current list
          this.allGHGI = _.reject(this.allGHGI, (ghgi) => ghgi.id === id)
          return this.allGHGI
        })
        .catch((error) => {
          throw error
        })
    },
    getAllGHGI () {
      const core = useCoreStore()
      return carbonService
        .getAllGHGI()
        .then((ghgis) => {
          this.allGHGI = updateAllGHGI(
            ghgis,
            core.activeModulesObjs,
            core.surveyLabelMapping,
            core.travelPositionLabels,
            core.travelPurposeLabels
          )
          this.computeAllGHGI()
          return ghgis
        })
        .catch((error) => {
          throw error
        })
    },
    getAllGHGIWithoutConsumptions () {
      const core = useCoreStore()
      return carbonService
        .getAllGHGIWithoutConsumptions()
        .then((ghgis) => {
          this.allGHGI = updateAllGHGI(
            ghgis,
            core.activeModulesObjs,
            core.surveyLabelMapping,
            core.travelPositionLabels,
            core.travelPurposeLabels
          )
          return ghgis
        })
        .catch((error) => {
          throw error
        })
    },
    computeAllGHGI (year = null) {
      for (let ghgi of this.allGHGI) {
        ghgi.compute(this.options, year)
      }
    },
    setGHGI (ghgi) {
      const core = useCoreStore()
      this.allGHGI = updateAllGHGI(
        [ghgi],
        core.activeModulesObjs,
        core.surveyLabelMapping,
        core.travelPositionLabels,
        core.travelPurposeLabels
      )
      this.computeAllGHGI()
    },
    updateGHGIYear (id, year) {
      let ghgis = this.allGHGI.filter((obj) => obj.id === id)
      if (ghgis.length === 1) {
        ghgis[0].year = year
      }
    },
    getScenarios () {
      return scenarioService
        .getScenarios()
        .then((scenarios) => {
          this.setScenarios(scenarios)
          return scenarios
        })
        .catch((error) => {
          throw error
        })
    },
    setScenarios (items) {
      this.allScenarios = []
      for (let item of items) {
        let scenario = Scenario.createFromObj(item)
        let ghgi = this.allGHGI.filter((obj) => obj.id === scenario.ghgi.id)[0]
        scenario.setGHGI(ghgi, false)
        this.allScenarios.unshift(scenario)
      }
    },
    deleteScenario (id) {
      return scenarioService
        .deleteScenario({
          id: id
        })
        .then((scenarios) => {
          this.setScenarios(scenarios)
          return scenarios
        })
        .catch((error) => {
          throw error
        })
    },
    getAllVehicles () {
      return carbonService
        .getAllVehicles()
        .then((data) => {
          if (data) {
            this.allVehicles = data
          }
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    getAllBuildings () {
      return carbonService
        .getAllBuildings()
        .then((data) => {
          if (data) {
            this.allBuildings = data
          }
          return data
        })
        .catch((error) => {
          throw error
        })
    },
    getAllTagCategories () {
      return coreService
        .getAllTagCategories()
        .then((tagCategories) => {
          this.allTagCategories = tagCategories.map((obj) =>
            TagCategory.createFromObj(obj)
          )
          return this.allTagCategories
        })
        .catch((error) => {
          throw error
        })
    },
    updateOrCreateTagCategory (data) {
      return coreService
        .updateOrCreateTagCategory(data.toDatabase())
        .then((category) => {
          if (data.id) {
            let index = this.allTagCategories.findIndex(
              (obj) => obj.id === data.id
            )
            this.allTagCategories.splice(
              index,
              1,
              TagCategory.createFromObj(category)
            )
          } else {
            this.allTagCategories.push(TagCategory.createFromObj(category))
          }
          return category
        })
        .catch((error) => {
          throw error
        })
    },
    deleteTagCategory (id) {
      return coreService
        .deleteTagCategory({
          id: id
        })
        .then(() => {
          // remove in-place the current category from the current list
          this.allTagCategories = _.reject(
            this.allTagCategories,
            (category) => category.id === id
          )
          return this.allTagCategories
        })
        .catch((error) => {
          throw error
        })
    }
  }
})
