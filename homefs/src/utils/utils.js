export function inverseMapping (o) {
  let inverseMapping = {}
  for (let [k, v] of Object.entries(o)) {
    if (inverseMapping[v] === undefined) {
      inverseMapping[v] = []
    }
    inverseMapping[v].push(k)
  }
  return inverseMapping
}
