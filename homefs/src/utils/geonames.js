/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import axios from 'axios'

function escapeVille (ville) {
  let eVille = ville.toLowerCase()
  eVille = eVille.replace(/^st\s/gi, 'saint ')
  eVille = eVille.replace('cedex', '')
  eVille = eVille.replace('#', '%23')
  eVille = eVille.replace('?', '%3F')
  eVille = eVille.replace('&', '%26')
  return eVille
}

function searchGEONames (
  userName,
  ville,
  lang,
  country = '',
  maxRows = 10,
  fuzzy = 0.6
) {
  const geoAxios = axios.create()
  geoAxios.defaults.headers.common = {}
  geoAxios.defaults.headers.common.accept = 'application/json'
  let request =
    'https://secure.geonames.net/searchJSON?name=' + escapeVille(ville)
  if (country) {
    if (Array.isArray(country)) {
      for (let ccountry of country) {
        request += '&country=' + ccountry
      }
    } else {
      request += '&country=' + country
    }
  }
  /*
  Filters for following feature classes
    P city, village,...
    S spot, building, farm, but also railroad station and airport
  */
  request += '&featureClass=P&featureClass=S'
  // request += '&countryBias=FR'
  request +=
    '&maxRows=' +
    maxRows +
    '&fuzzy=' +
    fuzzy +
    '&lang=' +
    lang +
    '&username=' +
    userName
  return geoAxios.get(request, { headers: {} })
}

function countryInfo (userName, lang) {
  const geoAxios = axios.create()
  geoAxios.defaults.headers.common = {}
  geoAxios.defaults.headers.common.accept = 'application/json'
  let request =
    'https://secure.geonames.net/countryInfo?lang=' +
    lang +
    '&username=' +
    userName
  return geoAxios.get(request, { headers: {} })
}

/**
 * GeoDecode and cache location based on their city name, country code
 * based on Geonames service
 *
 * usage:
 *   warm-up the cache
 *   request with the hit method
 *
 */
class GeonamesDecoder {
  constructor (username, lang = 'en') {
    // handle the cache as a simple array for now
    // use a collection otherwise
    this.cache = []
    this.cache = new Map()
    this.username = username
    this.lang = lang
  }

  hash (item) {
    return item.city + '-' + item.country
  }
  /**
   *
   * Check (offline) if the item has already been decoded.
   *
   * @param {Object} item - object to check
   * @param {string} item.city - city to decode
   * @param {sting} item.country - country to consider when decoding
   *
   * @returns {Object|undefined} - the hit if any, undefined otherwise
   */
  hit (item) {
    return this.cache.get(this.hash(item))
  }

  /**
   * Geodecode a list of locations and store it the local cache.
   * Used to warm-up the cache only
   *
   * @param {Object[]} items - list of object to decode
   * @param {string} items[].city - city to decode
   * @param {string} items[].country - country to consider when decoding
   */
  async warmup (items) {
    // keep track of the original item and its corresponding request to geodecode it
    let allRequests = []
    let allResponses = new Array(items.length)
    for (let index in items) {
      // preserve order
      let item = items[index]
      let hit = this.hit(item)
      if (!hit) {
        // set a marker to indicate we're waiting for an answer
        allResponses[index] = { city: item.city, country: item.country }
        // Make the request if we're clean enough
        if (item.city && item.country) {
          allRequests.push([
            item,
            searchGEONames(
              this.username,
              item.city,
              this.lang,
              item.country,
              1,
              1
            )
          ])
        } else {
          allResponses[index].clean = false
        }
      } else {
        allResponses[index] = hit
      }
    }
    let self = this
    // in a previous version we use axios.all/spread which are now deprecated.
    // the rationale behind using allSettled is to catch possible network errors
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Promise/allSettled
    return Promise.allSettled(allRequests.map((x) => x[1])).then(
      (responses) => {
        responses.forEach((response, index) => {
          if (response.status !== 'fulfilled') {
            return
          }
          // get the response
          let r = response.value
          if (r.status !== 200) {
            // FIXME(msimonin) : original code was handling some error here
            // .data.status.value === 19 (max request reached)
            console.error(r)
            return
          }
          if (r.data.geonames.length === 0) {
            console.error(r)
            return
          }
          // we're clean enough let's populate the cache
          // original item
          let item = allRequests[index][0]
          self.cache.set(this.hash(item), {
            name: r.data.geonames[0].name,
            lat: r.data.geonames[0].lat,
            lng: r.data.geonames[0].lng,
            countryCodeAdded: r.data.geonames[0].countryCode,
            // for cache management we need to get the original city/code
            city: item.city,
            country: item.country
          })
        })
      }
    )
  }
}

export { escapeVille, searchGEONames, countryInfo, GeonamesDecoder }
