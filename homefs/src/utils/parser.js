import * as Papa from 'papaparse'
import _ from 'lodash'
// critical
const ERROR_FILE_ERROR = 'error-msg-file-error'
const ERROR_MISSING_COLUMN = 'error-msg-missing-column'
const ERROR_MISSING_DELIMITER = 'error-msg-delimiter'
const ERROR_PREPROCESS = 'error-msg-preprocess'
const ERROR_GENERIC = 'error-msg-generic'
// less critical (row-based)
const ERROR_MISSING_QUOTES = 'error-msg-missing-quotes'
const ERROR_UNDETECTABLE_DELIMITER = 'error-msg-undetectable-delimiter'
const ERROR_TOO_FEW_FIELDS = 'error-msg-too-few-fields'
const ERROR_TOO_MANY_FIELDS = 'error-msg-too-many-fields'
const ERROR_CONVERTER = 'error-msg-converter'
const ERROR_MISSING_VALUE = 'error-msg-missing-value'
// unused for now
// const ERROR_NO_DEFAULT = 'error-msg-no-default'

class ParsingErrors extends Error {
  constructor (message) {
    super(message)
    this.errors = {}
  }

  /**
   *
   * @param {*} msg
   * @param {Object} params
   * @param {Number} params.row - the line number causing the error
   */
  add (msg, params) {
    if (this.errors[msg] === undefined) {
      this.errors[msg] = []
    }
    this.errors[msg].push(params)
  }

  get empty () {
    return _.isEmpty(this.errors)
  }

  getErrors (msg) {
    if (this.errors[msg] === undefined) {
      return []
    } else {
      return this.errors[msg]
    }
  }

  toObject () {
    // read-only
    return this.errors
  }
}

class CriticalParsingError extends Error {
  constructor (message, params) {
    super(message)
    this.params = params
  }
}

function _parseHeaders (header, formatDescriptions) {
  function _findHeader (regex) {
    for (let name of header) {
      if (name.search(regex) >= 0) {
        return name
      }
    }
    return null
  }
  let headerMap = {}
  Object.entries(formatDescriptions).forEach(function ([outName, desc]) {
    let matchedName = _findHeader(desc.pattern)
    if ('headerName' in desc && desc.headerName && !matchedName) {
      matchedName = desc.headerName
    }
    if ('required' in desc && desc.required && !matchedName) {
      let err = new CriticalParsingError(ERROR_MISSING_COLUMN, {
        column: outName
      })
      throw err
    }
    headerMap[outName] = {
      inName: matchedName,
      default: 'default' in desc ? desc.default : null,
      converter: 'converter' in desc ? desc.converter : (x) => x,
      required: 'required' in desc ? desc.required : false
    }
  })
  return headerMap
}

function _parseData (data, headerMap, preprocess) {
  let outData = []
  let preprocessedData = []
  try {
    preprocessedData = preprocess(data)
  } catch (error) {
    throw new CriticalParsingError(ERROR_PREPROCESS, { reason: error.message })
  }
  let parsingErrors = new ParsingErrors()
  let lineNumber = 0
  for (let line of preprocessedData) {
    let outLine = {}
    // marker to known if the outTravel is correct
    // for now incorrect means that we weren't able to parse an input and no
    // default value can be set
    Object.entries(headerMap).forEach(function ([outName, params]) {
      if (params.inName) {
        // the column is specified in the header
        // inName refers to the column name as it is written in the file

        // rationale:
        // handle quoted and unquoted field '"123"' or '123' -> '123'
        let value = line[params.inName].replace(/"(.*)"/, '$1').trim()
        try {
          // note that params.converter is always set and might be the identity
          outLine[outName] = params.converter(value)
        } catch (error) {
          // something went wrong when converting the value
          parsingErrors.add(ERROR_CONVERTER, {
            row: lineNumber,
            raw: line,
            inName: params.inName
          })
        }
        // note that params.default is always set and might be undefined
        if (!value && params.default !== null) {
          outLine[outName] = params.default
        } else if (!value) {
          // no value and no default,
          // -> don't output anything (ie undefined value)
          // two subsequent cases: the column is marked as required or not.
          // if we were strict we would throw a ERROR_NO_DEFAULT in the latter case.
          // but we prefer transmit the data as it is and let the caller make
          // sure the data is invalid
        }
      } else if (params.default !== null) {
        // the column is not specified in the header
        // we assign the default value
        outLine[outName] = params.default
      } else {
        // the column is not specified in the header
        // also the column is not marked as required (checked before calling
        // this function)
        // -> don't output anything
      }
    })
    outData.push(outLine)
    lineNumber = lineNumber + 1
  }
  if (!parsingErrors.empty) {
    throw parsingErrors
  }
  return outData
}

const _parseFile = (file, formatDescriptions) =>
  new Promise((resolve, reject) => {
    Papa.parse(file, {
      header: true,
      worker: true,
      delimitersToGuess: [',', '\t', ';'],
      skipEmptyLines: true,
      quoteChar: '"',
      // let's handle type conversions by ourselves
      dynamicTyping: false,
      complete: function (result) {
        let desc = formatDescriptions
        let format = 'default'
        if (formatDescriptions instanceof Function) {
          ;[desc, format] = formatDescriptions(result.meta.fields)
        }
        // check some format meta data
        let meta = {}
        if ('meta' in desc) {
          meta = desc.meta
          delete desc.meta
          if (
            'delimiters' in meta &&
            !meta.delimiters.includes(result.meta.delimiter)
          ) {
            // early leave, this is critical
            reject(new CriticalParsingError(ERROR_MISSING_DELIMITER))
          }
        }
        let headerMap = null
        try {
          headerMap = _parseHeaders(result.meta.fields, desc)
        } catch (error) {
          // early leave, this is critical
          reject(error)
        }

        // record all the errors
        let parsingErrors = new ParsingErrors()
        for (let e of result.errors) {
          let params = { row: e.row, raw: result.data[e.row] }
          switch (e.code) {
            case 'MissingQuotes':
              parsingErrors.add(ERROR_MISSING_QUOTES, params)
              break
            case 'UndetectableDelimiter':
              parsingErrors.add(ERROR_UNDETECTABLE_DELIMITER, params)
              break
            case 'TooFewFields':
              parsingErrors.add(ERROR_TOO_FEW_FIELDS, params)
              break
            case 'TooManyFields':
              parsingErrors.add(ERROR_TOO_MANY_FIELDS, params)
              break
            default:
              reject(new CriticalParsingError(ERROR_GENERIC))
          }
        }
        if (!parsingErrors.empty) {
          reject(parsingErrors)
        } else {
          let preprocess = function (data) {
            return data
          }
          if ('preprocess' in meta) {
            preprocess = meta.preprocess
          }

          try {
            let data = _parseData(result.data, headerMap, preprocess)
            resolve([data, format])
          } catch (error) {
            if (error instanceof ParsingErrors) {
              reject(error)
            } else {
              // not identified error
              reject(new CriticalParsingError(ERROR_GENERIC))
            }
          }
        }
      },
      error: function (error, file) {
        // issue when loading the file (before the complete cb is called)
        // CriticalError here ?
        console.error(error)
        return reject(CriticalParsingError(ERROR_FILE_ERROR, { file: file }))
      }
    })
  })

/**
 * Same as the _parseFile but with an extra callback to run before proceeding to
 * the next step
 * @param {*} file
 * @param {*} formatDescriptions
 * @param {*} callback
 *
 * @throws {ParsingError}
 *
 * @returns
 */
const parseFile = (file, formatDescriptions, callback = (x) => x) => {
  return new Promise((resolve, reject) => {
    _parseFile(file, formatDescriptions)
      .then(async (results) => {
        let data = results[0]
        let format = results[1]
        try {
          let items = await callback(data, format, file.name)
          return [items, format, file.name]
        } catch (error) {
          reject(new CriticalParsingError(ERROR_GENERIC))
        }
      })
      .then(([items, format, source]) => {
        resolve([items, format, source])
      })
      .catch((error) => reject(error))
  })
}

function cleanNumber (value) {
  let v = value
  if (typeof v === 'string') {
    // keep only the numbers
    // make sure float representation is using dot
    v = v.replace(',', '.')
    v = v.replace(/[^0-9.]/g, '')
  }
  return v
}

function intConverter (value) {
  let v = cleanNumber(value)
  if (isNaN(parseInt(v))) {
    return null
  } else {
    return parseInt(v)
  }
}

function floatConverter (value) {
  let v = cleanNumber(value)
  if (isNaN(parseFloat(v))) {
    return null
  } else {
    return parseFloat(v)
  }
}

function booleanConverter (value) {
  const DICT = {
    oui: true,
    o: true,
    yes: true,
    y: true,
    non: false,
    no: false,
    n: false
  }
  try {
    return DICT[value.toLowerCase()]
  } catch {
    return null
  }
}

export {
  parseFile,
  intConverter,
  floatConverter,
  booleanConverter,
  CriticalParsingError,
  ParsingErrors,
  ERROR_FILE_ERROR,
  ERROR_GENERIC,
  ERROR_MISSING_COLUMN,
  ERROR_MISSING_DELIMITER,
  ERROR_MISSING_QUOTES,
  ERROR_TOO_FEW_FIELDS,
  ERROR_TOO_MANY_FIELDS,
  ERROR_UNDETECTABLE_DELIMITER,
  ERROR_MISSING_VALUE
}
