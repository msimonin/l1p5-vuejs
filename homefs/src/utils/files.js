function generateFileName (name, prefix = 'GES1point5', ext = '.tsv') {
  let now = new Date()
  let month = ('0' + (now.getMonth() + 1)).slice(-2)
  let day = ('0' + now.getDate()).slice(-2)
  let filename =
    prefix + '_' + name + '_' + now.getFullYear() + month + day + ext
  return filename
}

function createExportFile (
  csvContent,
  name,
  prefix = 'GES1point5',
  ext = '.tsv'
) {
  csvContent = csvContent.replaceAll('#', 'hash')
  csvContent = 'data:text/csv;charset=utf-8,' + csvContent
  const data = encodeURI(csvContent)
  const link = document.createElement('a')
  link.setAttribute('href', data)
  let fileName = generateFileName(name, prefix, ext)
  link.setAttribute('download', fileName)
  link.click()
}

export { generateFileName, createExportFile }
