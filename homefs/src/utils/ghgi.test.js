import GHGI from '@/models/carbon/GHGI.js'
import { makeGHGI } from './ghgi.js'

import { readFile } from 'fs/promises'
import Modules from '@/models/Modules.js'
import { Laboratory } from '../models/core/entity/Laboratory.js'
import { ActiveModules } from '@/models/ActiveModules.js'
import { Boundary, Structure } from '../models/carbon/Boundary.js'
import Commute from '../models/carbon/Commute.js'

import { Configuration } from '../models/carbon/Configuration.js'

import _ from 'lodash'

import ELECTRICITY_FACTORS from '@/../data/factors/carbon/electricityFactors.json'
import CONSTRUCTION_FACTORS from '@/../data/factors/carbon/constructionFactors.json'
import FOOD_FACTORS from '@/../data/factors/carbon/foodFactors.json'
import HEATING_FACTORS from '@/../data/factors/carbon/heatingFactors.json'
import PURCHASES_FACTORS from '@/../data/factors/carbon/purchasesFactors.json'
import RACTIVITIES_FACTORS from '@/../data/factors/carbon/ractivitiesFactors.json'
import REFRIGERANTS_FACTORS from '@/../data/factors/carbon/refrigerantsFactors.json'
import TRANSPORTS_FACTORS from '@/../data/factors/carbon/transportsFactors.json'
import VEHICLES_FACTORS from '@/../data/factors/carbon/vehiclesFactors.json'
import WATER_FACTORS from '@/../data/factors/carbon/waterFactors.json'

const path = require('path')

const fakeDataDir = path.resolve(__dirname, '../..', 'backend/carbon/fake')
const FAKE_GHGI_PATH = path.join(fakeDataDir, 'fakeGHGI.json')
const FAKE_CONF_PATH = path.join(fakeDataDir, 'fakeConf.json')

describe('With custom position titles', () => {
  let conf = null

  beforeEach(async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)
  })

  test('one matching position title', () => {
    // create the boundary
    let boundary = Boundary.create({ foo: 2 })

    // one commute
    let c = new Commute({
      position: 'foo',
      nWorkingDay: 1,
      sections: [
        {
          mode: 'car',
          distance: 1,
          isDay2: false,
          pooling: 1,
          engine: 'gasoline'
        }
      ]
    })

    let payload = {
      year: 2023,
      description: 'test desc',
      boundary: boundary.toDatabase(),
      entity: new Laboratory({ name: 'Cogitamus' }).toDatabase(),
      commutes: [c]
    }
    let ghgi = makeGHGI(
      payload,
      // look up ACTIVE_MOULES setting
      ActiveModules.fromSettings(conf.settings),
      // don't use the mapping from the conf
      // use the generated one instead (identity)
      boundary.surveyLabelMapping
    )
    ghgi.compute(conf.toOptions())

    let e = ghgi.emissions
    expect(e.commutes.cdays).toEqual({ foo: 41 })
    expect(e.commutes.nbAnswers).toEqual({ foo: 1 })
    // check extrapolation, 41 weeks, 2 people
    expect(_.sum(e.commutes.status.distances['foo'])).toBe(82)
  })
})

describe('Build GHGI from payload', () => {
  let conf = null

  beforeEach(async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)
  })

  test('from fakeJSON (scenario)', async () => {
    // NOTE(msimonin): it doesn't contain any research activity
    let payload = await readFile(FAKE_GHGI_PATH, 'utf8')

    let ghgi = makeGHGI(
      JSON.parse(payload),
      ActiveModules.fromSettings(conf.settings),
      conf.CF_LABEL_MAPPING.value
    )
    ghgi.compute(conf.toOptions())
    let e = ghgi.emissions
    expect(e.buildings.intensity.intensity).toBeCloseTo(284848, 0)
    expect(e.commutes.intensity.intensity).toBeCloseTo(89177, 0)
    expect(e.foods.intensity.intensity).toBeCloseTo(40880, 0)
    expect(e.devices.intensity.intensity).toBeCloseTo(11728, 0)
    expect(e.travels.intensity.intensity).toBeCloseTo(176884, 0)
    expect(e.purchases.intensity.intensity).toBeCloseTo(410458, 0)
    expect(e.vehicles.intensity.intensity).toBeCloseTo(14635, 0)

    // test the synthesis
    // synthesis includes all possible modules
    let synthesis = ghgi.synthesis
    expect(synthesis.heatings.intensity).toBeCloseTo(134328, 0)
    expect(synthesis.electricity.intensity).toBeCloseTo(55844, 0)
    expect(synthesis.refrigerants.intensity).toBeCloseTo(7440, 0)
    expect(synthesis.water.intensity).toBeCloseTo(26400, 0)
    expect(synthesis.construction.intensity).toBeCloseTo(60836, 0)
    expect(synthesis.commutes.intensity).toBeCloseTo(89177, 0)
    expect(synthesis.foods.intensity).toBeCloseTo(40880, 0)
    expect(synthesis.travels.intensity).toBeCloseTo(176884, 0)
    expect(synthesis.vehicles.intensity).toBeCloseTo(14635, 0)
    expect(synthesis.devices.intensity).toBeCloseTo(11728, 0)
    expect(synthesis.purchases.intensity).toBeCloseTo(410458, 0)
    expect(synthesis.ractivities.intensity).toBeCloseTo(0, 0)
  })

  test('Building.compute called check synthesis', async () => {
    let payload = await readFile(path.resolve(FAKE_GHGI_PATH), 'utf8')

    let activeModulesObjs = ActiveModules.fromSettings(conf.settings)
    let ghgi = makeGHGI(JSON.parse(payload), activeModulesObjs)

    ghgi.compute(conf.toOptions(), null, [], Modules.BUILDINGS)

    // only building related intensities are set
    // other modules are set to 0
    let synthesis = ghgi.synthesis
    expect(synthesis.heatings.intensity).toBeCloseTo(134328, 0)
    expect(synthesis.electricity.intensity).toBeCloseTo(55844, 0)
    expect(synthesis.refrigerants.intensity).toBeCloseTo(7440, 0)
    expect(synthesis.water.intensity).toBeCloseTo(26400, 0)
    expect(synthesis.construction.intensity).toBeCloseTo(60836, 0)
    expect(synthesis.commutes.intensity).toBe(0)
    expect(synthesis.foods.intensity).toBe(0)
    expect(synthesis.travels.intensity).toBe(0)
    expect(synthesis.vehicles.intensity).toBe(0)
    expect(synthesis.devices.intensity).toBe(0)
    expect(synthesis.purchases.intensity).toBe(0)
    expect(synthesis.ractivities.intensity).toBe(0)
  })

  // Rationale: test that the right compute function is called
  // but we're changing the definition of static fonction so it will be changed
  // for subsequent tests
  // idea: create a completey fake module

  // test('TravelNew.compute called', async () => {
  //   let payload = await readFile(
  //     path.resolve(__dirname, '../../', 'data/utils/fakeGHGI.json'),
  //     'utf8'
  //   )

  //   // enable travel/form/precise
  //   let settings = _.cloneDeep(DEFAULT_SETTINGS)
  //   settings.find((s) => s.name === 'ACTIVE_MODULES').value = [
  //     'travels/form/precise'
  //   ]
  //   let activeModulesObjs = ActiveModules.fromSettings(settings)

  //   let ghgi = makeGHGI(JSON.parse(payload), activeModulesObjs)

  //   Building.compute = jest.fn()
  //   Travel.compute = jest.fn()
  //   TravelWithLocations.compute = jest.fn()

  //   ghgi.compute(settings, null, [], Modules.TRAVELS)
  //   expect(Building.compute).not.toHaveBeenCalled()
  //   expect(Travel.compute).not.toHaveBeenCalled()
  //   expect(TravelWithLocations.compute).toHaveBeenCalled()

  //   // should work even if all the modules don't get activated
  //   ghgi.compute(settings)
  //   expect(Building.compute).not.toHaveBeenCalled()
  // })

  test('with researchActivity', async () => {
    // initialize a structure with 0 members.
    let structure = Structure.fromNames(
      conf.POSITION_TITLES.value.map((p) => p.name)
    )
    structure
      .setNumber('pt.researcher', 1)
      .setNumber('pt.teacher', 1)
      .setNumber('pt.support', 1)
      .setNumber('pt.student-postdoc', 1)
    let boundary = new Boundary({
      structure,
      surveyLabelMapping: conf.CF_LABEL_MAPPING.value
    })

    let activeModulesObjs = ActiveModules.fromSettings(conf.settings)
    let ghgi = new GHGI({
      boundary,
      travelPositionLabels: conf.TRAVEL_POSITION_LABELS.value,
      travelPurposeLabels: conf.TRAVEL_PURPOSE_LABELS.value,
      activeModulesObjs
    })
    let ractivity = {
      category: 'facilities',
      type: 'cern',
      subtype: 'with.lhc',
      amount: 10000
    }
    ghgi.add(Modules.RACTIVITIES, [ractivity])

    // note GHGI doesn't have a toDatabase function yet
    // and to use makeGHGI we need to nest the entity
    let payload = {
      year: ghgi.year,
      description: ghgi.description,
      boundary: boundary.toDatabase(),
      entity: new Laboratory({ name: 'Cogitamus' }).toDatabase(),
      submitted: ghgi.submitted,
      synthesis: ghgi.synthesis,
      ractivities: [ractivity]
    }

    let g = makeGHGI(payload, activeModulesObjs, conf.CF_LABEL_MAPPING.value)
    g.compute(conf.toOptions())

    let e = g.emissions
    expect(e.ractivities.intensity.intensity).toBeCloseTo(345300000, 0)
  })
})

describe('Build GHGI from payload with some deactivated modules', () => {
  test('Intensity getter should account only for the activated modules(travels)', async () => {
    let payload = await readFile(FAKE_GHGI_PATH, 'utf8')
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    confPayload = JSON.parse(confPayload)
    // Activate a single module
    confPayload.settings.find((s) => s.name === 'ACTIVE_MODULES').value = [
      'travels/form'
    ]

    let conf = Configuration.fromDatabase(confPayload.settings)

    let ghgi = makeGHGI(
      JSON.parse(payload),
      ActiveModules.fromSettings(conf.settings),
      conf.CF_LABEL_MAPPING.value
    )

    ghgi.compute(conf.toOptions())
    let intensity = ghgi.intensity

    expect(intensity.intensity).toBeCloseTo(176884, 0)
    expect(intensity.uncertainty).toBeCloseTo(64066, 0)
  })

  test('Intensity getter should account only for the activated modules(buildings)', async () => {
    let payload = await readFile(FAKE_GHGI_PATH, 'utf8')
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    confPayload = JSON.parse(confPayload)
    // Activate a single module
    confPayload.settings.find((s) => s.name === 'ACTIVE_MODULES').value = [
      'buildings/form'
    ]

    let conf = Configuration.fromDatabase(confPayload.settings)

    let ghgi = makeGHGI(
      JSON.parse(payload),
      ActiveModules.fromSettings(conf.settings),
      conf.CF_LABEL_MAPPING.value
    )

    ghgi.compute(conf.toOptions())
    let intensity = ghgi.intensity

    // account for all the submodules of buildings
    expect(intensity.intensity).toBeCloseTo(284848, 0)
    expect(intensity.uncertainty).toBeCloseTo(50929, 0)
  })
})

function findEFS (o) {
  if (o.decomposition) {
    return [o]
  } else {
    return Object.values(o).flatMap((v) => findEFS(v))
  }
}

describe('Sanity checks for emission factor', () => {
  test.each([
    { efz: CONSTRUCTION_FACTORS, name: 'CONSTRUCTION_FACTORS' },
    { efz: ELECTRICITY_FACTORS, name: 'ELECTRICITY_FACTORS' },
    { efz: FOOD_FACTORS, name: 'FOOD_FACTORS' },
    { efz: HEATING_FACTORS, name: 'HEATING_FACTORS' },
    { efz: PURCHASES_FACTORS, name: 'PURCHASES_FACTORS' },
    { efz: RACTIVITIES_FACTORS, name: 'RACTIVITES_FACTORS' },
    { efz: REFRIGERANTS_FACTORS, name: 'REFRIGERANTS_FACTORS' },
    { efz: TRANSPORTS_FACTORS, name: 'TRANSPORTS_FACTORS' },
    { efz: VEHICLES_FACTORS, name: 'VEHICLES_FACTORS' },
    { efz: WATER_FACTORS, name: 'WATER_FACTORS' }
  ])('total is the sum of all the decomposition for $name', ({ efz, name }) => {
    // handle the unconsistency of EFz format
    let efs = findEFS(efz)

    // efs is a list of object containing a decomposition property
    for (let ef of Object.values(efs)) {
      // we have an ef
      for (let decomposition of Object.values(ef.decomposition)) {
        // for each year compare the total with the sum of the decomposition
        let totalIntensity = decomposition.total.total
        // compute the sum of other keys if a decomposition is available ==
        // there are other keys than 'total'
        let otherKeys = Object.keys(decomposition).filter(
          (k) => k !== 'total' && !_.isEmpty(decomposition[k])
        )
        if (otherKeys.length > 0) {
          // we have something to compare with
          let decompositionIntensity = _.sum(
            otherKeys.map((k) => decomposition[k].total)
          )
          if (totalIntensity > 0) {
            expect(decompositionIntensity / totalIntensity).toBeCloseTo(1, 1)
          }
        }
      }
    }
  })
})

describe('Commutes aggregate emisions', () => {
  test('from fake ghgi', async () => {
    let payload = await readFile(FAKE_GHGI_PATH, 'utf8')
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    confPayload = JSON.parse(confPayload)
    let conf = Configuration.fromDatabase(confPayload.settings)

    let ghgi = makeGHGI(
      JSON.parse(payload),
      ActiveModules.fromSettings(conf.settings),
      conf.CF_LABEL_MAPPING.value
    )

    ghgi.compute(conf.toOptions())
    let dict = Commute.aggregate(ghgi.emissions.commutes)

    // quick checks
    expect(dict['no.answers.cf.researcher-teacher']).toBe(22)
  })
})
