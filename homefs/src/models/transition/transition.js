/**
 * A Tag
 *
 */
// list of available tags (built from the json description)
import {
  ALL_TAGS,
  ALL_ADMIN_STATUSES,
  ALL_USER_STATUSES,
  COLORS
} from './constants.js'
import Modules from '@/models/Modules'

import _ from 'lodash'
import { entityFactory } from '../core/index.js'

const FIVE_DAYS_MS = 5 * 24 * 3600 * 1000

function dateToString (d) {
  if (d === null) {
    return null
  }
  return `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`
}

class Tag {
  static _TAGS = ALL_TAGS

  constructor () {
    this.descriptor = ''
    this.info = ''
    this.icon = ''
    this.type = ''
    this.color = ''
    this.i18n = {}
  }

  // group by type/color
  static group (tags) {
    let inverse = {}
    // assume not two same color
    for (let [k, v] of Object.entries(COLORS)) {
      inverse[v] = k
    }
    let groups = _.groupBy(tags, (t) => t.color)
    let keys = _.sortBy(Object.keys(groups))
    let sortGroups = {}
    for (let k of keys) {
      sortGroups[inverse[k]] = groups[k]
    }
    return sortGroups
  }

  setDescriptor (descriptor) {
    this.descriptor = descriptor
    return this
  }

  setInfo (info) {
    this.info = info
    return this
  }

  setIcon (icon) {
    this.icon = icon
    return this
  }

  setType (type) {
    this.type = type
    return this
  }

  setColor (color) {
    this.color = color
    return this
  }

  setI18n (i18n) {
    this.i18n = i18n
    return this
  }

  translate (lang) {
    if (!('i18n' in this)) return this.descriptor
    if (!('tag' in this.i18n)) return this.descriptor
    if (!(lang in this.i18n.tag)) {
      if ('fr' in this.i18n.tag) {
        return this.i18n.tag['fr']
      }
      return this.descriptor
    }
    return this.i18n.tag[lang]
  }

  translateInfo (lang) {
    if (!('i18n' in this)) return this.descriptor
    if (!('taginfo' in this.i18n)) return this.descriptor
    if (!(lang in this.i18n.taginfo)) {
      if ('fr' in this.i18n.taginfo) {
        return this.i18n.taginfo['fr']
      }
      return this.descriptor
    }
    return this.i18n.taginfo[lang]
  }

  static translate (tag, lang) {
    return Tag.fromJSON(tag).translate(lang)
  }

  static translateInfo (tag, lang) {
    return Tag.fromJSON(tag).translateInfo(lang)
  }

  static fromJSON (d) {
    return new Tag()
      .setDescriptor(d.descriptor)
      .setIcon(d.icon)
      .setInfo(d.info)
      .setType(d.type)
      .setColor(d.color)
      .setI18n(d.i18n)
  }

  /**
   * Test if the tag match text
   * @param {*} text
   */
  match (text) {
    let fr = _.deburr(this.translate('fr').toLowerCase())
    let en = _.deburr(this.translate('en').toLowerCase())
    let matchText = _.deburr(text).toLowerCase()
    return fr.indexOf(matchText) >= 0 || en.indexOf(matchText) >= 0
  }
}

// The list of all possible tags, admin and user status
const TAGS = ALL_TAGS.map((t) => Tag.fromJSON(t))
const ADMIN_STATUSES = ALL_ADMIN_STATUSES.map((t) => Tag.fromJSON(t))
const USER_STATUSES = ALL_USER_STATUSES.map((t) => Tag.fromJSON(t))

function makeGenericTag (tagList) {
  return function (descriptor) {
    let matches = tagList.filter((tag) => tag.descriptor === descriptor.trim())
    if (matches.length === 1) {
      return matches[0]
    }
    // NOTE(msimonin): should we use a default tag instead of throwing here ?
    return null
  }
}
function makeGenericTags (makeTagF) {
  return function (descriptors) {
    return descriptors
      .sort()
      .map((d) => makeTagF(d))
      .filter((t) => t != null)
  }
}

const makeTag = makeGenericTag(TAGS)
const makeTags = makeGenericTags(makeTag)

const makeAdminStatus = makeGenericTag(ADMIN_STATUSES)
const makeAdminStatuses = makeGenericTags(makeAdminStatus)

const makeUserStatus = makeGenericTag(USER_STATUSES)
const makeUserStatuses = makeGenericTags(makeUserStatus)

/**
 * Transform a module name to a tag
 * @param {*} module
 */
function moduleToTag (module) {
  switch (module) {
    case Modules.BUILDINGS:
      return makeTags(['tag:ges:buildings'])
    case Modules.HEATINGS:
      return makeTags(['tag:ges:buildings'])
    case Modules.ELECTRICITY:
      return makeTags(['tag:ges:buildings'])
    case Modules.REFRIGERANTS:
      return makeTags(['tag:ges:buildings'])
    case Modules.PURCHASES:
      return makeTags(['tag:ges:purchases'])
    case Modules.DEVICES:
      return makeTags(['tag:ges:devices'])
    case Modules.VEHICLES:
      return makeTags(['tag:ges:vehicles'])
    case Modules.TRAVELS:
      return makeTags(['tag:ges:travels'])
    case Modules.COMMUTES:
      return makeTags(['tag:ges:commutes'])
    default:
      return []
  }
}

function mapActionToGanttSeries (key, action) {
  return [
    {
      start: action.start.getTime(),
      end: action.end ? action.end.getTime() : new Date().getTime(),
      // name: `${key}-${series.length.toString()}`,
      name: key,
      action: action
    }
  ]
}

function toGantt (actions) {
  return actions.map((action) => mapActionToGanttSeries('-', action))
}

class PublicAction {
  constructor () {
    this.id = null
    this.title = ''
    this.text = ''
    // milliseconds
    this.createdAt = null
    // milliseconds
    this.updatedAt = null
    // Date object
    this.start = new Date()
    this.end = null
    this.abandoned = false
    this.contact = ''

    this.files = []

    this.tags = makeTags([])

    this.adminStatus = makeAdminStatus('adminstatus:draft')

    // the entity
    this.entity = null

    // the reviewer, if any
    this.reviewer = null

    // the reminder date if any
    this.reminder = null
  }

  setId (id) {
    this.id = id
    return this
  }

  setTitle (title) {
    this.title = title
    return this
  }

  setText (text) {
    this.text = text
    return this
  }

  setCreatedAt (createdAt) {
    this.createdAt = createdAt
    return this
  }

  setUpdatedAt (updatedAt) {
    this.updatedAt = updatedAt
    return this
  }

  setFiles (files) {
    this.files = files
    return this
  }

  setStart (start) {
    this.start = start
    return this
  }

  setEnd (end) {
    this.end = end
    return this
  }

  setAbandoned (abandoned) {
    this.abandoned = abandoned
    return this
  }

  setContact (contact) {
    this.contact = contact
    return this
  }

  setTags (tags) {
    this.tags = tags
    return this
  }

  setAdminStatus (adminStatus) {
    this.adminStatus = adminStatus
    return this
  }

  setEntity (entity) {
    this.entity = entity
    return this
  }

  setReviewer (reviewer) {
    this.reviewer = reviewer
    return this
  }

  setReminder (reminder) {
    this.reminder = reminder
    return this
  }

  displayContact () {
    if (!this.contact) {
      return this.entity.referent.email
    }
    return this.contact
  }

  get size () {
    return _.sumBy(this.files, (f) => f.size)
  }

  deleteFile (file) {
    let index = this.files.findIndex((f) => f.id === file.id)
    if (index >= 0) {
      this.files.splice(index, 1)
    }
  }

  /**
   *
   * Make a rich status tag from the action
   */
  get status () {
    if (this.abandoned) {
      return makeUserStatus('userstatus:abandoned')
    }
    if (this.end && this.end <= Date.now()) {
      return makeUserStatus('userstatus:terminated')
    }
    return makeUserStatus('userstatus:running')
  }

  static fromDatabase (payload) {
    let action = new PublicAction()

    let end = null
    if (payload.end) {
      end = new Date(payload.end)
    }
    let start = null
    if (payload.start) {
      start = new Date(payload.start)
    }
    let tags = []
    try {
      tags = makeTags(payload.tags.map((t) => t.descriptor))
    } catch {
      console.error('No tag can be decoded : ' + action.tags) // eslint-disable-line no-console
    }

    let reminder = null
    if (payload.reminder) {
      reminder = new Date(payload.reminder)
    }
    let EntityClass = entityFactory(payload.entity.type)
    let entity = EntityClass.createFromObj(payload.entity.data)

    action
      .setId(payload.id)
      .setTitle(payload.title)
      .setText(payload.text)
      .setCreatedAt(Date.parse(payload.created))
      .setUpdatedAt(Date.parse(payload.last_update))
      .setStart(start)
      .setEnd(end)
      .setAbandoned(payload.abandoned)
      .setContact(payload.contact)
      // taking the files as they are
      .setFiles(payload.files)
      .setTags(tags)
      .setAdminStatus(makeAdminStatus(payload.admin_status))
      .setEntity(entity)
      .setReviewer(payload.reviewer)
      .setReminder(reminder)

    return action
  }

  toDatabase () {
    let start = new Date(this.start)

    let result = {
      id: this.id,
      title: this.title,
      text: this.text,
      start: dateToString(start),
      abandoned: this.abandoned,
      contact: this.contact
    }

    let end = null
    if (this.end) {
      end = dateToString(this.end)
    }
    result.end = end

    let reminder = null
    if (this.reminder) {
      reminder = dateToString(this.reminder)
    }
    result.reminder = reminder

    result.tags = this.tags.map((t) => t.descriptor)
    result.admin_status = this.adminStatus.descriptor
    result.entity = this.entity
    result.reviewer = this.reviewer
    return result
  }

  clone () {
    let newAction = _.cloneDeep(this)
    // recover singleton natures of tags
    newAction.setTags(makeTags(newAction.tags.map((t) => t.descriptor)))
    return newAction
  }

  /**
   * True wheter the action is published
   */
  isPublished () {
    return this.adminStatus.descriptor === 'adminstatus:published'
  }

  /**
   * True wheter the action is submitted
   */
  isSubmitted () {
    return this.adminStatus.descriptor === 'adminstatus:submitted'
  }

  /**
   * True wheter the action is a draft
   */
  isDraft () {
    return this.adminStatus.descriptor === 'adminstatus:draft'
  }

  isReviewLate () {
    let current = new Date().getTime()
    // 5 days
    if (current - FIVE_DAYS_MS > this.updatedAt) {
      return true
    }
    return false
  }

  /**
   * True iff the reminder is in the past
   */
  isReminderLate () {
    return this.reminder <= new Date()
  }

  isAssigned () {
    return !!this.reviewer
  }
}

class Message {
  constructor () {
    this.message = ''
    this.date = null
    this.origin = ''
  }

  setMessage (message) {
    this.message = message
    return this
  }

  setDate (date) {
    this.date = date
    return this
  }

  setOrigin (origin) {
    this.origin = origin
    return this
  }

  static fromDatabase (payload) {
    return new Message()
      .setMessage(payload.message)
      .setDate(Date.parse(payload.date))
      .setOrigin(payload.origin)
  }
}

/**
 * Get the right class based on the string passed
 */
function getActionClass (str) {
  if (str === '') {
    return PublicAction
  }
}

/**
 * Util function that insert an item in an list in place.
 * items move follow MoveMixin
 *
 * @param {*} list
 * @param {*} item
 */
function insertOrUpdateInList (list, item) {
  // find the action to replace
  let index = list.findIndex((a) => a.id === item.id)
  if (index < 0) {
    // no match, inserting
    list.push(item)
  } else {
    // moving
    list[index].move(item)
  }
}

function removeInActions (actions, action) {
  let index = actions.findIndex((a) => a.id === action.id)
  if (index >= 0) {
    actions.splice(index, 1)
  }
}

let MoveMixin = {
  move (a) {
    for (let k of Object.keys(this)) {
      // FIXME(msimonin): if a[k] is a nested datastructure
      this[k] = a[k]
    }
    return this
  }
}

// assign some mixins
Object.assign(PublicAction.prototype, MoveMixin)

export {
  Tag,
  PublicAction,
  Message,
  dateToString,
  getActionClass,
  makeTag,
  makeTags,
  makeAdminStatus,
  makeAdminStatuses,
  makeUserStatus,
  makeUserStatuses,
  insertOrUpdateInList,
  removeInActions,
  moduleToTag,
  toGantt,
  TAGS,
  USER_STATUSES,
  ADMIN_STATUSES
}
