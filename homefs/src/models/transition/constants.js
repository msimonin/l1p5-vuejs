// ref https://mypads2.framapad.org/p/tags-pour-le-module-t1p5-lp3umc9ka

export const COLORS = {
  // dark sea green
  bgesModule: '#8fbc8f',
  // blue gray
  type: '#6699cc',
  // lavender
  rules: '#e6e6fa',
  // champagne
  mandatory: '#f7e7ce',
  // orange
  decision: '#ffca68',
  // pink
  level: 'pink',
  // red
  taskforce: '#f98c6b'
}

export const ALL_TAGS = [
  //  tags corresponding to GES1point5 modules (building, travels ...)
  {
    descriptor: 'tag:ges:buildings',
    info: 'taginfo:ges:buildings',
    icon: 'building',
    type: 'is-dark',
    color: COLORS.bgesModule,
    i18n: {
      tag: {
        fr: 'Bâtiments'
      },
      taginfo: {
        fr: "Action concernant le poste d'émission des bâtiments (chauffage, électricité, liquides frigorigènes, eau)"
      }
    }
  },
  {
    descriptor: 'tag:ges:devices',
    info: 'taginfo:ges:devices',
    icon: 'desktop',
    type: 'is-dark',
    color: COLORS.bgesModule,
    i18n: {
      tag: {
        fr: 'Matériel informatique'
      },
      taginfo: {
        fr: "Action concernant le poste d'émission lié au matériel informatique"
      }
    }
  },
  {
    descriptor: 'tag:ges:purchases',
    info: 'taginfo:ges:purchases',
    icon: 'euro',
    type: 'is-dark',
    color: COLORS.bgesModule,
    i18n: {
      tag: {
        fr: 'Achats'
      },
      taginfo: {
        fr: "Action concernant le poste d'émission lié aux achats (hors Matériel Informatique, Bâtiments, Déplacements, ...)"
      }
    }
  },
  {
    descriptor: 'tag:ges:travels',
    info: 'taginfo:ges:travels',
    icon: 'train',
    type: 'is-dark',
    color: COLORS.bgesModule,
    i18n: {
      tag: {
        fr: 'Missions'
      },
      taginfo: {
        fr: "Action concernant le poste d'émission des déplacements professionnels (transport uniquement)"
      }
    }
  },
  {
    descriptor: 'tag:ges:commutes',
    info: 'taginfo:ges:commutes',
    icon: 'bicycle',
    type: 'is-dark',
    color: COLORS.bgesModule,
    i18n: {
      tag: {
        fr: 'Ddt'
      },
      taginfo: {
        fr: "Action concernant le poste d'émission des déplacements domicile-travail"
      }
    }
  },
  {
    descriptor: 'tag:ges:vehicles',
    info: 'taginfo:ges:vehicles',
    icon: 'car',
    type: 'is-dark',
    color: COLORS.bgesModule,
    i18n: {
      tag: {
        fr: 'Véhicules'
      },
      taginfo: {
        fr: "Action concernant le poste d'émission lié aux véhicules du laboratoire"
      }
    }
  },
  {
    descriptor: 'tag:type:food',
    info: 'taginfo:type:food',
    icon: 'cutlery',
    type: 'is-dark',
    color: COLORS.bgesModule,
    i18n: {
      tag: {
        fr: 'Alimentation'
      },
      taginfo: {
        fr: 'Action visant à favoriser une alimentation moins émettrice de CO2 (repas végétariens, circuits courts, produits bio, produits locaux, ...)'
      }
    }
  },
  {
    descriptor: 'tag:ges:researchactivities',
    info: 'taginfo:ges:researchactivities',
    icon: 'hubzilla',
    type: 'is-dark',
    color: COLORS.bgesModule,
    i18n: {
      tag: {
        fr: 'Activités de recherche'
      },
      taginfo: {
        fr: "Activité concernant le poste d'émission lié aux activités de recherche"
      }
    }
  },
  // tags corresponding to action type
  {
    descriptor: 'tag:type:ghgi',
    info: 'taginfo:type:ghgi',
    icon: 'thermometer',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'BGES'
      },
      taginfo: {
        fr: "Établissement d'un ou plusieurs BGES et mise à disposition du réseau"
      }
    }
  },

  {
    descriptor: 'tag:type:heating',
    info: 'taginfo:type:heating',
    icon: 'thermometer',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Chauffage'
      },
      taginfo: {
        fr: 'Action concernant le chauffage des locaux et/ou son fonctionnement (installation de panneaux solaires, compteurs, thermostats, ...)'
      }
    }
  },
  {
    descriptor: 'tag:type:secondhand',
    info: 'taginfo:type:secondhand',
    icon: 'gift',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Deuxième vie'
      },
      taginfo: {
        fr: "Action visant à donner une seconde vie à des matériels/fournitures en dehors du labo (bourse d'échange, ressourcerie, ...)"
      }
    }
  },
  {
    descriptor: 'tag:type:remote',
    info: 'taginfo:type:remote',
    icon: 'globe',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Distanciel'
      },
      taginfo: {
        fr: "Action visant à favoriser le distanciel pour limiter les déplacements (équiper des salles en matériel de visoconférence, réunions internes à l'entité en distanciel, ...)"
      }
    }
  },
  {
    descriptor: 'tag:type:eco',
    info: 'taginfo:type:eco',
    icon: 'leaf',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Éco-gestes'
      },
      taginfo: {
        fr: 'Action relevant de gestes éco-responsables (utilisation de vaisselle lavable, impressions recto-verso, débrancher les appareils quand ils ne sont pas utilisés, ...)'
      }
    }
  },
  {
    descriptor: 'tag:type:spare',
    info: 'taginfo:type:spare',
    icon: 'lightbulb-o',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: "Économie d'énergie/eau"
      },
      taginfo: {
        fr: "Action visant une réduction de la consommation d'eau ou d'énergie(ne pas laisser  couler l'eau, éteindre les lumières avant de quitter les pièces, utilisation des eaux grises, trompes à eau en circuit fermé, ...)"
      }
    }
  },

  {
    descriptor: 'tag:type:renewables',
    info: 'taginfo:type:renewables',
    icon: 'sun-o',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Énergies renouvelables'
      },
      taginfo: {
        fr: "Action visant l'utilisation d'énergies renouvelables(installation de panneaux solaires sur les toits, fours solaires, ...)"
      }
    }
  },
  {
    descriptor: 'tag:type:gouvernance',
    info: 'taginfo:gouvernance',
    icon: 'user plus',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Gouvernance'
      },
      taginfo: {
        fr: "Action touchant aux modes d'organisation du laboratoire (mutualisation des équipements, groupes de travail sur le développement durable, mettre à l'ODJ du conseil de laboratoire un point bilan carbone du laboratoire, ...)"
      }
    }
  },
  {
    descriptor: 'tag:type:group',
    info: 'taginfo:group',
    icon: 'users',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Groupe'
      },
      taginfo: {
        fr: "Action concernant la constitution d'un groupe de personnes."
      }
    }
  },
  {
    descriptor: 'tag:type:tax',
    info: 'taginfo:type:tax',
    icon: 'euro',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Taxe'
      },
      taginfo: {
        fr: "Dispositif de taxe financière appliqué par tonne d’équivalent CO2  émise (donner le montant pour 1t eqCO2 et l'utilisation des revenus de la taxe)"
      }
    }
  },
  {
    descriptor: 'tag:type:decarbon',
    info: 'taginfo:type:decarbon',
    icon: 'bicycle',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Transports moins carbonés'
      },
      taginfo: {
        fr: 'Action visant à favoriser des modes de déplacement moins émetteurs de CO2 (vélo, transports en commun, mise en place de garages à vélos sécurisés, ...)'
      }
    }
  },
  {
    descriptor: 'tag:type:lifequality',
    info: 'taginfo:type:lifequality',
    icon: 'smile-o',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Qualité de vie au travail'
      },
      taginfo: {
        fr: 'Action visant à améliorer la qualité de vie au travail (escape game en lien avec la sobriété en recherche, ...)'
      }
    }
  },
  {
    descriptor: 'tag:type:quotas',
    info: 'taginfo:type:quotas',
    icon: 'sign-in',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Quotas'
      },
      taginfo: {
        fr: 'Dispositif de plafonnement des émissions de CO2 (achats,déplacements professionnels, déplacements  domicile-travail, ...)'
      }
    }
  },
  {
    descriptor: 'tag:type:offset',
    info: 'taginfo:type:offset',
    icon: 'calculator',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Compensation carbone'
      },
      taginfo: {
        fr: "Action visant à compenser les émissions de CO2 qui n'ont pu être évitées"
      }
    }
  },
  {
    descriptor: 'tag:type:trash:',
    info: 'taginfo:type:trash',
    icon: 'recycle',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Réduction des déchets'
      },
      taginfo: {
        fr: 'Action visant à réduire la quantité de déchets produits par le labo (vaisselle et verrerie lavables, cafetières avec filtres lavables, composteurs, ...)'
      }
    }
  },
  {
    descriptor: 'tag:type:purchases',
    info: 'taginfo:type:purchases',
    icon: 'hourglass half',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Réduire les achats neufs'
      },
      taginfo: {
        fr: "Action visant à réduire l'achat de matériels et fournitures neufs (seconde main, reconditionné, leasing, ...)"
      }
    }
  },
  {
    descriptor: 'tag:type:awareness',
    info: 'taginfo:type:awareness',
    icon: 'bullhorn',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Sensibilisation'
      },
      taginfo: {
        fr: "Action de sensibilisation (fresque du climat, de la biodiversité, de l'eau, du numérique... atelier Ma Terre en 180', conversations carbone, ...)"
      }
    }
  },
  {
    descriptor: 'tag:type:poll',
    info: 'taginfo:type:poll',
    icon: 'question',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Sondage'
      },
      taginfo: {
        fr: "Action visant à mesurer l'acceptabilité de mesures via un sondage"
      }
    }
  },
  {
    descriptor: 'tag:rules:charter',
    info: 'taginfo:rules:charter',
    icon: 'caret-right',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Charte'
      },
      taginfo: {
        fr: "Action concernant la mise en place d'une charte"
      }
    }
  },
  {
    descriptor: 'tag:rules:plans',
    info: 'taginfo:rules:plans',
    icon: 'calendar alternate',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Plan'
      },
      taginfo: {
        fr: "Action concernant la mise en place d'un plan de transition"
      }
    }
  },
  {
    descriptor: 'tag:type:researchtopics',
    info: 'taginfo:type:researchtopics',
    icon: 'flask',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Thématiques de recherche'
      },
      taginfo: {
        fr: 'Action concernant la réflexion sur les thématiques de recherche (objectifs de la recherche, valeurs, ...)'
      }
    }
  },

  {
    descriptor: 'tag:rules:recruitment',
    info: 'taginfo:rules:recruitment',
    icon: 'vcard-o',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Recrutement'
      },
      taginfo: {
        fr: "Recrutement (CDD, CDI ... à préciser dans le texte) d'une personne en lien avec l'empreinte environnementale (mesure, plan d'actions, réduction, animation, ...). profil de poste à joindre"
      }
    }
  },
  {
    descriptor: 'tag:rules:digital',
    info: 'taginfo:rules:digital',
    icon: 'desktop',
    type: 'is-dark',
    color: COLORS.type,
    i18n: {
      tag: {
        fr: 'Numérique'
      },
      taginfo: {
        fr: 'Action concernant la sobriété numérique'
      }
    }
  },

  // dimension incitations vs contrainte
  {
    descriptor: 'tag:incitatif',
    info: 'taginfo:incitatif',
    icon: 'minus square outline',
    type: 'is-light',
    color: COLORS.mandatory,
    i18n: {
      tag: {
        fr: 'Incitative'
      },
      taginfo: {
        fr: 'Action à visée incitative'
      }
    }
  },
  {
    descriptor: 'tag:obligatoire',
    info: 'taginfo:obligatoire',
    icon: 'plus square outline',
    type: 'is-light',
    color: COLORS.mandatory,
    i18n: {
      tag: {
        fr: 'Obligatoire'
      },
      taginfo: {
        fr: 'Action aux modalités obligatoires'
      }
    }
  },

  // Decision mode
  {
    descriptor: 'tag:decision:vote',
    info: 'taginfo:decision:vote',
    icon: 'envelope',
    type: 'is-light',
    color: COLORS.decision,
    i18n: {
      tag: {
        fr: 'Vote'
      },
      taginfo: {
        fr: 'Action mise en place après un vote (détailler les modalités du vote)'
      }
    }
  },
  {
    descriptor: 'tag:decision:cl',
    info: 'taginfo:decision:cl',
    icon: 'commenting',
    type: 'is-light',
    color: COLORS.decision,
    i18n: {
      tag: {
        fr: 'Échanges en conseil de laboratoire'
      },
      taginfo: {
        fr: 'Action mise en place par une décision du conseil de laboratoire'
      }
    }
  },
  {
    descriptor: 'tag:decision:ag',
    info: 'taginfo:decision:ag',
    icon: 'comments',
    type: 'is-light',
    color: COLORS.decision,
    i18n: {
      tag: {
        fr: "Échanges en AG d'unité"
      },
      taginfo: {
        fr: "Action mise en place par une décision à l'échelle de tout le laboratoire"
      }
    }
  },
  {
    descriptor: 'tag:decision:du',
    info: 'taginfo:decision:du',
    icon: 'comment',
    type: 'is-light',
    color: COLORS.decision,
    i18n: {
      tag: {
        fr: 'Décision de la direction'
      },
      taginfo: {
        fr: "Décision mise en place par une décision d'un organe de direction (directeur d'unité, comité de direction ...)"
      }
    }
  },

  // Level of the action
  {
    descriptor: 'tag:level:individual',
    info: 'taginfo:level:individual',
    icon: 'user',
    type: 'is-light',
    color: COLORS.level,
    i18n: {
      tag: {
        fr: 'Échelle individuelle'
      },
      taginfo: {
        fr: "Action qui s'applique à l'échelle individuelle"
      }
    }
  },
  {
    descriptor: 'tag:level:team',
    info: 'taginfo:level:team',
    icon: 'users',
    type: 'is-light',
    color: COLORS.level,
    i18n: {
      tag: {
        fr: 'Échelle équipe'
      },
      taginfo: {
        fr: "Action qui s'applique à l'échelle d'une équipe/service"
      }
    }
  },
  {
    descriptor: 'tag:level:lab',
    info: 'taginfo:level:lab',
    icon: 'home',
    type: 'is-light',
    color: COLORS.level,
    i18n: {
      tag: {
        fr: 'Échelle laboratoire'
      },
      taginfo: {
        fr: "Action qui s'applique à l'échelle collective"
      }
    }
  },
  // Task force
  {
    descriptor: 'tag:taskforce:intern',
    info: 'taginfo:taskforce:intern',
    icon: 'street-view',
    type: 'is-dark',
    color: COLORS.taskforce,
    i18n: {
      tag: {
        fr: 'Stagiaire'
      },
      taginfo: {
        fr: "Action qui implique une ou plusieurs personnes en stage (joindre l'offre de stage)"
      }
    }
  },
  {
    descriptor: 'tag:taskforce:cdd',
    info: 'taginfo:taskforce:cdd',
    icon: 'handshake-o',
    type: 'is-dark',
    color: COLORS.taskforce,
    i18n: {
      tag: {
        fr: 'CDD'
      },
      taginfo: {
        fr: 'Action qui implique une ou plusieurs personnes en CDD (joindre la fiche de poste)'
      }
    }
  },
  {
    descriptor: 'tag:taskforce:cdi',
    info: 'taginfo:taskforce:cdi',
    icon: 'handshake-o',
    type: 'is-dark',
    color: COLORS.taskforce,
    i18n: {
      tag: {
        fr: 'CDI'
      },
      taginfo: {
        fr: 'Action qui implique une ou plusieurs personnes en CDI (joindre la fiche de poste)'
      }
    }
  },
  {
    descriptor: 'tag:taskforce:volunteer',
    info: 'taginfo:taskforce:volunteer',
    icon: 'user-o',
    type: 'is-dark',
    color: COLORS.taskforce,
    i18n: {
      tag: {
        fr: 'Volontariat'
      },
      taginfo: {
        fr: 'Action qui implique une ou plusieurs personnes volontaires (préciser les modalités)'
      }
    }
  },
  {
    descriptor: 'tag:taskforce:mission',
    info: 'taginfo:taskforce:mission',
    icon: 'user',
    type: 'is-dark',
    color: COLORS.taskforce,
    i18n: {
      tag: {
        fr: 'Chargé.e de mission'
      },
      taginfo: {
        fr: 'Action qui implique un ou plusieurs personnes chargée de mission (préciser les modalités)'
      }
    }
  }
]

export const ALL_ADMIN_STATUSES = [
  {
    descriptor: 'adminstatus:draft',
    info: 'adminstatusinfo:draft',
    icon: 'pencil',
    type: 'is-primary',
    i18n: {
      tag: {
        fr: 'Brouillon'
      },
      taginfo: {
        fr: "L'action est éditable et non publiée"
      }
    }
  },
  {
    descriptor: 'adminstatus:submitted',
    info: 'adminstatusinfo:submitted',
    icon: '',
    type: 'is-warning',
    i18n: {
      tag: {
        fr: 'Soumis'
      },
      taginfo: {
        fr: "L'action est en cours de validation avant publication"
      }
    }
  },
  {
    descriptor: 'adminstatus:published',
    info: 'adminstatusinfo:published',
    icon: '',
    type: 'is-success',
    i18n: {
      tag: {
        fr: 'Publiée'
      },
      taginfo: {
        fr: "L'action est publiée"
      }
    }
  }
]

export const ALL_USER_STATUSES = [
  {
    descriptor: 'userstatus:running',
    info: 'userstatusinfo:running',
    icon: 'play',
    type: 'is-dark',
    i18n: {
      tag: {
        fr: 'en cours'
      },
      taginfo: {
        fr: "L'action est en cours"
      }
    }
  },
  {
    descriptor: 'userstatus:paused',
    info: 'userstatusinfo:paused',
    icon: 'pause',
    type: 'is-dark',
    i18n: {
      tag: {
        fr: 'en pause'
      },
      taginfo: {
        fr: "L'action est en pause"
      }
    }
  },

  {
    descriptor: 'userstatus:terminated',
    info: 'userstatusinfo:terminated',
    icon: 'check',
    color: 'black',
    type: 'is-dark',
    i18n: {
      tag: {
        fr: 'terminée'
      },
      taginfo: {
        fr: "L'action est terminée"
      }
    }
  },
  {
    descriptor: 'userstatus:abandoned',
    info: 'userstatusinfo:abandoned',
    icon: 'close',
    type: 'is-dark',
    i18n: {
      tag: {
        fr: 'abandonnée'
      },
      taginfo: {
        fr: "L'action est abandonnée"
      }
    }
  }
]
