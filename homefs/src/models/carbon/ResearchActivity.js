/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import RACTIVITIES_FACTORS from '@/../data/factors/carbon/ractivitiesFactors.json'
import COMPUTING_FACILITIES_ALPHA from '@/../data/researchActivities/computingFacilitiesAlpha.json'
import { EmissionFactor } from '@/models/carbon/Factor.js'
import Model from '@/models/carbon/Model.js'
import {
  CarbonIntensity,
  CarbonIntensities,
  DetailedCarbonIntensity,
  DetailedCarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'

// Depreciation periods (in years) for astronomical ground and space facilities
const DEPRECIATION = {
  'astro.ground': 38,
  'astro.space': 18
}
export class ResearchActivity extends Model {
  lang = {
    fr: {
      subtype: 'Sous-type',
      amount: 'Quantité'
    },
    en: {
      subtype: 'Sub-type',
      amount: 'Amount'
    }
  }

  constructor ({
    id = null,
    category = null,
    type = null,
    subtype = null,
    amount = 0,
    tags = []
  } = {}) {
    super({ tags })
    this.id = id
    this.category = category
    this.type = type
    this.subtype = subtype
    this.amount = amount
    this.intensity = new CarbonIntensity()
    this.items = []
  }

  get descriptor () {
    let h = 'nocategorynortype'
    if (this.type) {
      h = '' + this.type
    }
    if (this.category) {
      h = h.concat(this.category)
    }
    return h
  }

  isSimilarTo (other) {
    return (
      this === other ||
      this.id === other.id ||
      this.uuid === other.uuid ||
      this.descriptor === other.descriptor
    )
  }

  toString (sep = '\t') {
    return [
      this.category,
      this.type,
      this.subtype,
      this.amount,
      this.tags.map((val) => val.name).join(';'),
      Math.round(this.intensity.intensity),
      Math.round(this.intensity.uncertainty)
    ].join(sep)
  }

  isOther () {
    return false
  }

  isValid () {
    return this.score === 1
  }

  isIncomplete () {
    return this.score !== 1
  }

  isInvalid () {
    return this.score !== 1
  }

  getUnit () {
    let ef = EmissionFactor.createFromObj(
      RACTIVITIES_FACTORS[this.category][this.type][this.subtype]
    )
    return ef.unit
  }

  toDatabase () {
    return {
      category: this.category,
      type: this.type,
      subtype: this.subtype,
      amount: this.amount,
      tags: this.tags
    }
  }

  getEmissionFactor (year) {
    let ef = EmissionFactor.createFromObj(
      RACTIVITIES_FACTORS[this.category][this.type][this.subtype]
    )
    return ef.getFactor(year)
  }

  getCarbonIntensity (year = null) {
    let intensity = new CarbonIntensity()
    let ef = this.getEmissionFactor(year)
    intensity = new CarbonIntensity(
      this.amount * ef.total.total,
      this.amount * ef.total.total * ef.total.uncertainty,
      ef.group
    )
    this.intensity = intensity
    return intensity
  }

  getCarbonIntensityGHGP (year = null) {
    return {
      3.1: this.getCarbonIntensity(year)
    }
  }

  getCarbonIntensityV5 (year = null) {
    return {
      4.5: this.getCarbonIntensity(year)
    }
  }

  static translate (ractivity, key, lang) {
    if (key in ractivity.lang[lang]) {
      return ractivity.lang[lang][key]
    } else {
      return key
    }
  }

  static getDescription (category, type, subtype, lang) {
    let key = 'description_' + lang.toUpperCase()
    let ef = RACTIVITIES_FACTORS[category][type][subtype]
    if (key in ef) {
      return ef[key]
    } else {
      return ef['description_EN']
    }
  }

  static getUnit (category, type, subtype) {
    let unit = '-'
    if (Object.keys(RACTIVITIES_FACTORS).includes(category)) {
      if (Object.keys(RACTIVITIES_FACTORS[category]).includes(type)) {
        if (subtype) {
          if (
            Object.keys(RACTIVITIES_FACTORS[category][type]).includes(subtype)
          ) {
            let ef = EmissionFactor.createFromObj(
              RACTIVITIES_FACTORS[category][type][subtype]
            )
            unit = ef.unit
          }
        } else {
          let firstSubtype = Object.keys(RACTIVITIES_FACTORS[category][type])[0]
          let ef = EmissionFactor.createFromObj(
            RACTIVITIES_FACTORS[category][type][firstSubtype]
          )
          unit = ef.unit
        }
      }
    }
    return unit
  }

  static getCategories () {
    let categories = Object.keys(RACTIVITIES_FACTORS)
    return Array.from(new Set(categories))
  }

  static getTypes (category) {
    let types = Object.keys(RACTIVITIES_FACTORS[category])
    return Array.from(new Set(types))
  }

  static getSubTypes (category, type, year) {
    let subtypes = Object.keys(RACTIVITIES_FACTORS[category][type])
    return Array.from(new Set(subtypes))
  }

  static exportHeader (sep = '\t') {
    return [
      'category',
      'type',
      'subtype',
      'amount',
      'tags',
      'emission.kg.co2e',
      'uncertainty.kg.co2e'
    ].join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = ResearchActivity.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static compute (items, year, tags = []) {
    let intensities = new CarbonIntensities()
    let intensitiesGHGP = {}
    let intensitiesV5 = {}
    let meta = []
    for (let item of items.filter((obj) => obj.hasTags(tags))) {
      let intensity = item.getCarbonIntensity(year)
      let intensityV5 = item.getCarbonIntensityV5(year)
      let intensityGHGP = item.getCarbonIntensityGHGP(year)
      for (let key of Object.keys(intensityGHGP)) {
        if (key in intensitiesGHGP) {
          intensitiesGHGP[key].add(intensityGHGP[key])
        } else {
          if (key.startsWith('1.')) {
            intensitiesGHGP[key] = new DetailedCarbonIntensities()
          } else {
            intensitiesGHGP[key] = new CarbonIntensities()
          }
          intensitiesGHGP[key].add(intensityGHGP[key])
        }
      }
      for (let key of Object.keys(intensityV5)) {
        if (key in intensitiesV5) {
          intensitiesV5[key].add(intensityV5[key])
        } else {
          if (key.startsWith('1.')) {
            intensitiesV5[key] = new DetailedCarbonIntensities()
          } else {
            intensitiesV5[key] = new CarbonIntensities()
          }
          intensitiesV5[key].add(intensityV5[key])
        }
      }
      intensities.add(intensity)
      meta.unshift({
        category: item.category,
        type: item.type,
        subtype: item.subtype,
        amount: item.amount,
        intensity: intensity
      })
    }
    for (let key of Object.keys(intensitiesGHGP)) {
      intensitiesGHGP[key] = intensitiesGHGP[key].sum()
    }
    for (let key of Object.keys(intensitiesV5)) {
      intensitiesV5[key] = intensitiesV5[key].sum()
    }
    return {
      intensity: intensities.sum(),
      intensitiesGHGP: intensitiesGHGP,
      intensitiesV5: intensitiesV5,
      meta: meta
    }
  }
}

export class CERNFacility extends ResearchActivity {
  lang = {
    fr: {
      subtype: "Type d'expérience",
      amount: 'Quantité'
    },
    en: {
      subtype: 'Type of experience',
      amount: 'Amount'
    }
  }
}

export class GENCIComputingFacilities extends ResearchActivity {
  lang = {
    fr: {
      subtype: 'Type de service de calcul',
      amount: 'Quantité'
    },
    en: {
      subtype: 'Type of computing service',
      amount: 'Amount'
    }
  }

  getUnit () {
    let subtype = this.subtype.split('.')[1]
    let ef = EmissionFactor.createFromObj(
      RACTIVITIES_FACTORS[this.category][this.type][subtype]
    )
    return ef.unit
  }

  getEmissionFactor (year) {
    let efsubtype = this.subtype.split('.')[1]
    let ef = EmissionFactor.createFromObj(
      RACTIVITIES_FACTORS[this.category][this.type][efsubtype]
    )
    return ef.getFactor(year)
  }

  getCarbonIntensity (year = null) {
    let intensity = new CarbonIntensity()
    let ef = this.getEmissionFactor(year)
    let alpha = GENCIComputingFacilities.ALPHA.filter(
      (obj) => obj.name === this.subtype
    )[0].alpha
    intensity = new CarbonIntensity(
      this.amount * ef.total.total * alpha,
      this.amount * ef.total.total * alpha * ef.total.uncertainty,
      ef.group
    )
    this.intensity = intensity
    return intensity
  }

  static getDescription (category, type, subtype, lang) {
    let alpha = GENCIComputingFacilities.ALPHA.filter(
      (obj) => obj.name === subtype
    )[0]
    if (alpha) {
      return alpha.description
    } else {
      return '-'
    }
  }

  static getUnit (category, type, subtype) {
    let rsubtype = subtype.split('.')[1]
    let ef = EmissionFactor.createFromObj(
      RACTIVITIES_FACTORS['facilities']['genci.computing.facilities'][rsubtype]
    )
    return ef.unit
  }

  static getSubTypes (category, type, year) {
    return GENCIComputingFacilities.ALPHA.filter(
      (obj) => year >= obj['commissioning.year'] && year <= obj['downtime.year']
    ).map((obj) => obj.name)
  }

  static get ALPHA () {
    return COMPUTING_FACILITIES_ALPHA
  }
}

export class AstroFacilities extends ResearchActivity {
  get isGrouped () {
    return true
  }

  getCarbonIntensity (year) {
    let intensity = new CarbonIntensity()
    const ef = this.getEmissionFactor(year)
    const start =
      RACTIVITIES_FACTORS[this.category][this.type][this.subtype]['start']
    const stop =
      RACTIVITIES_FACTORS[this.category][this.type][this.subtype]['stop']
    let construction = 0
    let operations = 0
    const astroCat =
      RACTIVITIES_FACTORS[this.category][this.type][this.subtype]['class']
    if (year >= start && year - start <= DEPRECIATION[astroCat]) {
      construction =
        (this.amount / 100) * (ef.construction.total / DEPRECIATION[astroCat])
    }
    if (year >= start && year <= stop) {
      operations = (this.amount / 100) * ef.operations.total
    }
    intensity = new CarbonIntensity(
      construction + operations,
      construction * ef.construction.uncertainty +
        operations * ef.operations.uncertainty,
      ef.group
    )
    this.intensity = intensity
    return intensity
  }

  getUnit () {
    if (this.subtype === '-') {
      return 'use'
    } else {
      return super.getUnit()
    }
  }

  static get headerRow () {
    // amount smells here
    let row = new AstroFacilities({
      id: null,
      category: 'facilities',
      type: 'astro',
      subtype: '-',
      amount: '-'
    })
    return row
  }
}

export class FertilisersActivity extends ResearchActivity {
  lang = {
    fr: {
      subtype: "Type d'engrais",
      amount: 'Quantité épandu'
    },
    en: {
      subtype: 'Type of fertilizer',
      amount: 'Amount spread'
    }
  }

  getCarbonIntensityGHGP (year = null) {
    let ef = this.getEmissionFactor(year)
    return {
      3.1: new CarbonIntensity(
        this.amount * ef.manufacturing.total,
        this.amount * ef.manufacturing.total * ef.manufacturing.uncertainty,
        ef.group
      ),
      1.4: new DetailedCarbonIntensity(
        this.amount * ef.spreading.co2,
        this.amount * ef.spreading.ch4,
        this.amount * ef.spreading.n2o,
        this.amount * ef.spreading.other,
        this.amount * ef.spreading.total,
        this.amount * ef.spreading.total * ef.spreading.uncertainty,
        0,
        0,
        0,
        0,
        ef.group
      )
    }
  }

  getCarbonIntensityV5 (year = null) {
    let ef = this.getEmissionFactor(year)
    return {
      4.1: new CarbonIntensity(
        this.amount * ef.manufacturing.total,
        this.amount * ef.manufacturing.total * ef.manufacturing.uncertainty,
        ef.group
      ),
      1.4: new DetailedCarbonIntensity(
        this.amount * ef.spreading.co2,
        this.amount * ef.spreading.ch4,
        this.amount * ef.spreading.n2o,
        this.amount * ef.spreading.other,
        this.amount * ef.spreading.total,
        this.amount * ef.spreading.total * ef.spreading.uncertainty,
        0,
        0,
        0,
        0,
        ef.group
      )
    }
  }
}

export class LivestockActivity extends ResearchActivity {
  lang = {
    fr: {
      subtype: 'Espèce',
      amount: 'Nombre'
    },
    en: {
      subtype: 'Species',
      amount: 'Number'
    }
  }

  getCarbonIntensityGHGP (year = null) {
    let ef = this.getEmissionFactor(year)
    return {
      1.4: new DetailedCarbonIntensity(
        this.amount * ef.digestiondejections.co2,
        this.amount * ef.digestiondejections.ch4,
        this.amount * ef.digestiondejections.n2o,
        this.amount * ef.digestiondejections.other,
        this.amount * ef.digestiondejections.total,
        this.amount *
          ef.digestiondejections.total *
          ef.digestiondejections.uncertainty,
        0,
        0,
        0,
        0,
        ef.group
      ),
      3.1: new DetailedCarbonIntensity(
        this.amount * ef.feeding.co2,
        this.amount * ef.feeding.ch4,
        this.amount * ef.feeding.n2o,
        this.amount * ef.feeding.other,
        this.amount * ef.feeding.total,
        this.amount * ef.feeding.total * ef.feeding.uncertainty,
        0,
        0,
        0,
        0,
        ef.group
      )
    }
  }

  getCarbonIntensityV5 (year = null) {
    let ef = this.getEmissionFactor(year)
    return {
      1.4: new DetailedCarbonIntensity(
        this.amount * ef.digestiondejections.co2,
        this.amount * ef.digestiondejections.ch4,
        this.amount * ef.digestiondejections.n2o,
        this.amount * ef.digestiondejections.other,
        this.amount * ef.digestiondejections.total,
        this.amount *
          ef.digestiondejections.total *
          ef.digestiondejections.uncertainty,
        0,
        0,
        0,
        0,
        ef.group
      ),
      4.1: new DetailedCarbonIntensity(
        this.amount * ef.feeding.co2,
        this.amount * ef.feeding.ch4,
        this.amount * ef.feeding.n2o,
        this.amount * ef.feeding.other,
        this.amount * ef.feeding.total,
        this.amount * ef.feeding.total * ef.feeding.uncertainty,
        0,
        0,
        0,
        0,
        ef.group
      )
    }
  }
}

export function researchActivityClass (type) {
  let classes = {
    cern: CERNFacility,
    'genci.computing.facilities': GENCIComputingFacilities,
    astro: AstroFacilities,
    fertilisers: FertilisersActivity,
    livestock: LivestockActivity
  }
  if (Object.keys(classes).includes(type)) {
    return classes[type]
  } else {
    return ResearchActivity
  }
}
// return the activity as a list (accounting for items)
export function toList (ractivity) {
  let items = []
  if (ractivity.items.length === 0) {
    items.push(researchActivityFactory(ractivity))
  } else {
    for (let item of ractivity.items) {
      let iitem = researchActivityFactory(item)
      iitem.tags = ractivity.tags
      items.push(iitem)
    }
  }
  return items
}

export function researchActivityFactory (item) {
  let Nclass = researchActivityClass(item.type)
  let obj = new Nclass(item)
  return obj
}
