import _ from 'lodash'

class PositionTitle {
  constructor ({ id = null, name = null, quotity = 1 } = {}) {
    this.id = id
    this.name = name
    this.quotity = quotity
  }

  static fromDatabase (payload) {
    return new this(payload)
  }

  static fromName (name) {
    return new this({ name })
  }
}

/** somehow a wrapper of PositionTitle */
class Members {
  constructor ({ name = null, quotity = 1, number = 0 } = {}) {
    this.name = name
    this.quotity = quotity
    this.number = number
  }

  get workforce () {
    return this.number
  }

  get totalPerCapita () {
    return this.number * this.quotity
  }

  setNumber (number) {
    this.number = number
    return this
  }

  toDatabase () {
    return {
      position: { name: this.name, quotity: this.quotity },
      number: this.number
    }
  }

  static fromPositionTitle (positionTitle) {
    // id will be stripped
    return new this(positionTitle)
  }

  static fromName (name) {
    return new this({ name })
  }

  static fromDatabase (payload) {
    // example: {"number": "46", "position": {"name": "student.postdoc", "quotity": 1}}
    return new this({
      name: payload.position.name,
      number: parseInt(payload.number),
      quotity: parseFloat(payload.position.quotity)
    })
  }
}

/**
 * Model the positionTitles and associated number of people
 */
class Structure {
  constructor (membersList) {
    this.membersList = membersList
  }

  /** Initialization with no people in each position. */
  static fromPositionTitles (positionTitles) {
    return new this(positionTitles.map((p) => Members.fromPositionTitle(p)))
  }

  /** Initializes with no people in each position. */
  static fromNames (names) {
    return new this(names.map((n) => Members.fromName(n)))
  }

  get workforce () {
    return _.sum(this.membersList.map((m) => m.workforce))
  }

  get totalPerCapita () {
    return _.sum(this.membersList.map((m) => m.totalPerCapita))
  }

  get names () {
    return this.membersList.map((p) => p.name)
  }

  membersByPositionName (positionName) {
    let candidates = this.membersList.filter((p) => p.name === positionName)
    if (!candidates.length) {
      throw new Error(`${positionName} doesn't exist in the structure`)
    }
    return candidates[0]
  }

  setNumber (positionName, number) {
    let members = this.membersByPositionName(positionName)
    members.setNumber(number)
    return this
  }

  addNumber (positionName, number) {
    let n = this.getNumber(positionName)
    this.setNumber(positionName, parseInt(n) + parseInt(number))
    return this
  }

  getNumber (positionName) {
    let members = this.membersByPositionName(positionName)
    return members.number
  }

  toDatabase () {
    return this.membersList.map((m) => m.toDatabase())
  }

  /**
   * Allows to iterate natively on a collection
   */
  [Symbol.iterator] () {
    return this.membersList.values()
  }
}

/**
 *
 * Build a mapping from label used in Commute survey and position names.  The
 * resuting mapping can be used in the Boundary to aggregate some of the
 * positions together. The (supposed) rationale is to simplify the number of
 * mapping used in the various survey and (maybe) to better anonymised the
 * responses.
 *
 * @param {Object} mapping - an object (mapping)
 * @param {PositionTitle[] | Members[]} positionTitles -  list of position titles
 * @returns
 */
function buildPositionLabelMapping (mapping, positionTitles) {
  if (!positionTitles) {
    return {}
  }

  let positionNames = positionTitles.map((p) => p.name)
  // mapping not null but might be missing some name found in structure
  let r = {}
  for (let positionName in mapping) {
    if (!positionNames.includes(positionName)) {
      throw new Error(`unknown ${positionName} in the current structure`)
    }
    r[positionName] = mapping[positionName]
  }
  // add missing in a 1-to-1 mapping
  for (let p of positionTitles) {
    if (!(p.name in r)) {
      r[p.name] = p.name
    }
  }
  return r
}

class Boundary {
  constructor ({
    id = null,
    budget = 0,
    structure = null,
    locations = [],
    surveyLabelMapping = {}
  } = {}) {
    this.id = id
    this.budget = budget
    this.structure = structure || new Structure()
    this.locations = locations
    // For each positionTitle (given by its name) returns the corresponding
    // label to use in the surveys (food and commutes) if unknown use one-to-one
    // mapping with position titles
    this.surveyLabelMapping = buildPositionLabelMapping(
      surveyLabelMapping,
      this.structure.membersList
    )
  }

  get workforce () {
    return this.structure?.workforce
  }

  get totalPerCapita () {
    return this.structure?.totalPerCapita
  }

  /**
   * Returns a virtual structure based on the surveyLabels
   */
  get surveyStructure () {
    // /!\ quotity will be set to 1
    // number will be 0 (expected)

    // FIXME(msimonin): handle undefined/empty surveyLabelMapping
    let structure = Structure.fromNames(
      _.uniq(Object.values(this.surveyLabelMapping))
    )
    for (let positionName in this.surveyLabelMapping) {
      structure.addNumber(
        this.surveyLabelMapping[positionName],
        this.structure.getNumber(positionName)
      )
    }
    return structure
  }

  static fromDatabase (payload, surveyLabelMapping) {
    let memberList = payload.members.map((m) => Members.fromDatabase(m))
    let structure = new Structure(memberList)
    // FIXME(msimonin): handle locations
    return new this({
      id: payload.id,
      structure,
      budget: payload.budget,
      locations: payload.locations,
      surveyLabelMapping
    })
  }

  /**
   * Create a Boundary from a structure description
   *
   * @param {*} structureDesc
   * @param {*} surveyLabelMapping
   * @returns
   */
  static create (structureDesc, surveyLabelMapping) {
    let structure = Structure.fromNames(Object.keys(structureDesc))
    for (let key in structureDesc) {
      structure.setNumber(key, structureDesc[key])
    }
    let boundary = new this({
      structure,
      surveyLabelMapping
    })
    return boundary
  }

  static fromPositionTitles (positionTitles, surveyLabelMapping) {
    let names = positionTitles.map((p) => p.name)
    // init the structure description with zeros
    let structureDesc = {}
    for (let name of names) {
      structureDesc[name] = 0
    }
    return this.create(structureDesc, surveyLabelMapping)
  }

  toDatabase () {
    return {
      id: this.id,
      budget: this.budget,
      members: this.structure.toDatabase(),
      locations: this.locations.map((l) => l.toDatabase())
    }
  }

  // name is backward compat
  // survey only
  positionNormalizationFactor (label, nb) {
    let s = this.surveyStructure
    // how many people for this label
    return s.getNumber(label) / nb
  }

  static exportHeader (positionTitles, sep = '\t') {
    let headers = positionTitles.map((p) => p.name).sort()
    headers.push('budget')
    return headers.join(sep)
  }

  toString (sep = '\t') {
    let s = []
    for (let members of _.sortBy(this.structure.membersList, (m) => m.name)) {
      s.push(members.number)
    }
    s.push(this.budget)
    return s.join(sep)
  }
}

export {
  Boundary,
  Members,
  PositionTitle,
  Structure,
  buildPositionLabelMapping
}
