import Commute from '@/models/carbon/Commute.js'
import { Tag } from '@/models/core/TagCategory.js'
import { Boundary } from './Boundary'
import { Configuration } from './Configuration'

import { readFile } from 'fs/promises'
const path = require('path')

const fakeDataDir = path.resolve(__dirname, '../../..', 'backend/carbon/fake')
const FAKE_CONF_PATH = path.join(fakeDataDir, 'fakeConf.json')

/**
 *
 * @param {Object} modifier key, value to modify in the default commute Object
 *
 * @returns {Commute}
 */
function commuteObj (modifier = undefined) {
  let commute = {
    seqID: null,
    position: null,
    sections: [],
    nWorkingDay: null,
    nWorkingDay2: null,
    message: null,
    cdeleted: false
  }

  if (modifier === undefined) {
    modifier = {}
  }

  for (let [key, value] of Object.entries(modifier)) {
    commute[key] = value
  }
  return Commute.createFromObj(commute)
}

function zerosExcept (modifier) {
  // initialize an array with some value changed
  let result = new Array(Commute.TRAVELS_MODES.length).fill(0)
  if (modifier === undefined) {
    modifier = {}
  }

  for (let [mode, value] of Object.entries(modifier)) {
    result[Commute.TRAVELS_MODES.indexOf(mode)] = value
  }
  return result
}

describe('isPooling', () => {
  test('no section should return false', () => {
    let commute = commuteObj()
    expect(commute.isPooling).toBeFalsy()
  })

  // type1 carpooling
  test('mode=car, distance=1, pooling=2 should return true', () => {
    let commute = commuteObj({
      sections: [
        {
          mode: 'car',
          distance: 1,
          isDay2: false,
          pooling: 2
        }
      ]
    })
    expect(commute.isPooling).toBeTruthy()
  })

  test('mode=motorbike, distance=1, pooling=2 should return true', () => {
    let commute = commuteObj({
      sections: [
        {
          mode: 'motorbike',
          distance: 1,
          isDay2: false,
          pooling: 2
        }
      ]
    })
    expect(commute.isPooling).toBeTruthy()
  })

  test('mode=car, distance=1, pooling=2, isDay2=true should return true', () => {
    let commute = commuteObj({
      sections: [
        {
          mode: 'car',
          distance: 1,
          isDay2: true,
          pooling: 2
        }
      ]
    })
    expect(commute.isPooling).toBeTruthy()
  })
})

describe('compute', () => {
  let conf = null

  beforeEach(async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)
  })

  test('return value with no commutes: check anything but intensity', () => {
    let boundary = Boundary.create(
      {
        'pt.researcher': 1,
        'pt.teacher': 1,
        'pt.support': 1,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )

    let r = Commute.compute([], 0, boundary, 2042, conf.setting('commute'))
    // console.log(r)
    expect(r).toEqual(
      expect.objectContaining({
        totalPerCommute: [],
        status: expect.objectContaining({
          distances: {
            'cf.researcher-teacher': zerosExcept(),
            'cf.support': zerosExcept(),
            'cf.student-postdoc': zerosExcept()
          }
        }),
        nbAnswers: {
          'cf.researcher-teacher': 0,
          'cf.support': 0,
          'cf.student-postdoc': 0
        },
        cdays: {
          'cf.researcher-teacher': 0,
          'cf.support': 0,
          'cf.student-postdoc': 0
        }
      })
    )
  })

  test('return value with [{car=1, engine=diesel, nbWorkingDay=5, position=engineer}] -- 100% answers: check anything but intensity', () => {
    let commute = commuteObj({
      nWorkingDay: 5,
      position: 'cf.support',
      sections: [
        {
          mode: 'car',
          distance: 10,
          engine: 'diesel',
          isDay2: false,
          pooling: 1
        }
      ]
    })

    let boundary = Boundary.create(
      {
        'pt.researcher': 1,
        'pt.teacher': 1,
        'pt.support': 1,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )

    let r = Commute.compute([commute], 0, boundary, 2042, conf.toOptions())
    // console.log(r)
    expect(r).toEqual(
      expect.objectContaining({
        // TODO
        // totalPerCommute
        // TODO
        // intensity
        status: expect.objectContaining({
          distances: {
            'cf.researcher-teacher': zerosExcept(),
            'cf.support': zerosExcept({ car: 10 * 5 * 41 }),
            'cf.student-postdoc': zerosExcept()
          },
          volumePerMode: {
            'cf.researcher-teacher': zerosExcept(),
            'cf.support': zerosExcept({ car: 41 * 5 }),
            'cf.student-postdoc': zerosExcept()
          }
        }),
        nbAnswers: {
          'cf.researcher-teacher': 0,
          'cf.support': 1,
          'cf.student-postdoc': 0
        },
        cdays: {
          'cf.researcher-teacher': 0,
          'cf.support': 41 * 5 * 1,
          'cf.student-postdoc': 0
        }
      })
    ) // expect
  })

  test('return value with [{car=1, engine=diesel, nbWorkingDay=5, position=engineer}] -- 50% answers: check anything but intensity', () => {
    let commute = commuteObj({
      nWorkingDay: 5,
      position: 'cf.support',
      sections: [
        {
          mode: 'car',
          distance: 10,
          engine: 'diesel',
          isDay2: false,
          pooling: 1
        }
      ]
    })

    let boundary = Boundary.create(
      {
        'pt.researcher': 1,
        'pt.teacher': 1,
        'pt.support': 2,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )

    // Consider having 1 answer over 2 engineers (50%)
    let r = Commute.compute([commute], 0, boundary, 2042, conf.toOptions())
    // console.log(r)
    expect(r).toEqual(
      expect.objectContaining({
        // TODO
        // totalPerCommute
        // TODO
        // intensity
        status: expect.objectContaining({
          distances: {
            'cf.researcher-teacher': zerosExcept(),
            // extrapolate to all the engineer : x2
            'cf.support': zerosExcept({ car: 2 * 10 * 5 * 41 }),
            'cf.student-postdoc': zerosExcept()
          },
          volumePerMode: {
            'cf.researcher-teacher': zerosExcept(),
            'cf.support': zerosExcept({ car: 2 * 5 * 41 }),
            'cf.student-postdoc': zerosExcept()
          }
        }),
        nbAnswers: {
          'cf.researcher-teacher': 0,
          'cf.support': 1,
          'cf.student-postdoc': 0
        },
        cdays: {
          'cf.researcher-teacher': 0,
          'cf.support': 41 * 5 * 1,
          'cf.student-postdoc': 0
        }
      })
    ) // expect
  })

  test('Commute.compute with tags', () => {
    let commute1 = commuteObj({
      nWorkingDay: 5,
      position: 'cf.support',
      tags: [Tag.createFromObj('tag1')],
      sections: [
        {
          mode: 'car',
          distance: 10,
          engine: 'diesel',
          isDay2: false,
          pooling: 1
        }
      ]
    })
    let commute2 = commuteObj({
      nWorkingDay: 5,
      position: 'cf.researcher-teacher',
      tags: [Tag.createFromObj('tag2')],
      sections: [
        {
          mode: 'ebike',
          distance: 10,
          isDay2: false,
          pooling: 1
        }
      ]
    })
    let commute3 = commuteObj({
      nWorkingDay: 5,
      position: 'cf.researcher-teacher',
      sections: [
        {
          mode: 'motorbike',
          distance: 10,
          isDay2: false,
          pooling: 1
        }
      ]
    })

    let boundary0 = Boundary.create(
      {
        'pt.researcher': 0,
        'pt.teacher': 0,
        'pt.support': 1,
        'pt.student-postdoc': 0
      },
      conf.CF_LABEL_MAPPING.value
    )

    let r = Commute.compute([commute1], 0, boundary0, 2022, conf.toOptions())
    expect(r.intensity.intensity).toEqual(0.2121 * 10 * 5 * 41)

    // 10 km a day by car, 5 days, 41 weeks, 1 engineer
    let boundary1 = Boundary.create(
      {
        'pt.researcher': 0,
        'pt.teacher': 0,
        'pt.support': 2,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )
    let r1 = Commute.compute([commute1], 0, boundary1, 2022, conf.toOptions())
    // 10 km a day by car, 5 days, 41 weeks, 2 engineers
    expect(r1.intensity.intensity).toEqual(0.2121 * 10 * 5 * 41 * 2)

    let boundary2 = Boundary.create(
      {
        'pt.researcher': 1,
        'pt.teacher': 0,
        'pt.support': 0,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )

    let r2 = Commute.compute([commute2], 0, boundary2, 2022, conf.toOptions())
    // 10 km a day by bike, 5 days, 41 weeks, 1 researcher
    expect(r2.intensity.intensity).toBeCloseTo(0.0109 * 10 * 5 * 41, 5)

    let boundary3 = Boundary.create(
      {
        'pt.researcher': 2,
        'pt.teacher': 0,
        'pt.support': 0,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )
    let r3 = Commute.compute([commute2], 0, boundary3, 2022, conf.toOptions())
    // 10 km a day by bike, 5 days, 41 weeks, 2 researchers
    expect(r3.intensity.intensity).toBeCloseTo(0.0109 * 10 * 5 * 41 * 2, 5)

    let boundary4 = Boundary.create(
      {
        'pt.researcher': 1,
        'pt.teacher': 0,
        'pt.support': 0,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )
    let r4 = Commute.compute([commute3], 0, boundary4, 2022, conf.toOptions())
    // 10 km a day by motorbike, 5 days, 41 weeks, 1 researcher
    expect(r4.intensity.intensity).toBeCloseTo(0.1913 * 10 * 5 * 41, 5)

    let boundary5 = Boundary.create(
      {
        'pt.researcher': 4,
        'pt.teacher': 0,
        'pt.support': 1,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )

    let r5 = Commute.compute(
      [commute1, commute2, commute3],
      0,
      boundary5,
      2022,
      conf.toOptions()
    )
    // 10 km a day by bike, 5 days, 41 weeks, 1 engineer + 4 researchers
    expect(r5.intensity.intensity).toBeCloseTo(
      0.2121 * 10 * 5 * 41 + (0.0109 + 0.1913) * 2 * 10 * 5 * 41,
      5
    )

    let boundary6 = Boundary.create(
      {
        'pt.researcher': 4,
        'pt.teacher': 0,
        'pt.support': 1,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )
    let r6 = Commute.compute(
      [commute1, commute2, commute3],
      0,
      boundary6,
      2022,
      conf.toOptions(),
      [Tag.createFromObj('tag1')]
    )
    // 10 km a day by bike, 5 days, 41 weeks, 1 engineer + 4 researchers, tag1 (> only 1 engineer)
    expect(r6.intensity.intensity).toBeCloseTo(0.2121 * 10 * 5 * 41, 5)
    expect(r6.intensity.intensity).toEqual(r.intensity.intensity)

    let boundary7 = Boundary.create(
      {
        'pt.researcher': 4,
        'pt.teacher': 0,
        'pt.support': 1,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )
    let r7 = Commute.compute(
      [commute1, commute2, commute3],
      0,
      boundary7,
      2022,
      conf.toOptions(),
      [Tag.createFromObj('tag2')]
    )
    // 10 km a day by bike, 5 days, 41 weeks, 1 engineer + 4 researcher, tag2 (> 50% of the 4 researchers)
    expect(r7.intensity.intensity).toBeCloseTo(0.0109 * 10 * 5 * 41 * 2, 5)

    let boundary8 = Boundary.create(
      {
        'pt.researcher': 4,
        'pt.teacher': 0,
        'pt.support': 1,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )

    let r8 = Commute.compute(
      [commute1, commute2, commute3],
      0,
      boundary8,
      2022,
      conf.toOptions(),
      [Tag.createFromObj('untagged')]
    )
    // 10 km a day by bike, 5 days, 41 weeks, 1 engineer + 4 researcher, untagged (> 50% of the 4 researchers)
    expect(r8.intensity.intensity).toBeCloseTo(0.1913 * 10 * 5 * 41 * 2, 5)
    // Check sum tag1 + tag2 + untagged
    expect(r5.intensity.intensity).toEqual(
      r6.intensity.intensity + r7.intensity.intensity + r8.intensity.intensity
    )
  })
})

describe('exportToFile', () => {
  let conf = null

  beforeEach(async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)
  })

  test('Export items', () => {
    let commute = commuteObj({
      nWorkingDay: 5,
      position: 'cf.support',
      sections: [
        {
          mode: 'car',
          distance: 10,
          engine: 'diesel',
          isDay2: false,
          pooling: 1
        }
      ]
    })

    let boundary = Boundary.create(
      {
        'pt.researcher': 1,
        'pt.teacher': 0,
        'pt.support': 0,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )

    Commute.compute([commute], 1, boundary, 2022, conf.toOptions())
    // NOTE(msimonin): there is an extra \n at the beginning
    expect(Commute.exportToFile([commute], false, null, ';')).toBe(
      '\n;false;5;cf.support;diesel;0;1;0;0;0;0;0;10;0;0;0;0;0;0;0;;;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;435;0;0;0;0;0;0;0;435;;'
    )
  })

  test('Export with deleted items', () => {
    let commute = commuteObj({
      nWorkingDay: 5,
      position: 'cf.support',
      cdeleted: true,
      sections: [
        {
          mode: 'car',
          distance: 10,
          engine: 'diesel',
          isDay2: false,
          pooling: 1
        }
      ]
    })

    let boundary = Boundary.create(
      {
        'pt.researcher': 1,
        'pt.teacher': 0,
        'pt.support': 0,
        'pt.student-postdoc': 1
      },
      conf.CF_LABEL_MAPPING.value
    )

    Commute.compute([commute], 1, boundary, 2022, conf.toOptions())
    // NOTE(msimonin): there is an extra \n at the beginning
    expect(Commute.exportToFile([commute], false, null, ';')).toBe(
      '\n;true;5;cf.support;diesel;0;1;0;0;0;0;0;10;0;0;0;0;0;0;0;;;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;435;0;0;0;0;0;0;0;435;;'
    )
  })
})

export { commuteObj }
