import {
  Boundary,
  PositionTitle,
  Structure,
  buildPositionLabelMapping
} from './Boundary'

describe('buildPositionLabelMapping', () => {
  test('Empty mapping', () => {
    let positionTitles = ['p1', 'p2', 'p3'].map((n) =>
      PositionTitle.fromName(n)
    )
    let mapping = buildPositionLabelMapping({}, positionTitles)
    expect(mapping).toEqual({ p1: 'p1', p2: 'p2', p3: 'p3' })
  })

  test('all in one', () => {
    let positionTitles = ['p1', 'p2', 'p3'].map((n) =>
      PositionTitle.fromName(n)
    )
    let mapping = { p1: 'a', p2: 'a', p3: 'a' }
    let newMapping = buildPositionLabelMapping(mapping, positionTitles)
    expect(newMapping).toEqual({ p1: 'a', p2: 'a', p3: 'a' })
  })

  test('partial mapping', () => {
    let positionTitles = ['p1', 'p2', 'p3'].map((n) =>
      PositionTitle.fromName(n)
    )
    let mapping = { p1: 'a', p2: 'b' }
    let newMapping = buildPositionLabelMapping(mapping, positionTitles)
    expect(newMapping).toEqual({ p1: 'a', p2: 'b', p3: 'p3' })
  })
})

describe('surveyStructure', () => {
  test('simple translation', () => {
    let positionTitles = ['p1', 'p2'].map((n) => PositionTitle.fromName(n))
    let structure = Structure.fromPositionTitles(positionTitles)
      .setNumber('p1', 1)
      .setNumber('p2', 2)
    let mapping = { p1: 'a', p2: 'b' }
    let surveyLabelMapping = buildPositionLabelMapping(mapping, positionTitles)
    let b = new Boundary({ structure, surveyLabelMapping })
    expect(b.surveyStructure.membersByPositionName('a').number).toEqual(1)
    expect(b.surveyStructure.membersByPositionName('b').number).toEqual(2)
  })

  test('aggregation', () => {
    let positionTitles = ['p1', 'p2'].map((n) => PositionTitle.fromName(n))
    let structure = Structure.fromPositionTitles(positionTitles)
      .setNumber('p1', 1)
      .setNumber('p2', 2)
    let mapping = { p1: 'a', p2: 'a' }
    let surveyLabelMapping = buildPositionLabelMapping(mapping, positionTitles)
    let b = new Boundary({ structure, surveyLabelMapping })
    expect(b.surveyStructure.membersByPositionName('a').number).toEqual(3)
  })
})

describe('export boundary', () => {
  test('toString', () => {
    let positionTitles = ['p1', 'p2'].map((n) => PositionTitle.fromName(n))
    let structure = Structure.fromPositionTitles(positionTitles)
      .setNumber('p1', 1)
      .setNumber('p2', 2)
    let b = new Boundary({ structure, budget: 42 })
    expect(b.toString(',')).toBe('1,2,42')
  })

  test('toString order independant', () => {
    let positionTitles = ['p2', 'p1'].map((n) => PositionTitle.fromName(n))
    let structure = Structure.fromPositionTitles(positionTitles)
      .setNumber('p1', 1)
      .setNumber('p2', 2)
    let b = new Boundary({ structure, budget: 42 })
    expect(b.toString(',')).toBe('1,2,42')
  })

  test('exportHeader', () => {
    let positionTitles = ['p1', 'p2'].map((n) => PositionTitle.fromName(n))
    let s = Boundary.exportHeader(positionTitles, ',')
    expect(s).toBe('p1,p2,budget')
  })

  test('exportHeader order independant', () => {
    let positionTitles = ['p2', 'p1'].map((n) => PositionTitle.fromName(n))
    let s = Boundary.exportHeader(positionTitles, ',')
    expect(s).toBe('p1,p2,budget')
  })
})

describe('build Boundary', () => {
  test('No parameter', () => {
    // not really practical (should it throw instead ?)
    let b = new Boundary()
    expect(b.surveyLabelMapping).toEqual({})
  })

  test('Simple structure', () => {
    let b = Boundary.create({ foo: 100 })
    expect(b.surveyLabelMapping).toEqual({ foo: 'foo' })
  })
})
