/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import {
  CarbonIntensity,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'
import Modules from '@/models/Modules.js'
import { Tag } from '@/models/core/TagCategory'
import {
  TravelCollection,
  ModuleCollection
} from '@/models/carbon/Collection.js'
import Synthesis from '@/models/carbon/Synthesis'
import { entityFactory } from '../core/entity/factory'
import { Laboratory } from '../core/entity/Laboratory'
import { Boundary } from './Boundary'

/**
 * Build a submitted object and making sure all modules are in there
 * @param {Object} submitted
 *
 * @return{Object} the submitted Object with all modules as keys
 */
function makeSubmitted (submitted) {
  let s = {}
  for (let module of Modules.getModules(true)) {
    s[module] = false
  }
  if (submitted) {
    Object.assign(s, submitted)
  }
  return s
}

/**
 * Central piece of the app.
 * Represents a GHGI, the associated entity, boundary and data.
 *
 */
class GHGI {
  constructor ({
    id = null,
    uuid = null,
    year = null,
    boundary = null,
    created = null,
    description = null,
    surveyMessage = null,
    surveyCloneYear = null,
    commutesActive = true,
    foodsActive = true,
    surveyTags = [],
    entity = null,
    submitted = null,
    buildings = [],
    purchases = [],
    devices = [],
    vehicles = [],
    travels = [],
    commutes = [],
    foods = [],
    ractivities = [],
    synthesis = null,
    emissions = {},
    activeModulesObjs = null
  } = {}) {
    this.id = id
    this.uuid = uuid
    this.year = year
    this.created = new Date(created)
    this.description = description

    // FIXME(msimonin): sanity check those two ?
    // list of Member
    this.boundary = boundary

    this.surveyMessage = surveyMessage
    this.surveyCloneYear = surveyCloneYear
    this.commutesActive = commutesActive
    this.foodsActive = foodsActive
    this.surveyTags = surveyTags.map((obj) => Tag.createFromObj(obj))

    if (entity) {
      this.entity = entity
    } else {
      this.entity = {
        area: null,
        country: null,
        citySize: null
      }
    }

    this.submitted = makeSubmitted(submitted)
    // Create collections
    this.buildings = new ModuleCollection()
    this.purchases = new ModuleCollection()
    this.devices = new ModuleCollection()
    this.vehicles = new ModuleCollection()
    this.travels = new TravelCollection()
    this.commutes = new ModuleCollection()
    this.foods = new ModuleCollection()
    this.ractivities = new ModuleCollection()

    // Add objects to collections
    buildings.map((obj) => this.buildings.add(obj))
    purchases.map((obj) => this.purchases.add(obj))
    devices.map((obj) => this.devices.add(obj))
    vehicles.map((obj) => this.vehicles.add(obj))
    travels.map((obj) => this.travels.add(obj))
    commutes.map((obj) => this.commutes.add(obj))
    foods.map((obj) => this.foods.add(obj))
    ractivities.map((obj) => this.ractivities.add(obj))

    // computed properties
    this.synthesis = Synthesis.createFromObj(synthesis)
    this.emissions = emissions

    // keep track of the activeModules
    this.activeModulesObjs = activeModulesObjs
  }

  add (module, data) {
    // we have date but the corresponding module but it's not activated
    if (!this.activeModulesObjs.byType(module)) {
      return
    }

    // set the consumption data
    let dataObjs = data.map((d) =>
      this.activeModulesObjs.byType(module).loads(d)
    )
    dataObjs.forEach((b) => this[module].add(b))
  }

  get surveyActive () {
    return this.commutesActive || this.foodsActive
  }

  get workforce () {
    return this.boundary.workforce
  }

  get intensity () {
    let intensity = new CarbonIntensities()
    for (let module of this.activeModulesObjs) {
      if (Modules.MODULES[module.type].length === 0) {
        intensity.add(this.emissions[module.type].intensity)
      } else {
        for (let submodule of Modules.MODULES[module.type]) {
          intensity.add(this.emissions[module.type][submodule].intensity)
        }
      }
    }
    return intensity.sum()
  }

  get buildingsSubmitted () {
    let allSubmitted = true
    for (let module of Modules.MODULES[Modules.BUILDINGS]) {
      if (!this.submitted[module]) {
        allSubmitted = false
        break
      }
    }
    return allSubmitted
  }

  get hasOneBuildingsModuleSubmitted () {
    let hasOneSubmitted = false
    for (let module of Modules.MODULES[Modules.BUILDINGS]) {
      if (this.submitted[module]) {
        hasOneSubmitted = true
        break
      }
    }
    return hasOneSubmitted
  }

  get allModulesSubmitted () {
    let allSubmitted = true
    for (let module of Modules.getModules(true)) {
      if (!this.submitted[module]) {
        allSubmitted = false
        break
      }
    }
    return allSubmitted
  }

  updateSubmitted (module) {
    if (Object.keys(this.submitted).includes(module)) {
      this.submitted[module] = !this.submitted[module]
    }
  }

  updateAllSubmitted (value) {
    for (let module of Object.keys(this.submitted)) {
      this.submitted[module] = value
    }
  }

  activateDesactivateSurvey (module = null) {
    if (module === Modules.FOODS) {
      this.foodsActive = !this.foodsActive
    } else if (module === Modules.COMMUTES) {
      this.commutesActive = !this.commutesActive
    } else {
      this.foodsActive = !this.foodsActive
      this.commutesActive = !this.commutesActive
    }
  }

  toString (sep = '\t') {
    let value = [this.uuid, this.created, this.year].join(sep)
    value += sep + this.entity.toString(sep)
    value += sep + this.boundary.toString(sep) + sep
    return (
      value +
      sep +
      [
        this.submitted.vehicles,
        Math.round(this.synthesis.vehicles.intensity),
        Math.round(this.synthesis.vehicles.uncertainty),
        this.submitted.travels,
        Math.round(this.synthesis.travels.intensity),
        Math.round(this.synthesis.travels.uncertainty),
        this.submitted.commutes,
        Math.round(this.synthesis.commutes.intensity),
        Math.round(this.synthesis.commutes.uncertainty),
        this.submitted.foods,
        Math.round(this.synthesis.foods.intensity),
        Math.round(this.synthesis.foods.uncertainty),
        this.submitted.heatings,
        Math.round(this.synthesis.heatings.intensity),
        Math.round(this.synthesis.heatings.uncertainty),
        this.submitted.electricity,
        Math.round(this.synthesis.electricity.intensity),
        Math.round(this.synthesis.electricity.uncertainty),
        this.submitted.refrigerants,
        Math.round(this.synthesis.refrigerants.intensity),
        Math.round(this.synthesis.refrigerants.uncertainty),
        this.submitted.water,
        Math.round(this.synthesis.water.intensity),
        Math.round(this.synthesis.water.uncertainty),
        this.submitted.construction,
        Math.round(this.synthesis.construction.intensity),
        Math.round(this.synthesis.construction.uncertainty),
        this.submitted.devices,
        Math.round(this.synthesis.devices.intensity),
        Math.round(this.synthesis.devices.uncertainty),
        this.submitted.purchases,
        Math.round(this.synthesis.purchases.intensity),
        Math.round(this.synthesis.purchases.uncertainty),
        this.submitted.ractivities,
        Math.round(this.synthesis.ractivities.intensity),
        Math.round(this.synthesis.ractivities.uncertainty)
      ].join(sep)
    )
  }

  /**
   * Whether the module(type or subtype) needs to be computed
   * specification:
   * - if the module is not activated -> false
   * - null, y -> true
   * - x, y -> true iff the parent module of x is y (e.g electricity, buidings)
   *
   * @param {string} module
   * @param {string} type
   * @returns {boolean} - true iff the module needs to be computed
   */
  _shouldCompute (module, type) {
    let klass = this.activeModulesObjs.byType(type)?.klass
    let ok =
      klass && (!module || (module && Modules.getParentModule(module) === type))
    return ok
  }

  /**
   * Get the module class from which the static compute method will be called
   *
   * @param {string} type
   * @returns
   */
  _getModuleClass (type) {
    let klass = this.activeModulesObjs.byType(type)?.klass
    if (!klass) {
      throw new Error(`Don't know how the compute for ${type}`)
    }
    return klass
  }

  _computeBuildings (year, tags) {
    let klass = this._getModuleClass(Modules.BUILDINGS, this.activeModulesObjs)
    return klass.compute(
      this.buildings,
      this.entity.area,
      this.entity.country,
      year,
      tags
    )
  }

  _computeCommutes (year, tags, commuteSettings) {
    let klass = this._getModuleClass(Modules.COMMUTES, this.activeModulesObjs)
    return klass.compute(
      this.commutes,
      this.entity.citySize,
      this.boundary,
      year,
      commuteSettings,
      tags
    )
  }

  _computeFoods (year, tags, commuteSettings) {
    let klass = this._getModuleClass(Modules.FOODS, this.activeModulesObjs)
    return klass.compute(
      this.foods,
      this.boundary.workforce,
      year,
      commuteSettings,
      tags
    )
  }

  _computeVehicles (year, tags) {
    let klass = this._getModuleClass(Modules.VEHICLES, this.activeModulesObjs)
    return klass.compute(this.vehicles, year, tags)
  }

  _computeTravels (
    year,
    tags,
    { TRAVEL_POSITION_LABELS = [], TRAVEL_PURPOSE_LABELS = [] } = {}
  ) {
    let klass = this._getModuleClass(Modules.TRAVELS, this.activeModulesObjs)
    return klass.compute(
      this.travels,
      year,
      tags,
      TRAVEL_POSITION_LABELS,
      TRAVEL_PURPOSE_LABELS
    )
  }

  _computeDevices (tags) {
    let klass = this._getModuleClass(Modules.DEVICES, this.activeModulesObjs)
    return klass.compute(this.devices, tags)
  }

  _computePurchases (year, tags) {
    let klass = this._getModuleClass(Modules.PURCHASES, this.activeModulesObjs)
    return klass.compute(this.purchases, year, tags)
  }

  _computeRActivities (year, tags) {
    let klass = this._getModuleClass(
      Modules.RACTIVITIES,
      this.activeModulesObjs
    )
    return klass.compute(this.ractivities, year, tags)
  }

  compute (options = null, year = null, tags = [], module = null) {
    let cyear = this.year
    if (year !== null) {
      cyear = year
    }

    if (this._shouldCompute(module, Modules.BUILDINGS)) {
      this.emissions.buildings = this._computeBuildings(cyear, tags)
    }

    if (this._shouldCompute(module, Modules.COMMUTES)) {
      this.emissions.commutes = this._computeCommutes(cyear, tags, options)
    }

    if (this._shouldCompute(module, Modules.FOODS)) {
      this.emissions.foods = this._computeFoods(cyear, tags, options)
    }

    if (this._shouldCompute(module, Modules.VEHICLES)) {
      this.emissions.vehicles = this._computeVehicles(cyear, tags)
    }

    if (this._shouldCompute(module, Modules.TRAVELS)) {
      this.emissions.travels = this._computeTravels(cyear, tags, options)
    }

    if (this._shouldCompute(module, Modules.DEVICES)) {
      this.emissions.devices = this._computeDevices(tags)
    }

    if (this._shouldCompute(module, Modules.PURCHASES)) {
      this.emissions.purchases = this._computePurchases(cyear, tags)
    }

    if (this._shouldCompute(module, Modules.RACTIVITIES)) {
      this.emissions.ractivities = this._computeRActivities(cyear, tags)
    }

    // Update synthesis
    this.synthesis = Synthesis.createFromObj({
      heatings: this.emissions?.buildings?.heatings?.intensity,
      electricity:
        this.emissions?.buildings?.electricity?.intensity?.toCarbonIntensity(),
      refrigerants: this?.emissions?.buildings?.refrigerants?.intensity,
      water: this?.emissions?.buildings?.water?.intensity,
      construction: this?.emissions?.buildings?.construction?.intensity,
      commutes: this?.emissions?.commutes?.intensity,
      foods: this?.emissions?.foods?.intensity,
      travels: this?.emissions?.travels?.intensity,
      vehicles: this?.emissions?.vehicles?.intensity,
      devices: this?.emissions?.devices?.intensity,
      purchases: this?.emissions?.purchases?.intensity,
      ractivities: this?.emissions?.ractivities?.intensity
    })
  }

  static exportHeader (positionTitles, sep = '\t') {
    let value = ['ghgi.uuid', 'created.date', 'year'].join(sep)
    value += sep + Laboratory.exportHeader(sep)
    value += sep + Boundary.exportHeader(positionTitles, sep) + sep
    return (
      value +
      sep +
      [
        'vehicles.submitted',
        'vehicles.emission.kg.co2e',
        'vehicles.uncertainty.kg.co2e',
        'travels.submitted',
        'travels.emission.kg.co2e',
        'travels.uncertainty.kg.co2e',
        'commutes.submitted',
        'commutes.emission.kg.co2e',
        'commutes.uncertainty.kg.co2e',
        'foods.submitted',
        'foods.emission.kg.co2e',
        'foods.uncertainty.kg.co2e',
        'heating.submitted',
        'heating.emission.kg.co2e',
        'heating.uncertainty.kg.co2e',
        'electricity.submitted',
        'electricity.emission.kg.co2e',
        'electricity.uncertainty.kg.co2e',
        'refrigerants.submitted',
        'refrigerants.emission.kg.co2e',
        'refrigerants.uncertainty.kg.co2e',
        'water.submitted',
        'water.emission.kg.co2e',
        'water.uncertainty.kg.co2e',
        'construction.submitted',
        'construction.emission.kg.co2e',
        'construction.uncertainty.kg.co2e',
        'devices.submitted',
        'devices.emission.kg.co2e',
        'devices.uncertainty.kg.co2e',
        'purchases.submitted',
        'purchases.emission.kg.co2e',
        'purchases.uncertainty.kg.co2e',
        'ractivities.submitted',
        'ractivities.emission.kg.co2e',
        'ractivities.uncertainty.kg.co2e'
      ].join(sep)
    )
  }

  static exportToFile (
    positionTitles,
    items,
    header = true,
    extraColValue = null,
    sep = '\t'
  ) {
    let headerValues = []
    if (header) {
      headerValues = GHGI.exportHeader(positionTitles, sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  /**
   * Create a GHGI from an obj (json-like)
   * Deal only with first level attributes (except for entity though)
   *
   * To build a full-fledge GHGI make use of make @see {@link makeGHGI}
   *
   * @param {*} item
   * @returns
   */

  static createFromObj (item, activeModulesObjs) {
    let e = item.entity
    let entity = entityFactory(e.type).createFromObj(e.data)
    // activity data must be set outside of this function using the add* methods
    // boundary must also be set outside this function
    return new GHGI({
      id: item.id,
      uuid: item.uuid,
      year: item.year,
      created: item.created,
      description: item.description,
      surveyMessage: item.surveyMessage,
      surveyCloneYear: item.surveyCloneYear,
      commutesActive: item.commutesActive,
      foodsActive: item.foodsActive,
      surveyTags: item.surveyTags,
      entity: entity,
      submitted: item.submitted,
      synthesis: item.synthesis,
      emissions: item.emissions,
      activeModulesObjs: activeModulesObjs
    })
  }

  /**
   * Build a report for the various modules.
   * Also aggregates some intensities for the modules issued as parameter.
   *
   * /!\ the report is generated from the last computed emissions.
   * (one must call this.compute before calling this method)
   *
   * Aggregates definitions:
   * - proTravels: sum of the intensities of all professional travels (vehicules,
   *  travels/missions)
   * - allTravels: proTravels + commutes
   * - total: sum of the intensities
   *
   * @param {string[]} modules - list of modules to account for the various
   *  aggregates
   * @returns {Object} report (custom schema)
   */
  buildL1P5Report (modules) {
    let emissions = this.emissions

    let s = {
      buildings: {
        usage: new CarbonIntensity(),
        heatings: new CarbonIntensity(),
        electricity: new CarbonIntensity(),
        refrigerants: new CarbonIntensity(),
        water: new CarbonIntensity(),
        construction: new CarbonIntensity(),
        // NOTE(msimonin): original code was using a CarbonIntensities instead
        // since we get this information directly from ghgi.emissions we stick
        // with the datatype used in ghgi.emissions
        total: new CarbonIntensity()
      },
      vehicles: {
        total: new CarbonIntensity()
      },
      ractivities: {
        total: new CarbonIntensity()
      },
      devices: {
        total: new CarbonIntensity()
      },
      commutes: {
        total: new CarbonIntensity()
      },
      travels: {
        total: new CarbonIntensity()
      },
      purchases: {
        total: new CarbonIntensity()
      },
      foods: {
        total: new CarbonIntensity()
      },
      // aggregates
      proTravels: new CarbonIntensities(),
      allTravels: new CarbonIntensities(),
      total: new CarbonIntensities()
    }

    if (!emissions) {
      return s
    }

    let results = emissions.buildings
    if (results && modules.includes(Modules.BUILDINGS)) {
      s.buildings.heatings = results.heatings.intensity
      s.buildings.electricity = results.electricity.intensity
      s.buildings.refrigerants = results.refrigerants.intensity
      s.buildings.water = results.water.intensity
      s.buildings.construction = results.construction.intensity

      // usage only
      let usage = new CarbonIntensities()
      usage.add(results.heatings.intensity)
      usage.add(results.electricity.intensity)
      usage.add(results.water.intensity)
      usage.add(results.refrigerants.intensity)
      s.buildings.usage = usage.sum()

      // total of buildings
      let total = new CarbonIntensities()
      total.add(results.heatings.intensity)
      total.add(results.electricity.intensity)
      total.add(results.water.intensity)
      total.add(results.refrigerants.intensity)
      total.add(results.construction.intensity)
      s.buildings.total = total.sum()

      s.total.add(results.construction.intensity)
      s.total.add(usage.sum())
    }

    results = emissions.vehicles
    if (results && modules.includes(Modules.VEHICLES)) {
      s.vehicles.total = results.intensity

      s.proTravels.add(results.intensity)
      s.allTravels.add(results.intensity)
      s.total.add(results.intensity)
    }

    results = emissions.ractivities
    if (results && modules.includes(Modules.RACTIVITIES)) {
      s.ractivities.total = results.intensity

      s.total.add(results.intensity)
    }

    results = emissions.devices
    if (results && modules.includes(Modules.DEVICES)) {
      s.devices.total = results.intensity

      s.total.add(results.intensity)
    }

    results = emissions.commutes
    if (results && modules.includes(Modules.COMMUTES)) {
      s.commutes.total = results.intensity

      s.allTravels.add(results.intensity)
      s.total.add(results.intensity)
    }

    results = emissions.foods
    if (results && modules.includes(Modules.FOODS)) {
      s.foods.total = results.intensity

      s.total.add(results.intensity)
    }

    results = emissions.travels
    if (results && modules.includes(Modules.TRAVELS)) {
      let intensity = new CarbonIntensity(
        results.intensity.intensity,
        results.intensity.uncertainty
      )
      s.travels.total = intensity

      s.proTravels.add(intensity)
      s.allTravels.add(intensity)
      s.total.add(intensity)
    }

    results = emissions.purchases
    if (results && modules.includes(Modules.PURCHASES)) {
      s.purchases.total = results.intensity

      s.total.add(results.intensity)
    }
    return s
  }
}

export { GHGI as default }
