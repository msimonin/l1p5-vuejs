/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import VEHICLES_FACTORS from '@/../data/factors/carbon/vehiclesFactors.json'
import Model from '@/models/carbon/Model.js'
import { floatConverter } from '@/utils/parser.js'
import {
  DetailedCarbonIntensity,
  DetailedCarbonIntensities,
  CarbonIntensities,
  CarbonIntensity
} from '@/models/carbon/CarbonIntensity.js'
import { EmissionFactor } from '@/models/carbon/Factor.js'

const ICONS = {
  car: 'car',
  motorbike: 'motorcycle',
  bike: 'bicycle',
  scooter: 'electric-scooter',
  aircraft: 'plane',
  boat: 'ship'
}

const ICONS_PACK = {
  car: 'fa',
  motorbike: 'fa',
  bike: 'fa',
  scooter: 'icomoon',
  aircraft: 'fa',
  boat: 'fa'
}

const DEFAULT_TYPE = 'car'
const DEFAULT_ENGINE = 'gasoline'
const DEFAULT_UNIT = 'km'
const DEFAULT_NOENGINE = 1

export class Vehicle extends Model {
  constructor ({
    id = null,
    type = DEFAULT_TYPE,
    name = '',
    engine = DEFAULT_ENGINE,
    consumption = Vehicle.makeConsumption(),
    unit = DEFAULT_UNIT,
    power = 0,
    noEngine = DEFAULT_NOENGINE,
    shp = 0,
    controled = true,
    tags = [],
    source = null
  } = {}) {
    super({ tags, source })
    this.id = id
    this.name = name
    this.type = type
    this.engine = engine
    this.consumption = consumption
    this.unit = unit
    this.power = power
    this.noEngine = noEngine
    this.shp = shp
    this.controled = controled
    this.intensity = new CarbonIntensity()
  }

  get descriptor () {
    return this.name
  }

  isSimilarTo (other) {
    return this === other || this.uuid === other.uuid
  }

  static makeConsumption () {
    return {
      id: null,
      january: 0,
      february: 0,
      march: 0,
      april: 0,
      may: 0,
      june: 0,
      july: 0,
      august: 0,
      septembre: 0,
      octobre: 0,
      novembre: 0,
      decembre: 0,
      total: 0,
      isMonthly: true
    }
  }

  get score () {
    let score = 1
    if (!this.name || !this.type || !this.engine) {
      score = 2
    }
    if (!this.getConsumption()) {
      score = 3
    }
    return score
  }

  toString (sep = '\t') {
    return [
      this.name.replace('\t', ' '),
      this.type,
      this.engine,
      this.getConsumption(),
      this.unit,
      this.tags.map((val) => val.name).join(';'),
      Math.round(this.intensity.intensity),
      Math.round(this.intensity.uncertainty)
    ].join(sep)
  }

  isOther () {
    return false
  }

  isValid () {
    return this.score === 1
  }

  isIncomplete () {
    return this.score === 2
  }

  isInvalid () {
    return this.score === 3
  }

  _getHelicopterTConsumption (_shp) {
    let cCarburant = 0
    if (this.shp > 1000) {
      cCarburant =
        4.0539 * Math.pow(10, -18) * Math.pow(_shp, 5) -
        3.16298 * Math.pow(10, -14) * Math.pow(_shp, 4)
      cCarburant =
        cCarburant +
        9.2087 * Math.pow(10, -11) * Math.pow(_shp, 3) -
        1.2156 * Math.pow(10, -7) * Math.pow(_shp, 2)
      cCarburant = cCarburant + 1.1476 * Math.pow(10, -4) * _shp + 0.01256
    } else if (this.shp > 600) {
      cCarburant =
        3.3158 * Math.pow(10, -16) * Math.pow(_shp, 5) -
        1.0175 * Math.pow(10, -12) * Math.pow(_shp, 4)
      cCarburant =
        cCarburant +
        1.1627 * Math.pow(10, -9) * Math.pow(_shp, 3) -
        5.9528 * Math.pow(10, -7) * Math.pow(_shp, 2)
      cCarburant = cCarburant + 1.8168 * Math.pow(10, -4) * _shp + 0.0062945
    } else {
      cCarburant =
        2.197 * Math.pow(10, -15) * Math.pow(_shp, 5) -
        4.4441 * Math.pow(10, -12) * Math.pow(_shp, 4)
      cCarburant =
        cCarburant +
        3.4208 * Math.pow(10, -9) * Math.pow(_shp, 3) -
        1.2138 * Math.pow(10, -6) * Math.pow(_shp, 2)
      cCarburant = cCarburant + 2.414 * Math.pow(10, -4) * _shp + 0.004583
    }
    return this._getRowConsumption() * this.noEngine * 3600 * cCarburant * 1.25
  }

  _getHelicopterPConsumption (_shp) {
    let cCarburant =
      1.9 * Math.pow(10, -12) * Math.pow(_shp, 4) -
      Math.pow(10, -9) * Math.pow(_shp, 3)
    cCarburant =
      cCarburant +
      2.6 * Math.pow(10, -7) * Math.pow(_shp, 2) +
      4 * Math.pow(10, -5) * _shp +
      0.0006
    return this._getRowConsumption() * this.noEngine * 3600 * cCarburant * 1.39
  }

  _getPlaneConsumption () {
    return this._getRowConsumption() * (0.22 * this.power + 0.54)
  }

  _getRowConsumption () {
    let consumption = 0
    if (this.consumption.isMonthly) {
      consumption +=
        parseInt(this.consumption.january) + parseInt(this.consumption.february)
      consumption +=
        parseInt(this.consumption.march) + parseInt(this.consumption.april)
      consumption +=
        parseInt(this.consumption.may) + parseInt(this.consumption.june)
      consumption +=
        parseInt(this.consumption.july) + parseInt(this.consumption.august)
      consumption +=
        parseInt(this.consumption.septembre) +
        parseInt(this.consumption.octobre)
      consumption +=
        parseInt(this.consumption.novembre) +
        parseInt(this.consumption.decembre)
    } else {
      consumption = this.consumption.total
    }
    return consumption
  }

  sameAggregateAs (vehicle) {
    return (
      this.name === vehicle.name &&
      this.type === vehicle.type &&
      this.engine === vehicle.engine &&
      this.unit === vehicle.unit && // need special logic if removed
      this.power === vehicle.power &&
      this.noEngine === vehicle.noEngine &&
      this.shp === vehicle.shp &&
      this.controled === vehicle.controled // FIXME(msimonin): unused
    )
  }

  getConsumption () {
    let consumption = this._getRowConsumption()
    if (this.type === 'aircraft') {
      consumption = this.getAircraftConsumption()
    }
    return consumption
  }

  _doAggregate (vehicle) {
    let newVehicle = Vehicle.createFromObj(this)
    if (newVehicle.consumption.isMonthly) {
      newVehicle.consumption.january += vehicle.consumption.january
      newVehicle.consumption.february += vehicle.consumption.february
      newVehicle.consumption.march += vehicle.consumption.march
      newVehicle.consumption.april += vehicle.consumption.april
      newVehicle.consumption.may += vehicle.consumption.may
      newVehicle.consumption.june += vehicle.consumption.june
      newVehicle.consumption.july += vehicle.consumption.july
      newVehicle.consumption.august += vehicle.consumption.august
      newVehicle.consumption.septembre += vehicle.consumption.septembre
      newVehicle.consumption.octobre += vehicle.consumption.octobre
      newVehicle.consumption.novembre += vehicle.consumption.novembre
      newVehicle.consumption.decembre += vehicle.consumption.decembre
    } else {
      newVehicle.consumption.total += vehicle.consumption.total
    }
    return newVehicle
  }

  getAircraftConsumption () {
    let _shp = null
    let consumption = this._getPlaneConsumption()
    if (this.engine === 'helicopter.pistons') {
      _shp = this.shp * 0.9
      consumption = this._getHelicopterPConsumption(_shp)
    } else if (this.engine === 'helicopter.turbines') {
      if (this.noEngine === 1) {
        _shp = this.shp * 0.8
      } else {
        if (this.shp > 1000) {
          _shp = this.shp * 0.62
        } else {
          _shp = this.shp * 0.65
        }
      }
      consumption = this._getHelicopterTConsumption(_shp)
    }
    return consumption
  }

  getEmissionFactor (year) {
    // let ef = filterEFByYear(VEHICLES_FACTORS[this.type][this.engine], year)
    // return ef
    let ef = EmissionFactor.createFromObj(
      VEHICLES_FACTORS[this.type][this.engine]
    )
    return ef.getFactor(year)
  }

  getCarbonIntensity (year) {
    let consumption = this.getConsumption()
    let ef = this.getEmissionFactor(year)
    let intensity = new DetailedCarbonIntensity(
      consumption * ef.combustion.co2,
      consumption * ef.combustion.ch4,
      consumption * ef.combustion.n2o,
      consumption * ef.combustion.other,
      consumption * ef.combustion.total,
      consumption * ef.combustion.total * ef.combustion.uncertainty,
      consumption * ef.upstream.total,
      consumption * ef.upstream.total * ef.upstream.uncertainty,
      consumption * ef.manufacturing.total,
      consumption * ef.manufacturing.total * ef.manufacturing.uncertainty,
      ef.group
    )
    this.intensity = intensity
    return intensity
  }

  toDatabase () {
    return {
      id: this.id,
      name: this.name,
      type: this.type,
      engine: this.engine,
      consumption: this.consumption,
      unit: this.unit,
      power: this.power,
      noEngine: this.noEngine,
      shp: this.shp,
      controled: this.controled,
      tags: this.tags
    }
  }

  static get formatDescription () {
    return {
      name: {
        pattern: /^nom$|^name$|^identifiant$|^id$|^vehicule$|^vehicle$/i,
        required: true
      },
      type: {
        pattern: /^type$/i,
        required: true,
        converter: function (value) {
          const PATTERNS = {
            car: /voiture|car/i,
            motorbike: /moto|motorbike/i,
            bike: /v.lo|bike|bicycle/i,
            scooter: /trotinette|trottinette|scooter/i
          }
          let toReturn = ''
          for (let name of Object.keys(PATTERNS)) {
            if (value.search(PATTERNS[name]) >= 0) {
              toReturn = name
            }
          }
          return toReturn
        }
      },
      engine: {
        pattern: /^motorisation$|^engine$/i,
        required: true,
        converter: function (value) {
          const PATTERNS = {
            gasoline: /essence|gasoline/i,
            diesel: /diesel|gazole/i,
            'unknown.engine': /inconnue|inconnu|unknown/i,
            lpg: /GPL|LPG/i,
            cng: /GNV|CNG/i,
            e85: /E85/i,
            hybrid: /hybride|hybrid/i,
            electric: /.lectrique|electric/i,
            muscular: /musculaire|muscular/i
          }
          let toReturn = ''
          for (let name of Object.keys(PATTERNS)) {
            if (value.search(PATTERNS[name]) >= 0) {
              toReturn = name
            }
          }
          return toReturn
        }
      },
      distance: {
        pattern: /^distance$|^km$/i,
        converter: floatConverter,
        required: true
      },
      tags: {
        pattern: /(^tag$|^tags$|^label$|^labels$)/i,
        converter: function (value) {
          return value.split(',').map((val) => val.trim())
        },
        default: []
      }
    }
  }

  static getIcon (type) {
    return ICONS[type]
  }

  static getIconPack (type) {
    return ICONS_PACK[type]
  }

  static getTypes () {
    let types = Object.keys(VEHICLES_FACTORS)
    return Array.from(new Set(types))
  }

  static getEngines (type) {
    let engines = Object.keys(VEHICLES_FACTORS[type])
    return Array.from(new Set(engines))
  }

  static getUnits (type, engine) {
    return VEHICLES_FACTORS[type][engine].unit
  }

  static createFromObj (item) {
    return new Vehicle(item)
  }

  /**
   * @param {Vehicle[]} lines - The list of vehicle object to handle
   * @param {String} format - The file format
   * @param {String} tags - The list of tags defined by the user
   * @param {String} source - The source file name
   *
   * @returns {Vehicle[]} - The list of vehicles objects
   */
  static createFromLines (lines, format, tags, source) {
    let vehicles = []
    for (let line of lines) {
      let nitem = Vehicle.createFromObj({
        id: null,
        name: line.name,
        type: line.type,
        engine: line.engine,
        consumption: {
          id: null,
          january: 0,
          february: 0,
          march: 0,
          april: 0,
          may: 0,
          june: 0,
          july: 0,
          august: 0,
          septembre: 0,
          octobre: 0,
          novembre: 0,
          decembre: 0,
          total: parseInt(line.distance) || 0,
          isMonthly: false
        },
        unit: 'km',
        power: 0,
        noEngine: 1,
        shp: 0,
        controled: true,
        source: source
      })
      nitem.addTags(Model.getTagsFromNames(line.tags, tags))
      // Force engine value for moto and scooter
      if (nitem.type === 'moto') {
        nitem.engine = 'gasoline'
      }
      if (nitem.type === 'scooter') {
        nitem.engine = 'electric'
      }
      vehicles.push(nitem)
    }
    return vehicles
  }

  static exportHeader (sep = '\t') {
    return [
      'name',
      'type',
      'engine',
      'consumption',
      'unit',
      'tags',
      'emission.kg.co2e',
      'uncertainty.kg.co2e'
    ].join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = Vehicle.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static compute (items, year, tags = []) {
    let controled = new DetailedCarbonIntensities()
    let uncontroled = new DetailedCarbonIntensities()
    let intensities = new CarbonIntensities()
    for (let item of items.filter((obj) => obj.hasTags(tags))) {
      let intensity = item.getCarbonIntensity(year)
      intensities.add(intensity)
      if (item.controled) {
        controled.add(intensity)
      } else {
        uncontroled.add(intensity)
      }
    }
    return {
      controled: controled.sum(),
      uncontroled: uncontroled.sum(),
      intensity: intensities.sum()
    }
  }
}

export {
  Vehicle as default,
  DEFAULT_ENGINE,
  DEFAULT_NOENGINE,
  DEFAULT_TYPE,
  DEFAULT_UNIT
}
