/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Meal from '@/models/carbon/Meal.js'
import CommuteSection from '@/models/carbon/CommuteSection.js'
import Model from '@/models/carbon/Model.js'

export default class Survey extends Model {
  constructor ({
    id = null,
    seqID = null,
    position = null,
    sections = [],
    meals = [],
    nWorkingDay = null,
    nWorkingDay2 = null,
    nCateringMeal = null,
    message = null,
    cdeleted = false,
    mdeleted = false,
    tags = []
  } = {}) {
    super({ tags })
    this.id = id
    this.seqID = seqID
    this.position = position
    if (sections) {
      this.sections = sections.map((obj) => CommuteSection.createFromObj(obj))
    } else {
      this.sections = []
    }
    if (meals) {
      this.meals = meals.map((obj) => Meal.createFromObj(obj))
    } else {
      this.meals = []
    }
    this.nWorkingDay = nWorkingDay
    this.nWorkingDay2 = nWorkingDay2
    this.nCateringMeal = nCateringMeal
    this.message = message
    this.cdeleted = cdeleted
    this.mdeleted = mdeleted
  }

  get descriptor () {
    return this.seqID
  }

  /**
   * Answer are assigned a seqId by default so we're matching on this.
   * @param {*} other
   */
  isSimilarTo (other) {
    return this === other || this.seqID === other.seqID
  }

  removeSection (mode, isDay2 = false) {
    let index = this.sections.findIndex(
      (obj) => obj.mode === mode && obj.isDay2 === isDay2
    )
    this.sections.splice(index, 1)
  }

  removeAllSectionsDay2 () {
    for (let section of this.sections) {
      if (section.isDay2) {
        this.removeSection(section.mode, true)
      }
    }
    this.nWorkingDay2 = 0
  }

  clean () {
    if (this.nWorkingDay === 0) {
      this.meals = []
      this.sections = []
      this.nWorkingDay2 = 0
    } else if (this.nWorkingDay === 1) {
      this.nWorkingDay2 = 0
      this.removeAllSectionsDay2()
    }
    if (this.nWorkingDay2 === 0) {
      this.removeAllSectionsDay2()
    }
    this.meals = this.meals.filter((obj) => obj.amount !== 0)
  }

  toDatabase () {
    return {
      id: this.id,
      seqID: this.seqID,
      position: this.position,
      sections: this.sections,
      meals: this.meals,
      nWorkingDay: this.nWorkingDay,
      nWorkingDay2: this.nWorkingDay2,
      nCateringMeal: this.nCateringMeal,
      message: this.message,
      cdeleted: this.cdeleted,
      mdeleted: this.mdeleted,
      tags: this.tags
    }
  }

  static createFromObj (obj) {
    return new Survey({
      id: obj.id,
      seqId: obj.seqID,
      position: obj.position,
      sections: obj.sections,
      meals: obj.meals,
      nWorkingDay: obj.nWorkingDay,
      nWorkingDay2: obj.nWorkingDay2,
      nCateringMeal: obj.nCateringMeal,
      message: obj.message,
      cdeleted: obj.cdeleted,
      mdeleted: obj.mdeleted,
      tags: obj.tags
    })
  }

  static createEmpty () {
    return new Survey()
  }

  getNumberOfWorkedWeeks (
    { NUMBER_OF_WORKED_WEEKS = { default: 0 } } = {},
    year
  ) {
    // let nww = settings.NUMBER_OF_WORKED_WEEK
    // let dfault = nww["default"]
    // nww[year]

    let nbWeeks = 0
    if (parseInt(year) in NUMBER_OF_WORKED_WEEKS) {
      nbWeeks = NUMBER_OF_WORKED_WEEKS[year]
    } else {
      nbWeeks = NUMBER_OF_WORKED_WEEKS['default']
    }
    return nbWeeks
  }
}
