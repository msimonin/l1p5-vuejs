import { Configuration } from '@/models/carbon/Configuration.js'
import Food from './Food'
import { Tag } from '@/models/core/TagCategory.js'

import { readFile } from 'fs/promises'
const path = require('path')

const fakeDataDir = path.resolve(__dirname, '../../..', 'backend/carbon/fake')
const FAKE_CONF_PATH = path.join(fakeDataDir, 'fakeConf.json')

function foodObj (modifier = undefined) {
  let food = {
    seqID: null,
    position: null,
    meals: [],
    nWorkingDay: null,
    nWorkingDay2: null,
    message: null,
    mdeleted: false,
    tags: []
  }

  if (modifier === undefined) {
    modifier = {}
  }

  for (let [key, value] of Object.entries(modifier)) {
    food[key] = value
  }
  return Food.createFromObj(food)
}

describe('getNumberOfWorkedWeeks', () => {
  let conf = null

  beforeEach(async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)
  })

  test('2021 number of weeks', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: []
    })
    expect(food.getNumberOfWorkedWeeks(conf.toOptions(), 2021)).toBe(37)
  })
  test('2022 number of weeks', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: []
    })
    expect(food.getNumberOfWorkedWeeks(conf.toOptions(), 2022)).toBe(41)
  })
})

describe('getAnualFactor', () => {
  let conf = null

  beforeEach(async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)
  })

  test('2021 annual factor', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: []
    })
    expect(food.getAnualFactor(conf.toOptions(), 2021)).toBe((37 * 5) / 10)
  })
})

describe('getCarbonIntensity', () => {
  let conf = null

  beforeEach(async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)
  })

  test('10 Vegetarian meals', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: [
        {
          type: 'vegetarian',
          amount: 10
        }
      ]
    })
    let annualFactor = (37 * 5) / 10
    expect(food.getCarbonIntensity(conf.toOptions(), 2021).intensity).toBe(
      1.115 * 10 * annualFactor
    )
  })
  test('5 Vegetarian meals + 5 Classic meal with meet 1', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: [
        {
          type: 'vegetarian',
          amount: 5
        },
        {
          type: 'classic.meet1',
          amount: 5
        }
      ]
    })
    let annualFactor = (37 * 5) / 10
    expect(food.getCarbonIntensity(conf.toOptions(), 2021).intensity).toBe(
      1.115 * 5 * annualFactor + 2.098 * 5 * annualFactor
    )
  })
})

describe('Food.compute', () => {
  let conf = null

  beforeEach(async () => {
    let confPayload = await readFile(FAKE_CONF_PATH, 'utf8')
    conf = Configuration.fromDatabase(JSON.parse(confPayload).settings)
  })

  test('10 vegetarian meals for 20 lab members', () => {
    let food = foodObj({
      nWorkingDay: 5,
      position: 'engineer',
      meals: [
        {
          type: 'vegetarian',
          amount: 10
        }
      ]
    })
    let annualFactor = (37 * 5) / 10

    let r = Food.compute([food], 20, 2021, conf.toOptions())
    expect(r).toEqual(
      expect.objectContaining({
        intensity: expect.objectContaining({
          intensity: 1.115 * 10 * 20 * annualFactor,
          uncertainty: 1.115 * 10 * 20 * annualFactor * 0.5,
          group: null
        })
      })
    )
  })

  test('10 vegetarian (tag1) + 10 classic.meat2 (tag2) + 10 classic.fish1 (untaged) meals for 20 lab members', () => {
    let foods = [
      foodObj({
        nWorkingDay: 5,
        position: 'engineer',
        meals: [
          {
            type: 'vegetarian',
            amount: 10
          }
        ],
        tags: [Tag.createFromObj('tag1')]
      }),
      foodObj({
        nWorkingDay: 5,
        position: 'engineer',
        meals: [
          {
            type: 'classic.meet2',
            amount: 10
          }
        ],
        tags: [Tag.createFromObj('tag2')]
      }),
      foodObj({
        nWorkingDay: 5,
        position: 'engineer',
        meals: [
          {
            type: 'classic.fish1',
            amount: 10
          }
        ],
        tags: []
      })
    ]
    // 37 weeks, 5 days a week
    let annualFactor = 37 * 5

    let r = Food.compute(foods, 15, 2021, conf.toOptions())
    // 1/3 tag1 + 1/3 tag2 + 1/3 untagged > mean of the 3 meals multiplied by 15 members
    expect(r.intensity.intensity).toBeCloseTo(
      (((1.115 + 5.51 + 1.63) * annualFactor) / 3) * 15,
      5
    )
    expect(r.intensity.uncertainty).toBeCloseTo(
      (Math.sqrt(
        Math.pow(1.115 * annualFactor * 0.5, 2) +
          Math.pow(5.51 * annualFactor * 0.5, 2) +
          Math.pow(1.63 * annualFactor * 0.5, 2)
      ) /
        3) *
        15,
      5
    )

    // Check tag1, 1/3 of the answers
    let r1 = Food.compute(foods, 15, 2021, conf.toOptions(), [
      Tag.createFromObj('tag1')
    ])
    expect(r1.intensity.intensity).toEqual(((1.115 * annualFactor) / 3) * 15)
    expect(r1.intensity.uncertainty).toBeCloseTo(
      ((1.115 * annualFactor) / 3) * 15 * 0.5,
      5
    )

    // Check tag2, 1/3 of the answers
    let r2 = Food.compute(foods, 15, 2021, conf.toOptions(), [
      Tag.createFromObj('tag2')
    ])
    expect(r2.intensity.intensity).toBeCloseTo(
      ((5.51 * annualFactor) / 3) * 15,
      5
    )
    expect(r2.intensity.uncertainty).toBeCloseTo(
      ((5.51 * annualFactor) / 3) * 15 * 0.5,
      5
    )

    // Check untagged, 1/3 of the answers
    let r3 = Food.compute(foods, 15, 2021, conf.toOptions(), [
      Tag.createFromObj('untagged')
    ])
    expect(r3.intensity.intensity).toBeCloseTo(
      ((1.63 * annualFactor) / 3) * 15,
      5
    )
    expect(r3.intensity.uncertainty).toBeCloseTo(
      ((1.63 * annualFactor) / 3) * 15 * 0.5,
      5
    )

    // Check sum tagged + untagged = without filters
    expect(r.intensity.intensity).toEqual(
      r1.intensity.intensity + r2.intensity.intensity + r3.intensity.intensity
    )
  })
})
