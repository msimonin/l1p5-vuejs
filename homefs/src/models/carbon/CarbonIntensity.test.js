import {
  CarbonIntensity,
  DetailedCarbonIntensity,
  CarbonIntensities,
  DetailedCarbonIntensities
} from './CarbonIntensity'

describe('CarbonIntensity', () => {
  test('constructor with default values', () => {
    let c = new CarbonIntensity()
    expect(c.intensity).toBe(0)
    expect(c.uncertainty).toBe(0)
    expect(c.group).toBe(null)
  })

  test('test multiply', () => {
    let c = new CarbonIntensity(1, 2)
    c = c.multiply(3)
    expect(c.intensity).toBe(3)
    expect(c.uncertainty).toBe(6)
  })
})

describe('DetailedCarbonIntensity', () => {
  test('constructor with default values', () => {
    let dc = new DetailedCarbonIntensity()
    expect(dc.co2).toBe(0)
    expect(dc.ch4).toBe(0)
    expect(dc.n2o).toBe(0)
    expect(dc.other).toBe(0)
    for (let key of ['combustion', 'upstream', 'manufacturing']) {
      expect(dc[key].intensity).toBe(0)
      expect(dc[key].uncertainty).toBe(0)
    }
    expect(dc.group).toBe(null)
  })

  test('constructor with non defaults values', () => {
    let dc = new DetailedCarbonIntensity(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'test')
    expect(dc.co2).toBe(1)
    expect(dc.ch4).toBe(2)
    expect(dc.n2o).toBe(3)
    expect(dc.other).toBe(4)
    let i = 0
    for (let key of ['combustion', 'upstream', 'manufacturing']) {
      expect(dc[key].intensity).toBe(5 + 2 * i)
      expect(dc[key].uncertainty).toBe(6 + 2 * i)
      i++
    }
    expect(dc.group).toBe('test')
  })

  test('Getters sum the combusion, upstream and manufacturing', () => {
    let dc = new DetailedCarbonIntensity(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'test')
    expect(dc.intensity).toBe(5 + 7 + 9)
    expect(dc.uncertainty).toBe(6 + 8 + 10)
  })

  test('toCarbonIntensity', () => {
    let dc = new DetailedCarbonIntensity(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'test')
    let c = dc.toCarbonIntensity()
    expect(c.intensity).toBe(5 + 7 + 9)
    expect(c.uncertainty).toBe(6 + 8 + 10)
  })
})

describe('CarbonIntensities', () => {
  test('Empty collection', () => {
    let cs = new CarbonIntensities()
    expect(cs.intensity).toBe(0)
    expect(cs.uncertainty).toBe(0)
    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(0)
    expect(c.uncertainty).toBe(0)
  })

  test('We can add a Carbon intensity (group == null)', () => {
    let cs = new CarbonIntensities()
    cs.add(new CarbonIntensity(1, 2))

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1)
    expect(c.uncertainty).toBe(2)
  })

  test('We can add a Carbon intensity (group != null)', () => {
    let cs = new CarbonIntensities()
    cs.add(new CarbonIntensity(1, 2, 'grp1'))

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1)
    expect(c.uncertainty).toBe(2)
  })

  // NOTE(msimonin): not clear yet why this choice was made
  test('2 with group  == null are treated as independant', () => {
    let cs = new CarbonIntensities()
    cs.add(new CarbonIntensity(1, 2))
    cs.add(new CarbonIntensity(5, 6))

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1 + 5)
    expect(c.uncertainty).toBe(Math.sqrt(2 * 2 + 6 * 6))
  })

  test('2 with different groups are treated as independant', () => {
    let cs = new CarbonIntensities()
    cs.add(new CarbonIntensity(1, 2, 'grp1'))
    cs.add(new CarbonIntensity(5, 6, 'grp2'))

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1 + 5)
    expect(c.uncertainty).toBe(Math.sqrt(2 * 2 + 6 * 6))
  })

  test('2 with same groups are treated as dependant', () => {
    let cs = new CarbonIntensities()
    cs.add(new CarbonIntensity(1, 2, 'grp1'))
    cs.add(new CarbonIntensity(5, 6, 'grp1'))

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1 + 5)
    expect(c.uncertainty).toBe(2 + 6)
  })

  // NOTE(msimonin): not clear yet why this choice was made
  test('Adding different groups (uncertainty is a quadratic sum over the groups)', () => {
    let cs = new CarbonIntensities()
    cs.add(new CarbonIntensity(1, 2, null))
    cs.add(new CarbonIntensity(5, 6, 'grp1'))
    cs.add(new CarbonIntensity(9, 10, 'grp2'))

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1 + 5 + 9)
    expect(c.uncertainty).toBe(Math.sqrt(2 * 2 + 6 * 6 + 10 * 10))
  })

  test('We can add a CarbonIntensities', () => {
    let cs = new CarbonIntensities()
    let subCs = new CarbonIntensities()
    subCs.add(new CarbonIntensity(1, 2))
    cs.add(subCs)

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1)
    expect(c.uncertainty).toBe(2)
  })

  test('We can add a CarbonIntensities null groups are treated as dependants', () => {
    let cs = new CarbonIntensities()
    let subCs = new CarbonIntensities()
    subCs.add(new CarbonIntensity(1, 2))
    cs.add(new CarbonIntensity(5, 6))
    cs.add(subCs)

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1 + 5)
    expect(c.uncertainty).toBe(2 + 6)
  })

  test('We can add a CarbonIntensities same group', () => {
    let cs = new CarbonIntensities()
    let subCs = new CarbonIntensities()
    subCs.add(new CarbonIntensity(1, 2, 'grp1'))
    subCs.add(new CarbonIntensity(5, 6, 'grp1'))
    cs.add(new CarbonIntensity(1, 2, 'grp1'))
    cs.add(new CarbonIntensity(5, 6, 'grp1'))
    cs.add(subCs)

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1 + 5 + 1 + 5)
    expect(c.uncertainty).toBe(2 + 6 + 2 + 6)
  })

  test('We can add a CarbonIntensities different group', () => {
    let cs = new CarbonIntensities()
    let subCs = new CarbonIntensities()
    subCs.add(new CarbonIntensity(1, 2, 'grp1'))
    subCs.add(new CarbonIntensity(5, 6, 'grp2'))
    cs.add(new CarbonIntensity(1, 2, 'grp3'))
    cs.add(new CarbonIntensity(5, 6, 'grp4'))
    cs.add(subCs)

    let c = cs.sum()
    expect(c instanceof CarbonIntensity).toBeTruthy()
    expect(c.intensity).toBe(1 + 5 + 1 + 5)
    expect(c.uncertainty).toBe(Math.sqrt(2 * 2 + 6 * 6 + 2 * 2 + 6 * 6))
  })
})

describe('DetailedCarbonIntensities', () => {
  test('Empty DetailedCarbonIntensities', () => {
    let dcs = new DetailedCarbonIntensities()

    let dc = dcs.sum()
    expect(dc instanceof DetailedCarbonIntensity).toBeTruthy()
    expect(dc.co2).toBe(0)
    expect(dc.ch4).toBe(0)
    expect(dc.n2o).toBe(0)
    expect(dc.other).toBe(0)
    expect(dc.combustion.intensity).toBe(0)
    expect(dc.combustion.uncertainty).toBe(0)
    expect(dc.upstream.intensity).toBe(0)
    expect(dc.upstream.uncertainty).toBe(0)
    expect(dc.manufacturing.intensity).toBe(0)
    expect(dc.manufacturing.uncertainty).toBe(0)
  })

  test('We can add a DetailedCarbonIntensity (group == null)', () => {
    let dcs = new DetailedCarbonIntensities()

    dcs.add(new DetailedCarbonIntensity(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    let dc = dcs.sum()
    expect(dc instanceof DetailedCarbonIntensity).toBeTruthy()
    expect(dc.co2).toBe(1)
    expect(dc.ch4).toBe(2)
    expect(dc.n2o).toBe(3)
    expect(dc.other).toBe(4)
    expect(dc.combustion.intensity).toBe(5)
    expect(dc.combustion.uncertainty).toBe(6)
    expect(dc.upstream.intensity).toBe(7)
    expect(dc.upstream.uncertainty).toBe(8)
    expect(dc.manufacturing.intensity).toBe(9)
    expect(dc.manufacturing.uncertainty).toBe(10)
  })

  test('We can add a DetailedCarbonIntensity (group != null)', () => {
    let dcs = new DetailedCarbonIntensities()

    dcs.add(new DetailedCarbonIntensity(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'grp1'))
    let dc = dcs.sum()
    expect(dc instanceof DetailedCarbonIntensity).toBeTruthy()
    expect(dc.co2).toBe(1)
    expect(dc.ch4).toBe(2)
    expect(dc.n2o).toBe(3)
    expect(dc.other).toBe(4)
    expect(dc.combustion.intensity).toBe(5)
    expect(dc.combustion.uncertainty).toBe(6)
    expect(dc.upstream.intensity).toBe(7)
    expect(dc.upstream.uncertainty).toBe(8)
    expect(dc.manufacturing.intensity).toBe(9)
    expect(dc.manufacturing.uncertainty).toBe(10)
  })

  // NOTE(msimonin): not clear yet why this choice was made
  test('2 with group  == null are treated as independant', () => {
    let dcs = new DetailedCarbonIntensities()

    dcs.add(new DetailedCarbonIntensity(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
    dcs.add(new DetailedCarbonIntensity(11, 12, 13, 14, 15, 16, 17, 18, 19, 20))

    let dc = dcs.sum()
    expect(dc instanceof DetailedCarbonIntensity).toBeTruthy()
    expect(dc.co2).toBe(1 + 11)
    expect(dc.ch4).toBe(2 + 12)
    expect(dc.n2o).toBe(3 + 13)
    expect(dc.other).toBe(4 + 14)
    expect(dc.combustion.intensity).toBe(5 + 15)
    expect(dc.combustion.uncertainty).toBe(Math.sqrt(6 * 6 + 16 * 16))
    expect(dc.upstream.intensity).toBe(7 + 17)
    expect(dc.upstream.uncertainty).toBe(Math.sqrt(8 * 8 + 18 * 18))
    expect(dc.manufacturing.intensity).toBe(9 + 19)
    expect(dc.manufacturing.uncertainty).toBe(Math.sqrt(10 * 10 + 20 * 20))
  })

  test('2 with different groups are treated as independant', () => {
    let dcs = new DetailedCarbonIntensities()

    dcs.add(new DetailedCarbonIntensity(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'grp1'))
    dcs.add(
      new DetailedCarbonIntensity(
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        'grp2'
      )
    )

    let dc = dcs.sum()
    expect(dc instanceof DetailedCarbonIntensity).toBeTruthy()
    expect(dc.co2).toBe(1 + 11)
    expect(dc.ch4).toBe(2 + 12)
    expect(dc.n2o).toBe(3 + 13)
    expect(dc.other).toBe(4 + 14)
    expect(dc.combustion.intensity).toBe(5 + 15)
    expect(dc.combustion.uncertainty).toBe(Math.sqrt(6 * 6 + 16 * 16))
    expect(dc.upstream.intensity).toBe(7 + 17)
    expect(dc.upstream.uncertainty).toBe(Math.sqrt(8 * 8 + 18 * 18))
    expect(dc.manufacturing.intensity).toBe(9 + 19)
    expect(dc.manufacturing.uncertainty).toBe(Math.sqrt(10 * 10 + 20 * 20))
  })

  test('2 with same groups are treated as dependant', () => {
    let dcs = new DetailedCarbonIntensities()

    dcs.add(new DetailedCarbonIntensity(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'grp1'))
    dcs.add(
      new DetailedCarbonIntensity(
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        'grp1'
      )
    )

    let dc = dcs.sum()
    expect(dc instanceof DetailedCarbonIntensity).toBeTruthy()
    expect(dc.co2).toBe(1 + 11)
    expect(dc.ch4).toBe(2 + 12)
    expect(dc.n2o).toBe(3 + 13)
    expect(dc.other).toBe(4 + 14)
    expect(dc.combustion.intensity).toBe(5 + 15)
    expect(dc.combustion.uncertainty).toBe(6 + 16)
    expect(dc.upstream.intensity).toBe(7 + 17)
    expect(dc.upstream.uncertainty).toBe(8 + 18)
    expect(dc.manufacturing.intensity).toBe(9 + 19)
    expect(dc.manufacturing.uncertainty).toBe(10 + 20)
  })

  test('Adding different groups (uncertainty is a quadratic sum over the groups)', () => {
    let dcs = new DetailedCarbonIntensities()

    dcs.add(new DetailedCarbonIntensity(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, null))
    dcs.add(
      new DetailedCarbonIntensity(
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        'grp1'
      )
    )
    dcs.add(
      new DetailedCarbonIntensity(
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
        'grp2'
      )
    )

    let dc = dcs.sum()
    expect(dc instanceof DetailedCarbonIntensity).toBeTruthy()
    expect(dc.co2).toBe(1 + 11 + 21)
    expect(dc.ch4).toBe(2 + 12 + 22)
    expect(dc.n2o).toBe(3 + 13 + 23)
    expect(dc.other).toBe(4 + 14 + 24)
    expect(dc.combustion.intensity).toBe(5 + 15 + 25)
    expect(dc.combustion.uncertainty).toBe(Math.sqrt(6 * 6 + 16 * 16 + 26 * 26))
    expect(dc.upstream.intensity).toBe(7 + 17 + 27)
    expect(dc.upstream.uncertainty).toBe(Math.sqrt(8 * 8 + 18 * 18 + 28 * 28))
    expect(dc.manufacturing.intensity).toBe(9 + 19 + 29)
    expect(dc.manufacturing.uncertainty).toBe(
      Math.sqrt(10 * 10 + 20 * 20 + 30 * 30)
    )
  })
})
