/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Travel from '@/models/carbon/Travel.js'
import VEHICLES_FACTORS from '@/../data/factors/carbon/vehiclesFactors.json'
import TRANSPORTS_FACTORS from '@/../data/factors/carbon/transportsFactors.json'
import { CarbonIntensity } from '@/models/carbon/CarbonIntensity.js'
import { EmissionFactor } from '@/models/carbon/Factor.js'
import { booleanConverter } from '@/utils/parser.js'

export default class TravelSection {
  constructor ({
    transportation = null,
    departureCity = null,
    departureCountry = null,
    destinationCity = null,
    destinationCountry = null,
    carpooling = 1,
    distance = 0,
    departureCityLat = 0.0,
    departureCityLng = 0.0,
    destinationCityLat = 0.0,
    destinationCityLng = 0.0,
    type = null,
    isRoundTrip = false
  } = {}) {
    this.setTransportation(transportation)
    this.departureCity = departureCity
    this.setDepartureCountry(departureCountry)
    this.destinationCity = destinationCity
    this.setDestinationCountry(destinationCountry)
    this.setCarpooling(carpooling)
    this.setDepartureCityLat(departureCityLat)
    this.setDepartureCityLng(departureCityLng)
    this.setDestinationCityLat(destinationCityLat)
    this.setDestinationCityLng(destinationCityLng)
    // this.distance = distance
    this.__distance = distance
    if (type !== null) {
      this.type = type
    } else {
      this.computeType()
    }
    this.setIsRoundTrip(isRoundTrip)
    this.intensity = new CarbonIntensity()
  }

  isFromDatabase () {
    return (
      this.departureCity === null &&
      this.destinationCity === null &&
      this.departureCountry === null &&
      this.destinationCountry === null &&
      // null is an acceptable value in the DB
      (this.departureCityLat === 0 || this.departureCityLat === null) &&
      (this.departureCityLng === 0 || this.departureCityLng === null) &&
      (this.destinationCityLat === 0 || this.destinationCityLat === null) &&
      (this.destinationCityLng === 0 || this.destinationCityLng === null)
    )
  }

  setDepartureCountry (value) {
    this.departureCountry = Travel.iso3166Country(value)
  }

  setDestinationCountry (value) {
    this.destinationCountry = Travel.iso3166Country(value)
  }

  setTransportation (value) {
    this.transportation = Travel.getTransportation(value)
  }

  setCarpooling (value) {
    if (isNaN(value) || value === null || value === 'null' || value === '') {
      this.carpooling = 1
    } else if (value === '0' || value === 0) {
      this.carpooling = parseInt(value)
    } else {
      this.carpooling = parseInt(value)
    }
  }

  setIsRoundTrip (value) {
    if (typeof value === 'boolean') {
      this.isRoundTrip = value
    } else {
      this.isRoundTrip = booleanConverter(value)
    }
  }

  setDepartureCityLat (value) {
    if (value === null) {
      this.setDepartureCountry(null)
    }
    this.departureCityLat = value
  }

  setDepartureCityLng (value) {
    if (value === null) {
      this.setDepartureCountry(null)
    }
    this.departureCityLng = value
  }

  setDestinationCityLat (value) {
    if (value === null) {
      this.setDestinationCountry(null)
    }
    this.destinationCityLat = value
  }

  setDestinationCityLng (value) {
    if (value === null) {
      this.setDestinationCountry(null)
    }
    this.destinationCityLng = value
  }

  get score () {
    let score = 1
    if (this.departureCountry === null) {
      score = 2
    } else if (this.destinationCountry === null) {
      score = 2
    } else if (this.transportation === null) {
      score = 2
    } else if (this.carpooling === 0) {
      score = 2
    } else if (typeof this.isRoundTrip !== 'boolean') {
      score = 2
    } else if (this.departureCityLat === null) {
      score = 2
    } else if (this.departureCityLng === null) {
      score = 2
    } else if (this.destinationCityLat === null) {
      score = 2
    } else if (this.destinationCityLng === null) {
      score = 2
    }
    return score
  }

  static exportHeader () {
    return [
      'type',
      'mode',
      'no.person',
      'round.trip',
      'geodesic.distance.km',
      'corrected.distance.km',
      'distance.km',
      'emission.kg.co2e',
      'uncertainty.kg.co2e'
    ]
  }

  toString (sep = '\t') {
    return [
      this.type,
      this.transportation,
      this.carpooling,
      this.isRoundTrip,
      this.distance,
      this.getOneWayCorrectedDistance(),
      this.getCarbonIntensityDistance(),
      // this assumes getCarbonIntensity has been called before (with the right year) :/
      this.intensity.intensity,
      this.intensity.uncertainty
    ].join(sep)
  }

  isValid () {
    return this.score === 1
  }

  /**
   * Used to know whether the lat/lng properties are usable
   *
   * @returns {Boolean} - true iff we can work with those properties
   */
  latLngIsValid () {
    return (
      !isNaN(parseFloat(this.departureCityLat)) &&
      !isNaN(parseFloat(this.departureCityLng)) &&
      !isNaN(parseFloat(this.destinationCityLat)) &&
      !isNaN(parseFloat(this.destinationCityLng))
    )
  }

  /**
   * First classs distance, this is rarely used directly since different
   * correction factors need to be applied in different contexts
   *
   */
  get distance () {
    if (this.latLngIsValid() && !this.isFromDatabase()) {
      return TravelSection.geodesicDist(
        this.departureCityLat,
        this.departureCityLng,
        this.destinationCityLat,
        this.destinationCityLng
      )
    } else {
      // if comming from database
      return this.__distance
    }
  }

  /**
   * Get the distance while applying some correction
   * Correction are applied following the declaration order.
   *
   * /!\ side effect on this (set this.distance to the geodesic distance)
   *
   * @param {Object} options
   * @param {Boolean} options.cTransportation - true iff correction based on
   *  transportation mean should be applied
   * @param {Boolean} options.cCarpooling - true iff correction based on
   *  carpooling should be applied
   * @param {Boolean} options.cRoundTrip - true iff round trip nature should be applied
   *
   * @returns {number} the corrected distance
   */
  __getDistance ({
    cTransportation = false,
    cCarpooling = false,
    cRoundTrip = false
  } = {}) {
    // get geodesic distance
    let distance = this.distance

    if (
      cTransportation &&
      Object.keys(Travel.transportationsCorrectionFactors).includes(
        this.transportation
      )
    ) {
      distance =
        Travel.transportationsCorrectionFactors[this.transportation](distance)
    }

    if (cCarpooling) {
      if (this.transportation === Travel.MODE_CAB) {
        distance = distance * (1 + 1 / this.carpooling)
      } else if (this.transportation === Travel.MODE_CAR) {
        distance = distance / this.carpooling
      }
    }

    if (cRoundTrip) {
      if (this.isRoundTrip) {
        distance = 2 * distance
      }
    }

    return distance
  }

  /**
   * Get the **ONE WAY** distance of the section.
   * It only correspond to the corrected distance due to the transportation mean
   *
   * @returns {number} the geodesic corrected distance
   */
  getOneWayCorrectedDistance () {
    return this.__getDistance({ cTransportation: true })
  }

  /**
   * Get the total distance of the section. It accounts for
   * - the corrected distance due to the transportation mean
   * - the round trip nature of the section
   *
   * @returns {number} the geodesic corrected total distance
   */
  getRoundTripCorrectedDistance () {
    return this.__getDistance({ cTransportation: true, cRoundTrip: true })
  }

  /**
   *
   * Internal use only
   *
   * Distance that is taken into account when
   * calculating the carbon intensity. It uses
   * - the corrected distance due to the transportation mean
   * - the round trip nature of the section
   * - the corrected distance due to carpooling
   *
   * This can be seen as the contribution of a single person to the section's
   * emission
   *
   * @returns {number} the actual distance taken into account in carbon intensity
   */
  getCarbonIntensityDistance () {
    return this.__getDistance({
      cTransportation: true,
      cCarpooling: true,
      cRoundTrip: true
    })
  }

  computeType () {
    if (this.departureCountry !== null && this.destinationCountry !== null) {
      this.type = 'IN'
      if (this.departureCountry === 'FR' && this.destinationCountry === 'FR') {
        this.type = 'NA'
      } else if (
        this.departureCountry === 'FR' ||
        this.destinationCountry === 'FR'
      ) {
        this.type = 'MX'
      }
    }
  }

  /**
   * Emission factor are computed on the *One Way* distance
   *
   * @param {*} year
   * @returns
   */
  getEmissionFactor (year) {
    let distance = this.__getDistance({
      cTransportation: true,
      cCarpooling: true,
      cRoundTrip: false
    })

    let ef = EmissionFactor.createFromObj()
    this.computeType()
    if (this.transportation === Travel.MODE_PLANE) {
      if (distance < 1000) {
        ef = EmissionFactor.createFromObj(
          TRANSPORTS_FACTORS['plane']['shorthaul.contrails']
        )
      } else if (distance < 3501) {
        ef = EmissionFactor.createFromObj(
          TRANSPORTS_FACTORS['plane']['mediumhaul.contrails']
        )
      } else {
        ef = EmissionFactor.createFromObj(
          TRANSPORTS_FACTORS['plane']['longhaul.contrails']
        )
      }
    } else if (
      this.transportation === Travel.MODE_CAR ||
      this.transportation === Travel.MODE_CAB
    ) {
      ef = EmissionFactor.createFromObj(
        VEHICLES_FACTORS['car']['unknown.engine']
      )
    } else if (this.transportation === Travel.MODE_BUS) {
      ef = EmissionFactor.createFromObj(
        TRANSPORTS_FACTORS['bus']['bus.intercity']
      )
    } else if (this.transportation === Travel.MODE_TRAIN) {
      if (this.type === 'NA') {
        if (distance < 200) {
          ef = EmissionFactor.createFromObj(
            TRANSPORTS_FACTORS['railway']['train.shortdistance']
          )
        } else {
          ef = EmissionFactor.createFromObj(
            TRANSPORTS_FACTORS['railway']['train.longdistance']
          )
        }
      } else if (this.type === 'MX') {
        ef = EmissionFactor.createFromObj(
          TRANSPORTS_FACTORS['railway']['train.mixed']
        )
      } else {
        ef = EmissionFactor.createFromObj(
          TRANSPORTS_FACTORS['railway']['train.international']
        )
      }
    } else if (this.transportation === Travel.MODE_TRAM) {
      ef = EmissionFactor.createFromObj(
        TRANSPORTS_FACTORS['railway']['tram.bigcity']
      )
    } else if (this.transportation === Travel.MODE_RER) {
      ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['rer'])
    } else if (this.transportation === Travel.MODE_SUBWAY) {
      ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['railway']['subway'])
    } else if (this.transportation === Travel.MODE_FERRY) {
      ef = EmissionFactor.createFromObj(TRANSPORTS_FACTORS['boat']['ferry'])
    }
    return ef.getFactor(year)
  }

  getCarbonIntensity (year) {
    // first compute the corresponding emission factor
    let ef = this.getEmissionFactor(year)
    // then apply this emission factor on the distance
    let fDistance = this.getCarbonIntensityDistance()
    let intensity = new CarbonIntensity(
      fDistance * ef.total.total,
      fDistance * ef.total.total * ef.total.uncertainty,
      ef.group
    )
    this.intensity = intensity
    return intensity
  }

  toDatabase (withLocations = false) {
    this.computeType()
    let data = {
      type: this.type,
      isRoundTrip: this.isRoundTrip,
      distance: this.distance,
      transportation: this.transportation,
      carpooling: this.carpooling
    }
    if (withLocations) {
      Object.assign(data, {
        destination_city: this.destinationCity,
        destination_latitude: this.destinationCityLat,
        destination_longitude: this.destinationCityLng,
        destination_country: this.destinationCountry,
        departure_city: this.departureCity,
        departure_latitude: this.departureCityLat,
        departure_longitude: this.departureCityLng,
        departure_country: this.departureCountry
      })
    }
    return data
  }

  static createFromObj (section) {
    let nsection = new TravelSection(section)
    nsection.computeType()
    return nsection
  }

  static fromDatabase (payload) {
    let section = {
      transportation: payload.transportation,
      departureCity: payload.departure_city,
      departureCountry: payload.departure_country,
      destinationCity: payload.destination_city,
      destinationCountry: payload.destination_country,
      carpooling: payload.carpooling,
      distance: payload.distance,
      departureCityLat: payload.departure_latitude,
      departureCityLng: payload.departure_longitude,
      destinationCityLat: payload.destination_latitude,
      destinationCityLng: payload.destination_longitude,
      type: payload.type,
      isRoundTrip: payload.isRoundTrip
    }
    return this.createFromObj(section)
  }

  static geodesicDist (lat1, lon1, lat2, lon2) {
    if (lat1 === lat2 && lon1 === lon2) {
      return 0
    } else {
      var radlat1 = (Math.PI * lat1) / 180
      var radlat2 = (Math.PI * lat2) / 180
      var theta = lon1 - lon2
      var radtheta = (Math.PI * theta) / 180
      var dist =
        Math.sin(radlat1) * Math.sin(radlat2) +
        Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
      if (dist > 1) {
        dist = 1
      }
      dist = Math.acos(dist)
      dist = (dist * 180) / Math.PI
      dist = dist * 60 * 1.1515
      dist = dist * 1.609344
      return dist
    }
  }
}
