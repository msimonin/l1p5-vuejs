/* eslint-disable camelcase */

import Model from '@/models/carbon/Model.js'
import { Device, is_valid } from '@ecodiag/ecodiag-lib'
import {
  CarbonIntensity,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'

/**
 * L1P5 computer device is a wrapper around an Ecodiag Device.
 *
 * The strong assumption here is that the constructor is passed a true Ecodiag
 * Device object. We only re-expose most of the ecodiag property except tags and
 * source that are handled on our side.
 *
 */
export default class ComputerDevice extends Model {
  constructor (ecodiagObject, tags = [], source = null) {
    super({ tags, source })

    // supposed to be a valid ecodiagObject loaded from an ecodiag device file
    this.ecodiagObject = ecodiagObject
    this.intensity = new CarbonIntensity()
  }

  get score () {
    if (this.isValid()) {
      // valid
      return 1
    }
    return 2
  }

  get isComputer () {
    return ['laptop', 'allinone', 'desktop'].includes(this.type)
  }

  get isScreen () {
    return ['screen'].includes(this.type)
  }

  get id () {
    return this.ecodiagObject.id
  }

  set id (id) {
    this.ecodiagObject.id = id
  }

  get type () {
    return this.ecodiagObject.type
  }

  set type (type) {
    this.ecodiagObject.type = type
  }

  get model () {
    return this.ecodiagObject.model
  }

  set model (model) {
    this.ecodiagObject.model = model
  }

  get amount () {
    return this.ecodiagObject.nb
  }

  set amount (amount) {
    this.ecodiagObject.nb = amount
  }

  get acquisitionYear () {
    return this.ecodiagObject.year
  }

  set acquisitionYear (year) {
    this.ecodiagObject.year = year
  }

  toString (sep = '\t') {
    return [
      this.type,
      this.model,
      this.amount,
      this.tags.map((val) => val.name).join(';'),
      Math.round(this.intensity.intensity),
      Math.round(this.intensity.uncertainty)
    ].join(sep)
  }

  toEcodiag () {
    return this.ecodiagObject
  }

  toDatabase () {
    return {
      type: this.type,
      model: this.model,
      amount: this.amount,
      acquisitionYear: this.acquisitionYear,
      tags: this.tags
    }
  }

  get descriptor () {
    let h = 'notypenormodel'
    if (this.type) {
      h = '' + this.type
    }
    if (this.model) {
      h = h.concat(this.model)
    }
    return h
  }

  isSimilarTo (other) {
    // NOTE(msimonin): id is given by default (uuid)
    return (
      this === other ||
      this.id === other.id ||
      this.descriptor === other.descriptor
    )
  }

  sameAggregateAs (other) {
    return (
      this.descriptor === other.descriptor && this.tagsHash === other.tagsHash
    )
  }

  _doAggregate (other) {
    let newDevice = ComputerDevice.createFromObj(this)
    newDevice.amount += other.amount
    return newDevice
  }

  isValid () {
    return is_valid(this.toEcodiag(), 'flux', this.acquisitionYear)
  }

  isInvalid () {
    return !this.isValid()
  }

  static get formatDescription () {
    return {
      model: {
        pattern: /mod.le/i,
        required: false
      },
      brand: {
        pattern: /(fabricant|brand|marque)/i,
        required: false
      },
      type: {
        pattern: /(type|cat.gorie|kind)/i,
        required: true
      },
      tags: {
        pattern: /(tag|tags|label|labels)/i,
        converter: function (value) {
          return value.split(',')
        },
        default: []
      }
    }
  }

  static type2group = function (type) {
    try {
      return {
        desktop: 'desktopgroup',
        server: 'desktopgroup',
        allinone: 'desktopgroup',
        smartphone: 'smartphonegroup',
        pad: 'smartphonegroup'
      }[type]
    } catch (e) {
      return type
    }
  }

  getCarbonIntensity () {
    let up = [90]
    let edintensity = this.toEcodiag().compute_device_co2e('flux', up)
    let uncertainty = (edintensity.sups[0] - edintensity.infs[0]) / 2.0
    let intensity = new CarbonIntensity(
      edintensity.grey,
      uncertainty,
      ComputerDevice.type2group(this.type)
    )
    this.intensity = intensity
    return intensity
  }

  /**
   *
   * Create from an object coming most likely from the backend
   * device must have the same property of a ComputerDevice so that the function
   * is "idempotent"(will copy however) when passed a ComputerDevice
   *
   * @param {Object} device
   * @param {string} device.type - the type as expected in the ecodiag devices file
   * @param {string} device.model - the model
   * @param {number} device.amount - amount of such device
   * @param {tags} device.tags - tags to attache to this device
   * @param {source} device.source - source file the device comes from
   *
   * @returns {ComputerDevice} a new computer device
   */
  static createFromObj (device) {
    let ecodiagObject = Device.fromType(device.type, {
      model: device.model,
      nb: device.amount,
      year: device.acquisitionYear
    })
    return ComputerDevice.fromEcodiag(ecodiagObject, device.tags, device.source)
  }

  static createEmpty (year) {
    return ComputerDevice.fromEcodiag(
      Device.fromType(null, {
        nb: 1,
        year: year,
        score: 2
      })
    )
  }

  /**
   * @param {ComputerDevice[]} lines - The list of devices object to handle
   * @param {String} format - The file format
   * @param {String} tags - The list of tags defined by the user
   * @param {String} source - The source file name
   *
   * @returns {ComputerDevice[]} - The list of devices objects
   */
  static createFromLines (lines, format, currentYear, tags, source) {
    let devices = []
    for (let line of lines) {
      let item = ComputerDevice.createEmpty()
      try {
        item = ComputerDevice.fromRegex(
          line.type,
          line.model,
          currentYear,
          source
        )
        item.addTags(Model.getTagsFromNames(line.tags, tags))
      } catch (error) {
        // this might happen if the type/model pair is wrong
        // we create an invalid item and let the user decide/fix it
        // no-op ?
      }
      devices.push(item)
    }
    return devices
  }

  static exportHeader (sep = '\t') {
    return [
      'type',
      'model',
      'amount',
      'tags',
      'emission.kg.co2e',
      'uncertainty.kg.co2e'
    ].join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = ComputerDevice.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static compute (items, tags = []) {
    let intensities = new CarbonIntensities()
    for (let item of items.filter((obj) => obj.hasTags(tags))) {
      let intensity = item.getCarbonIntensity()
      intensities.add(intensity)
    }
    return {
      intensity: intensities.sum()
    }
  }

  static fromEcodiag (ecodiagObject, tags = [], source = null) {
    return new ComputerDevice(ecodiagObject, tags, source)
  }

  static fromRegex (type, model, acquisitionYear, source) {
    let d = Device.fromRegex(type, model)
    let cd = ComputerDevice.fromEcodiag(d)
    cd.acquisitionYear = acquisitionYear
    cd.source = source
    return cd
  }

  i18nType (locale) {
    if (this.ecodiagObject) {
      return this.ecodiagObject.i18nType(locale)
    }
  }

  i18nModel (locale) {
    if (this.ecodiagObject) {
      return this.ecodiagObject.i18nModel(locale)
    }
  }
}
