import { Entity } from './Entity'

class Laboratory extends Entity {
  constructor (
    id = null,
    name = null,
    referent = null,
    mainSite = null,
    country = null,
    area = null,
    citySize = null,
    latitude = Entity.DEFAULT_LATITUDE,
    longitude = Entity.DEFAULT_LONGITUDE,
    administrations = [],
    disciplines = []
  ) {
    super(
      id,
      name,
      referent,
      mainSite,
      country,
      area,
      citySize,
      latitude,
      longitude
    )
    this.administrations = administrations
    this.disciplines = disciplines
  }

  toDatabase () {
    return {
      type: 'Laboratory',
      data: {
        id: this.id,
        name: this.name,
        // don't export referent
        // referent: this.referent,
        mainSite: this.mainSite,
        country: this.country,
        area: this.area,
        citySize: this.citySize,
        administrations: this.administrations,
        disciplines: this.disciplines,
        latitude: this.latitude,
        longitude: this.longitude
      }
    }
  }

  toString (sep = '\t') {
    return [
      this.id,
      this.name,
      this.administrations.map((obj) => obj.name).join(';'),
      this.disciplines.map((obj) => obj.name + ':' + obj.percentage).join(';'),
      this.mainSite,
      this.country,
      this.area
    ].join(sep)
  }

  static get ICON () {
    return 'flask'
  }

  static createEmpty () {
    return new Laboratory()
  }

  static createFromObj (item) {
    return new Laboratory(
      item.id,
      item.name,
      item.referent,
      item.mainSite,
      item.country,
      item.area,
      item.citySize,
      item.latitude,
      item.longitude,
      item.administrations,
      item.disciplines
    )
  }

  static exportHeader (sep = '\t') {
    return [
      'entity.id',
      'entity.name',
      'entity.supervisions',
      'entity.disciplines',
      'entity.city',
      'entity.country',
      'entity.region'
    ].join(sep)
  }
}

export { Laboratory }
