/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import { AbstractEntity } from './AbstractEntity'

const DEFAULT_LATITUDE = 46.75
const DEFAULT_LONGITUDE = 2.32

class Entity extends AbstractEntity {
  constructor (
    id = null,
    name = null,
    referent = null,
    mainSite = null,
    country = null,
    area = null,
    citySize = null,
    latitude = DEFAULT_LATITUDE,
    longitude = DEFAULT_LONGITUDE
  ) {
    super(name, referent)
    this.id = id
    this.mainSite = mainSite
    this.country = country
    this.area = area
    this.citySize = citySize
    this.latitude = latitude
    this.longitude = longitude
  }

  toDatabase () {
    return {
      type: 'Entity',
      data: {
        id: this.id,
        name: this.name,
        // don't export referent
        // referent: this.referent,
        mainSite: this.mainSite,
        country: this.country,
        area: this.area,
        citySize: this.citySize,
        latitude: this.latitude,
        longitude: this.longitude
      }
    }
  }

  toString (sep = '\t') {
    return [this.id, this.name, this.mainSite, this.country, this.area].join(
      sep
    )
  }

  static get DEFAULT_LATITUDE () {
    return DEFAULT_LATITUDE
  }

  static get DEFAULT_LONGITUDE () {
    return DEFAULT_LONGITUDE
  }

  static createEmpty () {
    return new Entity()
  }

  static createFromObj (item) {
    return new Entity(
      item.id,
      item.name,
      item.referent,
      item.mainSite,
      item.country,
      item.area,
      item.citySize,
      item.latitude,
      item.longitude
    )
  }

  static exportHeader (sep = '\t') {
    return [
      'entity.id',
      'entity.name',
      'entity.city',
      'entity.country',
      'entity.region'
    ].join(sep)
  }
}

export { Entity }
