/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Rule from '../Rule.js'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Favoriser le covoiturage',
    level1help:
      'Modifier le nombre de passagers par semaine par déplacement en voiture en prenant en compte la fréquence hebdomadaire de covoiturage.',
    description:
      "Le covoiturage permet de diviser l’impact carbone et environnemental d'un trajet entre covoitureurs. En France, la <strong>voiture</strong> est utilisée dans <strong>70% des trajets domicile-travail</strong> [1] qui représentent <strong>4% du total des émissions de GES de la France</strong> [2]. Si la voiture est le moyen de transport le plus répandu, son <strong>taux d'occupation</strong> reste faible et est évalué à <strong>1.43 personne par voiture</strong> pour les trajets de courte distance (< 100km) [3].",
    otherbenefits:
      "Le covoiturage présente des co-bénéfices pour l'usager et la collectivité. Partager une voiture permet entre autre de <strong>partager les frais</strong> du trajet mais aussi de participer à <strong>diminuer la pollution atmosphérique</strong> des villes [1]. <br /> Afin de favoriser le covoiturage, des aides financières et des plateformes sont disponibles [1].",
    limits:
      'Le covoiturage peut être perçu comme une contrainte par les usagers car sa mise en place nécessite d’adapter ses horaires et son trajet.',
    rebounds:
      'Si les avantages du covoiturage pour les courtes distances ne sont pas contestables, celui-ci gagnerait à être optimisé pour les longues distances. <br /> Le covoiturage peut ainsi entraîner un <strong>report modal en faveur de la voiture</strong> (22 % des conducteurs et 67 % des passagers auraient pris le train si il n’y avait pas eu de covoiturage) ou encore <strong>augmenter la distance parcourue</strong> (99 km en moyenne) [4].',
    manual:
      'Le curseur permet de spécifier le nombre de personnes en covoiturage par véhicule (hors conducteur). Il n’y a pas de configuration avancée proposée. Le nombre de passagers sélectionnable peut être décimal. Ceci permet de fusionner en un seul curseur le nombre de passagers par véhicule (hors conducteur) et la fréquence hebdomadaire de covoiturage. Par exemple, transport deux passagers deux jours par semaine équivaut à un nombre moyen de covoitureurs de 0.8 (2*2/5).',
    humanTitle: function (level1Human, level1Unit) {
      return `Viser ${level1Human} nombre de passagers pour les trajets en voiture`
    }
  },
  en: {
    title: 'Encourage carpooling',
    level1help: 'Number of carpoolers per car trip.',
    description:
      'Carpooling allows the carbon and environmental impact of a journey to be divided among carpoolers. In France, the <strong>car</strong> is used in <strong>70% of home-to-work journeys</strong> [1] which represent <strong>4% of France’s total greenhouse gas emissions</strong> [2]. While the car is the most common means of transportation, its <strong>occupancy rate</strong> remains low and is estimated at <strong>1.43 people per car</strong> for short trips (<100km) [3].',
    otherbenefits:
      'Carpooling presents co-benefits for the user and the community. Sharing a car allows, among other things, to <strong>share travel expenses</strong> but also to participate in <strong>reducing atmospheric pollution</strong> in cities [1]. <br /> To promote carpooling, financial aid and platforms are available [1].',
    limits:
      'Carpooling can be perceived as a constraint by users because its implementation requires adapting schedules and routes.',
    rebounds:
      'While the benefits of carpooling for short distances are undeniable, it would be beneficial to optimise it for long distances as well. <br /> Carpooling can thus lead to a <strong>modal shift in favor of the car</strong> (22% of drivers and 67% of passengers would have taken the train if carpooling had not been available) or even <strong>increase the distance traveled</strong> (99 km on average) [4].',
    manual:
      'The slider allows you to specify the number of people carpooling per vehicle (excluding the driver). There is no advanced configuration offered. The selectable number of passengers can be decimal. This allows the number of passengers per vehicle (excluding the driver) and the weekly carpooling frequency to be merged into a single slider. For example, transporting two passengers two days a week is equivalent to an average number of carpoolers of 0.8 (2*2/5).',
    humanTitle: function (level1Human, level1Unit) {
      return `Target ${level1Human} carpoolers per car trip`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title:
      'Le covoiturage en France, ses avantages et la réglementation en vigueur',
    year: '2023',
    authors:
      'Ministère de la Transition écologique et de la Cohésion des territoires',
    link: 'https://www.ecologie.gouv.fr/covoiturage-en-france-avantages-et-reglementation-en-vigueur'
  },
  '[2]': {
    title:
      'Trajet domicile-travail : développer le co-voiturage et les mobilités douces',
    year: '2019',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/trajet-domicile-travail-developper-co-voiturage-mobilites-douces'
  },
  '[3]': {
    title: 'Se déplacer en voiture : seul, à plusieurs ou en covoiturage ?',
    year: '2022',
    authors:
      'Ministère de la Transition écologique et de la Cohésion des territoires',
    link: 'https://www.statistiques.developpement-durable.gouv.fr/se-deplacer-en-voiture-seul-plusieurs-ou-en-covoiturage-0'
  },
  '[4]': {
    title:
      'Quel est l’impact environnemental du covoiturage de longue distance?',
    year: '2017',
    authors: 'ADEME, Agence de la transition écologique',
    link: 'https://transportsdufutur.ademe.fr/2017/02/environnemental-covoiturage-distance.html'
  }
}

export default class Carpooling extends Rule {
  name = 'Carpooling'
  module = Modules.COMMUTES
  lang = LANG
  references = REFERENCES

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
  }

  get level1Max () {
    return 4
  }

  get level1Unit () {
    return 'passengers'
  }

  get level1Step () {
    return 0.1
  }

  get ticks () {
    return [0, 1, 2, 3, 4]
  }

  tickFormatter (value) {
    return value
  }

  get graphics () {
    return ['CommutesDistancesCarpooling']
  }

  compute (commutes) {
    let carpoolingValue = this.level1 + 1
    for (let commute of commutes) {
      for (let section of commute.sections) {
        if (
          section.mode === 'car' &&
          section.distance > 0 &&
          section.pooling < carpoolingValue
        ) {
          section.pooling = carpoolingValue
        }
      }
    }
    return commutes
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let commute of this.data) {
      const nbWeeks = commute.getNumberOfWorkedWeeks(settings, this.year)
      if (!commute.cdeleted) {
        for (let section of commute.sections) {
          if (section.mode === 'car' && section.distance > 0) {
            let annuelFactor = commute.nWorkingDay1 * nbWeeks
            if (section.isDay2) {
              annuelFactor = commute.nWorkingDay2 * nbWeeks
            }
            intensity.add(
              section
                .getCarbonIntensity(this.entity.citySize, this.year)
                .multiply(annuelFactor)
            )
          }
        }
      }
    }
    return intensity
  }
}
