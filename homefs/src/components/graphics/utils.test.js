import { getBin, getBins } from './utils.js'

describe('getBin', () => {
  test('<100', () => {
    expect(getBin(0)).toBe(0)
    expect(getBin(25)).toBe(0)
    expect(getBin(75)).toBe(0)
    expect(getBin(99.99)).toBe(0)
  })

  test('>=100', () => {
    expect(getBin(100)).toBe(100)
    expect(getBin(550)).toBe(500)
    expect(getBin(1300)).toBe(1000)
    expect(getBin(9500)).toBe(9000)
    expect(getBin(13000)).toBe(10000)
    expect(getBin(21000)).toBe(20000)
  })
})

describe('getBins', () => {
  test('getBins(x), x < 100 returns [0]', () => {
    expect(getBins(0)).toEqual([0])
    expect(getBins(10)).toEqual([0])
    expect(getBins(100)).toEqual([0, 100])
    expect(getBins(199)).toEqual([0, 100])
    expect(getBins(21000)).toEqual([
      0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 2000, 3000, 4000,
      5000, 6000, 7000, 8000, 9000, 10000, 20000
    ])
  })
})
