/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Travel from '@/models/carbon/Travel'
import { intConverter, booleanConverter } from '@/utils/parser.js'

export default function (positionDictionary, purposeDictionary) {
  let positiond = positionDictionary
  let purposed = purposeDictionary
  return {
    meta: {
      name: 'GES 1point5',
      check: function (fields) {
        return fields.includes('# mission')
      }
    },
    names: {
      pattern: /(# |Num.ro |)(mission|trip)/i,
      required: true,
      converter: function (value) {
        return [value]
      }
    },
    date: {
      pattern: /(date|departure date|date de d.part)/i,
      required: true
    },
    departureCity: {
      pattern: /(departure city|ville de d.part)/i,
      required: true
    },
    departureCountry: {
      pattern: /(departure country|country of departure|pays de d.part)/i,
      required: true,
      converter: function (value) {
        return Travel.iso3166Country(value)
      }
    },
    destinationCity: {
      pattern: /(destination city|ville de destination)/i,
      required: true
    },
    destinationCountry: {
      pattern:
        /(destination country|country of destination|pays de destination)/i,
      required: true,
      converter: function (value) {
        return Travel.iso3166Country(value)
      }
    },
    transportation: {
      pattern: /(mode|moyens).*(transport|d.placement)/i,
      converter: function (value) {
        return Travel.getTransportation(value)
      },
      required: true
    },
    carpooling: {
      pattern:
        /(no of persons in the car|nb de personnes dans la voiture|no of persons|nb de personnes|covoiturage|carpooling)/i,
      default: 1,
      required: true,
      converter: intConverter
    },
    isRoundTrip: {
      pattern: /(roundtrip|aller.*retour)/i,
      converter: booleanConverter,
      required: true
    },
    purpose: {
      pattern: /(purpose of the trip|purpose|motif|motif du d.placement)/i,
      converter: function (value) {
        return Travel.getPurpose(value, purposed)
      },
      default: Travel.PURPOSE_UNKNOWN
    },
    status: {
      pattern: /(agent position|position|statut.*agent|statut)/i,
      converter: function (value) {
        return Travel.getStatus(value, positiond)
      },
      default: Travel.STATUS_UNKNOWN
    },
    amount: {
      pattern: /(quantit.|nombre|amount|number)/i,
      default: 1,
      converter: intConverter
    },
    tags: {
      pattern: /(tag|tags|label|labels)/i,
      converter: function (value) {
        return value.split(',').map((val) => val.trim())
      },
      default: []
    }
  }
}
