/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Travel from '@/models/carbon/Travel'
import { intConverter, booleanConverter } from '@/utils/parser.js'

export default function (positionDictionary, purposeDictionary) {
  let positiond = positionDictionary
  let purposed = purposeDictionary
  return {
    meta: {
      name: 'SIFAC',
      check: function (fields) {
        return fields.includes('Date Dép.')
      },
      delimiters: ['\t', ',', ';']
    },
    names: {
      pattern: /(#mission)/i,
      required: true,
      converter: function (value) {
        return [value]
      }
    },
    date: {
      pattern: /(Date Dép.)/i,
      required: true
    },
    departureCity: {
      pattern: /(Ville.*départ)/i,
      required: true
    },
    departureCountry: {
      pattern: /(Pays Dép.)/i,
      required: true,
      converter: function (value) {
        return Travel.iso3166Country(value)
      }
    },
    destinationCity: {
      pattern: /(Ville.*Destination)/i,
      required: true
    },
    destinationCountry: {
      pattern: /(Pays Dest.)/i,
      required: true,
      converter: function (value) {
        return Travel.iso3166Country(value)
      }
    },
    transportation: {
      pattern: /(Md Dép.|Mode de déplacement)/i,
      required: true,
      converter: function (value) {
        let mode = value
        if (value === 'VOLP') {
          mode = Travel.MODE_PLANE
        } else if (value === 'VOL') {
          mode = Travel.MODE_PLANE
        } else if (value === 'VEHI') {
          mode = Travel.MODE_CAR
        } else if (value === 'LOCP') {
          mode = Travel.MODE_CAR
        } else if (value === 'TRNP') {
          mode = Travel.MODE_TRAIN
        } else if (value === 'TRN') {
          mode = Travel.MODE_TRAIN
        }
        return mode
      }
    },
    carpooling: {
      pattern: /(Nb. Pers.)/i,
      default: 1,
      required: true,
      converter: intConverter
    },
    isRoundTrip: {
      pattern: /(A\/R.)/i,
      required: true,
      converter: booleanConverter
    },
    purpose: {
      pattern: /(Motif du déplacement)/i,
      converter: function (value) {
        return Travel.getPurpose(value, purposed)
      },
      default: Travel.PURPOSE_UNKNOWN
    },
    status: {
      pattern: /(Statut Agt)/i,
      converter: function (value) {
        return Travel.getStatus(value, positiond)
      },
      default: Travel.STATUS_UNKNOWN
    },
    amount: {
      pattern: /(quantit.|nombre|amount|number)/i,
      default: 1,
      converter: intConverter
    },
    tags: {
      pattern: /(tag|tags|label|labels)/i,
      converter: function (value) {
        return value.split(',').map((val) => val.trim())
      },
      default: []
    }
  }
}
