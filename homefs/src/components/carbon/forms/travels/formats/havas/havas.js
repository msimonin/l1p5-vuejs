/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Travel from '@/models/carbon/Travel'
import { intConverter } from '@/utils/parser.js'

const REPLACE_CITY = {
  'PARIS ORLY': 'PARIS',
  'PARIS GARE DE LYON': 'PARIS',
  'PARIS MONT': 'PARIS',
  'PARIS EST': 'PARIS',
  'VALENCE VILLE': 'VALENCE',
  'VALENCE TGV RHONE-ALPES SUD': 'VALENCE',
  'AVIGNON TGV': 'AVIGNON',
  'AVIGNON CENTRE': 'AVIGNON',
  'LYON PART DIEU': 'LYON',
  'TOULOUSE MATABIAU': 'TOULOUSE',
  'MONTPELLIER SAINT-ROCH': 'MONTPELLIER',
  'ANGERS ST LAUD': 'ANGERS',
  'MARSEILLE ST CHARLES': 'MARSEILLE',
  'DIJON VILLE': 'DIJON',
  'BORDEAUX ST JEAN': 'BORDEAUX'
}

export default function () {
  return {
    meta: {
      name: 'HAVAS',
      check: function (fields) {
        return fields.includes('Client')
      },
      delimiters: ['\t', ',', ';'],
      preprocess: function (data) {
        let toReturn = []
        let num = 1
        let positiveAmount = new Map()
        let negativeAmount = []
        for (let line of data) {
          // if transportation is plain or train and cost is positive
          if (
            ['billet aérien', 'Ferroviaire', 'LOW COST'].includes(
              line['Prestation']
            )
          ) {
            if (!line['Montant Net TTC'].startsWith('-')) {
              positiveAmount.set(
                line['Montant Net TTC'] + line['Trajet complet'],
                line
              )
            } else {
              negativeAmount.push(
                line['Montant Net TTC'] + line['Trajet complet']
              )
            }
          }
        }
        for (let amount of negativeAmount) {
          positiveAmount.delete(amount.replace('-', ''))
        }
        for (let line of Array.from(positiveAmount, ([name, value]) => value)) {
          let sections = line['Trajet complet'].split(' - ')
          if (sections.length === 1) {
            let nline = Object.assign({}, line)
            nline['# mission'] = num.toString()
            toReturn.push(nline)
          } else {
            // if multiple sections
            // the format defines the country only for departure and arrival cities
            // catch links between city and country from Routing column
            let cparts = line['Routing'].match(
              /(.*?) \((.*?)\)-(.*?) \((.*?)\)/
            )
            let clinks = {
              [cparts[1]]: cparts[2],
              [cparts[3]]: cparts[4]
            }
            let nsections = []
            for (let section of sections) {
              let nline = Object.assign({}, line)
              let found = section
                .replace('[stop] ', '')
                .match(/\d:(.*?) -> (.*?)$/)
              nline['# mission'] = num.toString()
              let lieu1 = found[1]
              if (Object.keys(REPLACE_CITY).includes(lieu1.toUpperCase())) {
                lieu1 = REPLACE_CITY[lieu1]
              }
              let lieu2 = found[2]
              if (Object.keys(REPLACE_CITY).includes(lieu2.toUpperCase())) {
                lieu2 = REPLACE_CITY[lieu2]
              }
              nline['Lieu départ'] = lieu1
              nline['Ville départ'] = lieu1
              nline['Lieu dest'] = lieu2
              nline['Ville dest'] = lieu2
              // if the country is defined
              if (Object.keys(clinks).includes(lieu1)) {
                nline['Code pays départ'] = clinks[lieu1]
                nline['Pays départ'] = clinks[lieu1]
              } else if (Object.keys(clinks).includes(found[1])) {
                nline['Code pays départ'] = clinks[found[1]]
                nline['Pays départ'] = clinks[found[1]]
              } else {
                nline['Code pays départ'] = ''
                nline['Pays départ'] = ''
              }
              if (Object.keys(clinks).includes(lieu2)) {
                nline['Code pays dest'] = clinks[lieu2]
                nline['Pays dest'] = clinks[lieu2]
              } else if (Object.keys(clinks).includes(found[2])) {
                nline['Code pays dest'] = clinks[found[2]]
                nline['Pays dest'] = clinks[found[2]]
              } else {
                nline['Code pays dest'] = ''
                nline['Pays dest'] = ''
              }
              nsections.push(nline)
            }
            // if departure and arrival cities are same, force one way trip
            if (
              nsections[0]['Ville départ'] ===
              nsections[nsections.length - 1]['Ville dest']
            ) {
              for (let section of nsections) {
                section['Type Trajet'] = 'Aller simple'
              }
            }
            toReturn = toReturn.concat(nsections)
          }
          num += 1
        }
        return toReturn
      }
    },
    names: {
      pattern: /(# mission)/i,
      headerName: '# mission'
    },
    date: {
      pattern: /(Date d.part\/d.but)/i,
      required: true
    },
    departureCity: {
      pattern: /(Ville départ)/i,
      required: true
    },
    departureCountry: {
      pattern: /(Code pays d.part)/i,
      required: true,
      converter: function (value) {
        return Travel.iso3166Country(value)
      }
    },
    destinationCity: {
      pattern: /(Ville dest)/i,
      required: true
    },
    destinationCountry: {
      pattern: /(Code pays dest)/i,
      required: true,
      converter: function (value) {
        return Travel.iso3166Country(value)
      }
    },
    transportation: {
      pattern: /(Prestation)/i,
      required: true,
      converter: function (value) {
        let mode = value
        if (value === 'billet aérien') {
          mode = Travel.MODE_PLANE
        } else if (value === 'Ferroviaire') {
          mode = Travel.MODE_TRAIN
        } else if (value === 'LOW COST') {
          mode = Travel.MODE_PLANE
        }
        return mode
      }
    },
    fulltrip: {
      pattern: /(Trajet complet)/i,
      required: true
    },
    isRoundTrip: {
      pattern: /(Type Trajet)/i,
      required: true,
      converter: function (value) {
        if (value === 'Aller simple') {
          return false
        } else if (value === 'Aller-retour') {
          return true
        }
      }
    },
    carpooling: {
      pattern: /(no column)/i,
      default: 1
    },
    purpose: {
      pattern: /(no column)/i,
      default: Travel.PURPOSE_UNKNOWN
    },
    status: {
      pattern: /(no column)/i,
      default: Travel.STATUS_UNKNOWN
    },
    amount: {
      pattern: /(quantit.|nombre|amount|number)/i,
      default: 1,
      converter: intConverter
    },
    tags: {
      pattern: /(tag|tags|label|labels)/i,
      converter: function (value) {
        return value.split(',').map((val) => val.trim())
      },
      default: []
    }
  }
}
