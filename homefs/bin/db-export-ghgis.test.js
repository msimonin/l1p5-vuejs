/**
 * This task iterates over all GHGI
 *  - It compute the corresponding emissions
 *  - and export the data to a file
 *
 * Implementation:
 *  We use jest framework, initially designed for running test.
 *  But jest also incudes a lot of preprocessor/module loader that allows us to
 *  use some of our frontend code without too much pain.
 *
 */

import axios from 'axios'
import Modules from '@/models/Modules'
import { makeGHGI } from '@/utils/ghgi'
import { ActiveModules } from '@/models/ActiveModules'
import { Configuration } from '@/models/carbon/Configuration'
import { generateFileName } from '@/utils/files'
import GHGI from '@/models/carbon/GHGI.js'

import { unparse } from 'papaparse'

const fs = require('fs')

const TOKEN = process.env.TOKEN
if (!TOKEN) {
  console.log(
    'No Token provided: get one with python manage.py token ADMIN_EMAIL'
  )
  process.exit(1)
}
const BASE_URL = process.env.BASE_URL || 'http://localhost:8000'
console.log(
  `BASE_URL=${BASE_URL} should target the webserver (make sure it contains http(s)://)`
)

let now = new Date()
const YEAR = process.env.YEAR || now.getFullYear() - 1
const EXPORT_DIR = process.env.EXPORT_DIR || './'
console.log(`Exporting year=${YEAR} to directory=${EXPORT_DIR}`)

describe('db:sythesis', () => {
  test('db:synthesis:gen', async () => {
    const http = axios.create({
      baseURL: `${BASE_URL}/api`,
      timeout: 60 * 4 * 100000,
      headers: {
        'Content-Type': 'application/json'
      }
    })

    http.interceptors.request.use(
      function (config) {
        const token = TOKEN
        if (token) config.headers.Authorization = `Bearer ${token}`
        return config
      },
      function (error) {
        return Promise.reject(error)
      }
    )
    let confPayload = await http
      .get('get_settings/')
      .then((response) => response.data)
    let conf = Configuration.fromDatabase(confPayload.settings)
    let activeModulesObjs = ActiveModules.fromSettings(conf.settings)

    let allGHGIs = await http
      .post('get_all_ghgi_admin/', { years: [YEAR] })
      .then((response) => response.data)
    console.log('allGHGISs length =', allGHGIs.ghgis.length)
    let ghgis = allGHGIs.ghgis
    let computedGHGIs = []
    for (let i in ghgis) {
      let ghgi = ghgis[i]
      console.log(`Checking ghgi.id=${ghgi.id}(${YEAR})`)
      try {
        let ghgiData = await http
          .get('get_ghgi_consumptions/', { params: { ghgi_id: ghgi.id } })
          .then((response) => response.data)
        let ghgiObj = makeGHGI(
          ghgiData,
          activeModulesObjs,
          conf.CF_LABEL_MAPPING.value
        )
        // compute the emissions we want to store
        ghgiObj.compute(conf.toOptions())
        computedGHGIs.push(ghgiObj)
      } catch (error) {
        console.error(`Error with ${ghgi.id}`, error)
        continue
      }
    } // for (let i in ghgis)
    let content = GHGI.exportHeader(conf.POSITION_TITLES.value)
    content += GHGI.exportToFile(
      conf.POSITION_TITLES.value,
      computedGHGIs,
      false
    )
    let fileName = generateFileName('GHGIs')
    fs.writeFileSync(EXPORT_DIR + '/' + fileName, content)

    // special case for commutes we export an aggregated version of the data
    let commutesModule = activeModulesObjs.byType(Modules.COMMUTES)
    let aggCommutes = []
    if (commutesModule) {
      for (let ghgi of computedGHGIs) {
        let aggCommute = commutesModule.klass.aggregate(ghgi.emissions.commutes)
        // inject the ghgi_uuid
        aggCommute['ghgi.uuid'] = ghgi.uuid
        aggCommutes.push(aggCommute)
      }
    }
    fileName = generateFileName(`${Modules.COMMUTES}-aggregated`)
    let options = { quotes: false, delimiter: '\t' }
    fs.writeFileSync(EXPORT_DIR + '/' + fileName, unparse(aggCommutes, options))

    // export data for all the modules
    for (let module of activeModulesObjs) {
      let content = 'ghgi.uuid\t' + module.klass.exportHeader()
      for (let ghgi of computedGHGIs) {
        content += module.klass.exportToFile(
          ghgi[module.type],
          false,
          ghgi.uuid
        )
      }
      let fileName = generateFileName(module.type)
      fs.writeFileSync(EXPORT_DIR + '/' + fileName, content)
    }
  })
})
