#!/usr/bin/env bash

set -ue

HERE=$(dirname "$0")
SETTINGS_DIR=$HERE/../backend/settings/l1p5

TO=$1
FILE=$SETTINGS_DIR/l1p5-$TO.py
TARGET=$SETTINGS_DIR/l1p5.py
 
ln -sfr $FILE $TARGET

ls -l $SETTINGS_DIR