/**
 * This task iterates over all GHGI
 *  - It compute the corresponding emissions
 *  - and for any submitted  module this (re-)submit the data
 *
 * Note that this task is supposed to be one-shot once the migrations that
 * introduce the CarboIntensity summary in the database is ran.
 *
 * TODO(msimonin):
 * - compute the GHGI only if it has been submitted
 * - getting all the GHGIs can last a long time, make sure to increase the timeout
 *  => optimization use offset + limit in the API
 *
 * Implementation:
 *  We use jest framework, initially designed for running test.
 *  But jest also incudes a lot of preprocessor/module loader that allows us to
 *  use some of our frontend code without too much pain.
 *
 */

import axios from 'axios'
import { makeGHGI } from '@/utils/ghgi'
import { ActiveModules } from '@/models/ActiveModules'
import { Configuration } from '@/models/carbon/Configuration'

const TOKEN = process.env.TOKEN
if (!TOKEN) {
  console.log(
    'No Token provided: get one with python manage.py token ADMIN_EMAIL'
  )
  process.exit(1)
}

const BASE_URL = process.env.BASE_URL || 'http://localhost:80'
console.log(
  `BASE_URL=${BASE_URL} should target the webserver (make sure it contains http(s)://)`
)

describe('db:sythesis', () => {
  test('db:synthesis:gen', async () => {
    const http = axios.create({
      baseURL: `${BASE_URL}/api`,
      timeout: 60 * 4 * 100000,
      headers: {
        'Content-Type': 'application/json'
      }
    })

    http.interceptors.request.use(
      function (config) {
        const token = TOKEN
        if (token) config.headers.Authorization = `Bearer ${token}`
        return config
      },
      function (error) {
        return Promise.reject(error)
      }
    )
    let confPayload = await http
      .get('get_settings/')
      .then((response) => response.data)
    let conf = Configuration.fromDatabase(confPayload.settings)
    let activeModulesObjs = ActiveModules.fromSettings(conf.settings)

    let allGHGIs = await http
      .post('get_all_ghgi_admin/')
      .then((response) => response.data)

    console.log('allGHGISs length =', allGHGIs.ghgis.length)
    let ghgis = allGHGIs.ghgis
    for (let i in ghgis) {
      let ghgi = ghgis[i]
      console.log(`Checking ghgi.id=${ghgi.id}`)
      try {
        let ghgiData = await http
          .get('get_ghgi_consumptions/', { params: { ghgi_id: ghgi.id } })
          .then((response) => response.data)
        let ghgiObj = makeGHGI(
          ghgiData,
          activeModulesObjs,
          conf.CF_LABEL_MAPPING.value
        )
        // compute the emissions we want to store
        ghgiObj.compute(conf.toOptions())
        console.log(`Syncing ghgi.id=${ghgi.id}`, ghgiObj.synthesis)
        await http
          .post('update_ghgi_synthesis/', {
            ghgi_id: ghgi.id,
            synthesis: ghgiObj.synthesis
          })
          .then((response) => response.data)
      } catch (error) {
        console.error(`Error with ${ghgi.id}`, error)
        continue
      }
    } // for (let i in ghgis)
  })
})
