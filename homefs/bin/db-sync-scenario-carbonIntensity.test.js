/**
 * This task iterates over all Scenario
 *  - It apply and compute the corresponding emissions
 *
 * Note that this task is supposed to be one-shot once the migrations that
 * introduce the CarboIntensity summary in the database is ran.
 *
 * Implementation:
 *  We use jest framework, initially designed for running test.
 *  But jest also incudes a lot of preprocessor/module loader that allows us to
 *  use some of our frontend code without too much pain.
 *
 */

import axios from 'axios'
import { ActiveModules } from '@/models/ActiveModules.js'
import Scenario from '@/models/scenario/Scenario.js'
import { makeGHGI } from '@/utils/ghgi'
import { Configuration } from '@/models/carbon/Configuration'

const TOKEN = process.env.TOKEN
if (!TOKEN) {
  console.log(
    'No Token provided: get one with python manage.py token ADMIN_EMAIL'
  )
  process.exit(1)
}

const BASE_URL = process.env.BASE_URL || 'http://localhost:80'
console.log(`BASE_URL=${BASE_URL} should target the webserver`)

describe('db:sythesis', () => {
  test('db:synthesis:gen', async () => {
    const http = axios.create({
      baseURL: `${BASE_URL}/api`,
      timeout: 60 * 4 * 100000,
      headers: {
        'Content-Type': 'application/json'
      }
    })

    http.interceptors.request.use(
      function (config) {
        const token = TOKEN
        if (token) config.headers.Authorization = `Bearer ${token}`
        return config
      },
      function (error) {
        return Promise.reject(error)
      }
    )
    let confPayload = await http
      .get('get_settings/')
      .then((response) => response.data)
    let conf = Configuration.fromDatabase(confPayload.settings)
    let activeModulesObjs = ActiveModules.fromSettings(conf.settings)

    let scenarios = await http
      .post('get_scenarios_admin/')
      .then((response) => response.data)
    console.log('scenarios', scenarios)
    for (let i in scenarios) {
      let scenarioObj = Scenario.createFromObj(scenarios[i])
      console.log(`Checking scenario.id=${scenarioObj.id}`)
      try {
        let ghgiData = await http
          .get('get_ghgi_consumptions/', {
            params: { ghgi_id: scenarioObj.ghgi.id }
          })
          .then((response) => response.data)
        let ghgiObj = makeGHGI(
          ghgiData,
          activeModulesObjs,
          conf.CF_LABEL_MAPPING.value
        )
        scenarioObj.setGHGI(ghgiObj)
        // compute the emissins we want to store
        scenarioObj.compute(conf.toOptions())
        console.log('Syncing ', scenarioObj.synthesis)
        await http
          .post('update_scenario_synthesis/', {
            scenario_id: scenarioObj.id,
            synthesis: scenarioObj.synthesis
          })
          .then((response) => response.data)
      } catch (error) {
        console.error(`Error with ${scenarioObj.id}`, error)
        continue
      }
    } // for (let i in scenarios)
  })
})
