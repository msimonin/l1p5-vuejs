# Generated by Django 3.2.7 on 2022-10-15 10:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_rename_newinitiative_laboratoire_initiative'),
        ('transition', '0002_update_laboratory_initiatives'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='NewInitiative',
            new_name='Initiative',
        ),
    ]
