from django.db import migrations, models
import django.db.models.deletion


def update_laboratory_to_entity(apps, schema_director):
    Laboratory = apps.get_model("core", "laboratory")
    PublicAction = apps.get_model("transition", "PublicAction")
    ContentType = apps.get_model("contenttypes", "ContentType")
    content_type = ContentType.objects.get_for_model(Laboratory)
    for action in PublicAction.objects.all():
        action.content_type = content_type
        action.object_id = action.laboratory.id
        action.save()

class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('carbon', '0063_auto_20240503_1751'),
        ('transition', '0011_publicaction_reminder')
    ]

    operations = [
        migrations.AddField(
            model_name='PublicAction',
            name='content_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.contenttype'),
        ),
        migrations.AddField(
            model_name='PublicAction',
            name='object_id',
            field=models.PositiveIntegerField(),
        ),
        migrations.RunPython(update_laboratory_to_entity),
        migrations.RemoveField(
            model_name='PublicAction',
            name='laboratory',
        ),
    ]
