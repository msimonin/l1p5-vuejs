from rest_framework import serializers
from rest_framework.reverse import reverse

from backend.core.models.core import Entity
from backend.core.serializers.core import EntitySerializer
from .models import Message, PublicAction, PublicActionFile, Tag

from ..core.serializers import AdministrationSerializer, DisciplineSerializer
from ..core.models import Laboratory

from ..users.serializers import L1P5UserSerializer


def wrap_entity(klass, data):
    return dict(type=klass.__name__, data=data)


class MyHyperlinkedIdentityField(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        url_kwargs = {
            "plan_id": obj.action.plan.id,
            "action_id": obj.action.id,
            "file_id": obj.id,
        }
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)


class MyPublicHyperlinkedIdentityField(serializers.HyperlinkedIdentityField):
    def get_url(self, obj, view_name, request, format):
        url_kwargs = {"action_id": obj.action.id, "file_id": obj.id}
        return reverse(view_name, kwargs=url_kwargs, request=request, format=format)


class PublicActionFileSerializer(serializers.ModelSerializer):
    # https://www.django-rest-framework.org/api-guide/relations/#custom-hyperlinked-fields
    # HyperlinkedIdentityField is for itself
    url = MyPublicHyperlinkedIdentityField(view_name="mgmt-public-file")
    # align with javascript file size  attribute
    size = serializers.SerializerMethodField()

    def get_size(self, obj):
        # this might throw if the file is not found for instance
        try:
            return obj.file.size
        except Exception as e:
            return 0

    class Meta:
        model = PublicActionFile
        fields = "__all__"


class SimpleLaboratorySerializer(serializers.ModelSerializer):
    referent = L1P5UserSerializer()
    administrations = AdministrationSerializer(many=True)
    disciplines = DisciplineSerializer(many=True)

    class Meta:
        model = Laboratory
        fields = [
            "id",
            "name",
            "referent",
            "administrations",
            "latitude",
            "longitude",
            "disciplines",
        ]


class GenericSimpleEntitySerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        if isinstance(instance, Laboratory):
            s = SimpleLaboratorySerializer(instance)
            return wrap_entity(Laboratory, s.data)
        elif isinstance(instance, Entity):
            s = EntitySerializer(instance)
            return wrap_entity(Entity, s.data)
        else:
            raise ValueError("Unknown Entity Type")


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = "__all__"


class PublicActionSerializer(serializers.ModelSerializer):
    files = PublicActionFileSerializer(many=True, read_only=True)
    # shortcut: just present the email to the frontend
    reviewer = serializers.SerializerMethodField()
    entity = GenericSimpleEntitySerializer()
    tags = TagSerializer(many=True, read_only=True)

    def get_reviewer(self, obj):
        if obj.reviewer is None:
            return None
        return obj.reviewer.email

    class Meta:
        model = PublicAction
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"


SEARCH_FIELDS = [
    "id",
    "name",
    "latitude",
    "longitude",
]


class SearchLaboratorySerializer(serializers.ModelSerializer):
    """Rationale: get the bare minimal for displaying the search map/table.

    Avoid going through related field to deeply.
    e.g reviewer and referent requires to fetch the auth group for each record
    which might be slow.
    """

    class Meta:
        model = Laboratory
        fields = SEARCH_FIELDS


class SearchEntitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entity
        fields = SEARCH_FIELDS


class GenericSearchEntitySerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        if isinstance(instance, Laboratory):
            s = SearchLaboratorySerializer(instance)
            return wrap_entity(Laboratory, s.data)
        elif isinstance(instance, Entity):
            s = SearchEntitySerializer(instance)
            return wrap_entity(Entity, s.data)
        else:
            raise NotImplementedError()


class SearchActionSerializer(serializers.ModelSerializer):
    """Used for displaying the search map/table of actions."""

    entity = GenericSearchEntitySerializer()
    files = PublicActionFileSerializer(many=True, read_only=True)
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = PublicAction
        fields = "__all__"
