import subprocess
import sys

from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status


from backend.job.models import Job
from backend.job.serializers import FullJobSerializer, PartialJobSerializer
from backend.job.constants import SUPPORTED_TASKS
from backend.utils import hasSuperPower


@api_view(["GET", "POST"])
@permission_classes([hasSuperPower])
def jobs(request):
    # let's keep the size reasonnable
    if request.method == "GET":
        jobs = Job.objects.all()
        serializer = PartialJobSerializer(jobs, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method == "POST":
        data = request.data.copy()
        task = data.pop("task", None)
        origin = data.pop("origin")
        # consider only task in the white list
        if task in SUPPORTED_TASKS:
            # prevent several task to run concurrently
            # FIXME: make it better by making sure the process isn't running
            # (keep the pids for that)
            running_jobs = Job.objects.filter(finished__isnull=True).count()
            if running_jobs > 0:
                return Response(
                    "A job is already running", status=status.HTTP_409_CONFLICT
                )
            # create the job object
            job = Job.objects.create(name=task)
            # fire a background task to launch this
            subprocess.Popen(
                [
                    sys.executable,
                    "manage.py",
                    "run",
                    "--daemon",
                    "--record",
                    "--origin",
                    origin,
                    "--uuid",
                    str(job.uuid),
                    task,
                ]
            )
            serializer = PartialJobSerializer(job)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            raise PermissionDenied()

    raise PermissionDenied()


@api_view(["GET", "DELETE"])
@permission_classes([hasSuperPower])
def one_job(request, job_id):
    job = get_object_or_404(Job, id=job_id)
    if request.method == "GET":
        serializer = FullJobSerializer(job)
        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method == "DELETE":
        job.delete()
        return Response(None, status=status.HTTP_200_OK)

    raise PermissionDenied()
