from django.db import migrations, models


class Migration(migrations.Migration):
 
    dependencies = [
        ('core', '0001_initial'),
    ]
 
    operations = [
        migrations.AddConstraint(
            model_name='settings',
            constraint=models.UniqueConstraint(fields=('name', 'section'), name='unique_configuration_key_in_section'),
        ),
    ]
