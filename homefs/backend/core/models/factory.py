from django.apps import apps


from .core import Boundary, Entity, Members, PositionTitle
from .laboratory import Laboratory, laboratory_update_or_create


def boundary_update_or_create(boundary):

    memberz = boundary.pop("members", [])
    # FIXME(msimonin): do the same with locations
    _ = boundary.pop("locations", [])

    # FIXME(msimonin): make it a transaction
    # update members
    b, _ = Boundary.objects.update_or_create(id=boundary.get("id"), defaults=boundary)
    # update members
    for members in memberz:
        # get the corresponding position
        position = PositionTitle.objects.get(name=members["position"]["name"])
        # update/create the members for this position
        Members.objects.update_or_create(
            boundary=b,
            position=position,
            defaults=dict(number=members.get("number", 0)),
        )

    return b


def entity_update_or_create(entity, referent):
    """Factory method to create the right entity"""
    # FIXME(msimonin): Check that we are saving an object that corresponds to
    # the active entity
    data = entity["data"]
    data["referent"] = referent
    if entity["type"] == Entity.__name__:
        entity, _ = Entity.objects.update_or_create(id=data.get("id"), defaults=data)
    elif entity["type"] == Laboratory.__name__:
        entity = laboratory_update_or_create(data)
    else:
        raise Exception("Entity type not recognized")
    return entity
