from django.db import models
from django.conf import settings
from django.db.models.constraints import UniqueConstraint
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType


class Settings(models.Model):
    name = models.CharField(max_length=50)
    section = models.CharField(max_length=50)
    value = models.JSONField(null=True)
    type = models.IntegerField()
    component = models.CharField(null=True, max_length=50)

    @classmethod
    def get_value(cls, **kwargs):
        try:
            return cls.objects.get(**kwargs).value
        except:
            return None

    class Meta:
        constraints = [
            UniqueConstraint(
                fields=["name", "section"], name="unique_configuration_key_in_section"
            )
        ]


from django.core.validators import MaxValueValidator, MinValueValidator


class Boundary(models.Model):
    budget = models.IntegerField(null=True, blank=True)


class PositionTitle(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False, unique=True)
    quotity = models.FloatField(
        null=False,
        blank=False,
        default=1.0,
        validators=[MinValueValidator(0.0), MaxValueValidator(1.0)],
    )


class Location(models.Model):
    name = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    latitude = models.DecimalField(max_digits=10, decimal_places=7)
    longitude = models.DecimalField(max_digits=10, decimal_places=7)

    boundary = models.ForeignKey(
        Boundary, on_delete=models.CASCADE, related_name="locations"
    )


class Members(models.Model):
    boundary = models.ForeignKey(
        Boundary, on_delete=models.CASCADE, related_name="members"
    )
    position = models.ForeignKey(PositionTitle, on_delete=models.CASCADE)
    number = models.PositiveIntegerField(null=True, blank=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["boundary", "position"],
                name="can't have twice the same position title in bounday",
            )
        ]


class AbstractEntity(models.Model):
    referent = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    mainSite = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    area = models.CharField(max_length=50)
    citySize = models.IntegerField(null=True)
    latitude = models.DecimalField(max_digits=10, decimal_places=7)
    longitude = models.DecimalField(max_digits=10, decimal_places=7)
    ghgis = GenericRelation(
        "carbon.GHGI", related_name="entity", related_query_name="theentity"
    )
    tag_categories = GenericRelation(
        "core.TagCategory", related_name="entity", related_query_name="theentity"
    )

    # NOTE(msimonin): The following doesn't work as I expected
    # With the following; PublicAction.objects.filter(theentity__name="plop")
    # creates a join on the Laboratory Table regardless the actual entity
    # referenced in the action.
    #
    # ... so for now I add the generic relation to the various entities and generates the related_query_name
    # on the fly (see filter_items in transition.views) if we want to use the reverse relationship
    # e.g: filter transition public actions based on the entity name
    #
    #
    # ... so beware that the abos GenericRelation might not work as expected as well !
    #
    #
    # actions = GenericRelation(
    #    "transition.PublicAction", related_name="entity", related_query_name="theentity"
    # )

    class Meta:
        abstract = True

    @property
    def nghgi(self):
        return len(self.ghgis.all())


class Entity(AbstractEntity):
    actions = GenericRelation(
        "transition.PublicAction", related_name="entity", related_query_name="theentity"
    )


class TagCategory(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True)
    color = models.CharField(max_length=7)

    # define the generic entity (laboratory, ...)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True)
    object_id = models.PositiveIntegerField()
    entity = GenericForeignKey("content_type", "object_id")


class Tag(models.Model):
    name = models.CharField(max_length=100)
    category = models.ForeignKey(
        TagCategory, related_name="tags", on_delete=models.CASCADE
    )

    @property
    def color(self):
        return self.category.color


class TaggedItem(models.Model):
    tag = models.ForeignKey(Tag, related_name="item", on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("content_type", "object_id")
