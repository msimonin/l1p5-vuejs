from typing import List, Tuple

from django.conf import settings
from django.core.management.base import BaseCommand

from backend.core.models.core import PositionTitle, Settings
from backend.utils import init, force_init

MOTD = """
*************************************************
*                                               *
* Initializing the L1P5 APP from l1p5.py        *
*                                               *
*************************************************
"""


def _init(
    force: bool, force_settings: List[str]
) -> Tuple[List[str], List[str], List[str], List[str]]:
    if force:
        return force_init()
    else:
        return init(force_settings)


class Command(BaseCommand):
    help = """Initialize the app. 
    Will read the l1p5 configuration and create the position titles and settings in the DB"""

    def add_arguments(self, parser):
        parser.add_argument(
            "force_setting",
            nargs="*",
            type=str,
            help="The setting name to force (should be in L1P5_SETTINGS)",
        )
        parser.add_argument(
            "--force", action="store_true", help="Force an update for all the settings"
        )

    def handle(self, *app_labels, **options):

        new_pt, updated_pt, new_settings, updated_settings = _init(
            options["force"], options["force_setting"]
        )

        all_settings = Settings.objects.values_list("name", flat=True)
        remaining_settings = (
            set(all_settings) - set(new_settings) - set(updated_settings)
        )

        all_pt = PositionTitle.objects.values_list("name", flat=True)
        remaining_pt = set(all_pt) - set(new_pt) - set(updated_pt)

        # succint report

        self.stdout.write(self.style.SUCCESS(MOTD))

        if new_pt:
            self.stdout.write(
                self.style.SUCCESS(
                    "Created the following position titles:" + ",".join(new_pt)
                )
            )

        if updated_pt:
            self.stdout.write(
                self.style.SUCCESS(
                    "Updated the following position titles:" + ",".join(updated_pt)
                )
            )
        if updated_settings:
            self.stdout.write(
                self.style.SUCCESS(
                    "Updated the following settings:" + ",".join(updated_settings)
                )
            )
        if remaining_settings:
            self.stdout.write(
                self.style.WARNING(
                    "The following settings remain untouched and need attention: "
                    + ",".join(remaining_settings)
                )
            )

        if remaining_pt:
            self.stdout.write(
                self.style.WARNING(
                    "The following remain untouched and need attention: "
                    + ",".join(remaining_pt)
                )
            )
