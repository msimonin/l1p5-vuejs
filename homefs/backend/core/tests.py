from abc import abstractmethod, ABCMeta
from django.test import Client, tag
from django.test import TestCase
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken

from backend.test_utils import (
    PermissionsTestCase,
    ensure_entity_created,
    ensure_lab_created,
    ensure_user_created,
    modify_conf,
)
from backend.core.models import Tag, TagCategory
from backend.core.models import Entity, Laboratory
from backend.utils import force_init


REQUEST_KWARGS = dict(content_type="application/json")


def make_payload(administrations):
    payload = dict(
        type=Laboratory.__name__,
        data=dict(
            name="test_add_same_administrations",
            latitude=42.0,
            longitude=43.0,
            disciplines=[],
            administrations=administrations,
        ),
    )
    return payload


@modify_conf(ENTITY_CLASS="Laboratory")
class TestAddAdministrations(TestCase):
    """Laboratory specific"""

    def setUp(self) -> None:
        force_init()
        self.user = ensure_user_created("test@test.org")
        refresh = RefreshToken.for_user(self.user)
        # in django >= 4.2 we could use the headers for that
        self.api = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

    def test_add_same_administrations(self):
        r = self.api.post(
            "/api/save_entity/",
            make_payload(["plop", "plop", "plop"]),
            **REQUEST_KWARGS,
        )
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # check the returned administrations
        response = r.json()
        self.assertEqual(
            1,
            len(response["data"]["administrations"]),
            "Only one administration must be created",
        )
        self.assertEqual(
            "plop",
            response["data"]["administrations"][0]["name"],
            "The administration must be named 'plop'",
        )

    def test_add_same_administrations_case_insensitive(self):
        r = self.api.post(
            "/api/save_entity/",
            make_payload(["plop", "PLOP", "PlOp"]),
            **REQUEST_KWARGS,
        )
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # check the returned administrations
        response = r.json()
        self.assertEqual(
            1,
            len(response["data"]["administrations"]),
            "Only one administration must be created",
        )
        self.assertEqual(
            "plop",
            response["data"]["administrations"][0]["name"],
            "The administration must be named 'plop'",
        )

    def test_add_update_lab_administrations_case_insensitive(self):
        r = self.api.post(
            "/api/save_entity/",
            make_payload(["plop"]),
            **REQUEST_KWARGS,
        )
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        # Update the lab with some new (same) administrations
        response = r.json()

        payload = dict(**response)
        payload["data"]["administrations"].append("PLOP")

        r = self.api.post(
            "/api/save_entity/",
            payload,
            **REQUEST_KWARGS,
        )
        self.assertEqual(status.HTTP_201_CREATED, r.status_code)

        response = r.json()
        self.assertEqual(
            1,
            len(response["data"]["administrations"]),
            "Only one administration must be created",
        )
        self.assertEqual(
            "plop",
            response["data"]["administrations"][0]["name"],
            "The administration must be named 'plop'",
        )

    def tearDown(self) -> None:
        self.user.delete()


class TestCoreViewWrapper:
    """Avoid unittest to instantiate this class.

    Subclasses of the inner class will run the same tests but for alternate entities.
    """

    class TestCoreView(PermissionsTestCase, metaclass=ABCMeta):
        def setUp(self) -> None:
            super().setUp()

            self.entity = self.make_resources(self.user)

            name = "test cat"
            TagCategory.objects.filter(object_id=self.entity.id, name=name).delete()
            self.category, _ = TagCategory.objects.update_or_create(
                object_id=self.entity.id,
                name=name,
                defaults=dict(description=name, color="#000", entity=self.entity),
            )

            for tag in ["test1", "test2"]:
                t, _ = Tag.objects.update_or_create(
                    name=tag, defaults=dict(category=self.category)
                )

        # abstract
        @abstractmethod
        def make_resources(self, user):
            ...

        # test / entity agnostic
        def test_get_all_tag_categories(self):
            # submitted
            def user_cb(self, response, d):
                r = response.json()
                self.assertEqual(
                    2, len(r[0]["tags"]), f"[{d['desc']}] must have 2 tags"
                )

            self.assert_permissions(
                "/api/get_all_tag_categories/",
                method="get",
                unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
                authenticated_rc=status.HTTP_404_NOT_FOUND,
                user_rc=status.HTTP_200_OK,
                user_cb=user_cb,
                reviewer_rc=status.HTTP_404_NOT_FOUND,
                admin_rc=status.HTTP_404_NOT_FOUND,
            )

        def test_delete_tag_category(self):
            data = dict(id=self.category.id)
            self.assert_permissions(
                "/api/delete_tag_category/",
                method="post",
                data=data,
                unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
                authenticated_rc=status.HTTP_403_FORBIDDEN,
                user_rc=status.HTTP_204_NO_CONTENT,
                # one can delete only once so desactivating the following
                reviewer_rc=None,
                admin_rc=None,
            )

        def test_delete_tag_category_reviewer(self):
            data = dict(id=self.category.id)
            self.assert_permissions(
                "/api/delete_tag_category/",
                method="post",
                data=data,
                unauthenticated_rc=None,
                authenticated_rc=None,
                user_rc=None,
                reviewer_rc=status.HTTP_403_FORBIDDEN,
                admin_rc=None,
            )

        def test_delete_tag_category_admin(self):
            data = dict(id=self.category.id)
            self.assert_permissions(
                "/api/delete_tag_category/",
                method="post",
                data=data,
                unauthenticated_rc=None,
                authenticated_rc=None,
                user_rc=None,
                reviewer_rc=None,
                admin_rc=status.HTTP_403_FORBIDDEN,
            )

        def test_update_or_create_tag_category(self):
            # update the category
            data = dict(id=self.category.id, tags=[dict(name="new tag")])
            self.assert_permissions(
                "/api/update_or_create_tag_category/",
                method="post",
                data=data,
                unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
                # no lab
                authenticated_rc=status.HTTP_404_NOT_FOUND,
                user_rc=status.HTTP_201_CREATED,
                # no lab
                reviewer_rc=status.HTTP_404_NOT_FOUND,
                # no lab
                admin_rc=status.HTTP_404_NOT_FOUND,
            )


@tag("laboratory")
@modify_conf(ENTITY_CLASS="Laboratory")
class TestCoreViewLaboratory(TestCoreViewWrapper.TestCoreView):
    def setUp(self) -> None:
        force_init()
        return super().setUp()

    def make_resources(self, user):
        # create some resource for the user
        return ensure_lab_created(user)

        # create some tags


@tag("entity")
@modify_conf(ENTITY_CLASS="Entity")
class TestCoreViewEntity(TestCoreViewWrapper.TestCoreView):
    def setUp(self) -> None:
        force_init()
        super().setUp()

    def make_resources(self, user):
        # create some resource for the user
        return ensure_entity_created(user)
