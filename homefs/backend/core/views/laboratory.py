"""Extracted Lab specific view functions"""
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from ..models import Administration, Discipline

from ..serializers import AdministrationSerializer, DisciplineSerializer


@api_view(["GET"])
@permission_classes([])
def get_administrations(request):
    administrations = Administration.objects.all()
    serializer = AdministrationSerializer(administrations, many=True)
    return Response(serializer.data)


@api_view(["GET"])
@permission_classes([])
def get_disciplines(request):
    sous_domaines = Discipline.objects.all()
    serializer = DisciplineSerializer(sous_domaines, many=True)
    return Response(serializer.data)
