"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""

from django.conf import settings
from django.contrib import admin
from django.urls import path, include, re_path
import django.contrib.auth.views as auth_views
from rest_framework import routers

from .core import views as coreviews
from .carbon import views as carbonviews
from .job import views as jobviews
from .users import views as usersviews
from .scenario import views as scenarioviews
from .transition import views as transitionviews

router = routers.DefaultRouter()

urlpatterns = [

    # admin
    path('django-admin/', admin.site.urls),

    # http://localhost:8000/
    path('', coreviews.index_view, name='index'),

    # http://localhost:8000/api/<router-viewsets>
    path('api/', include(router.urls)),

    # handle users
    path('api/is_super_user/', usersviews.is_super_user, name='is_super_user'),
    path('api/get_users/', usersviews.get_users, name='get_all_users'),
    path('api/update_roles/', usersviews.update_roles, name='update_permissions'),
    path('api/update_is_admin/', usersviews.update_is_admin, name='update_is_admin'),
    path('api/update_user/', usersviews.update_user, name='update_user'),
    path('api/delete_user/', usersviews.delete_user, name='delete_user'),
    path('api/send_activation_email/', usersviews.send_activation_email, name='send_activation_email'),
    path('api/auth/registration/', usersviews.RegisterView.as_view()),
    path('api/user_exists/', usersviews.user_exists, name='user_exists'),
    path('api/reset_password/', usersviews.PasswordResetView.as_view(), name='rest_password_reset'),
    path('accounts/reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('accounts/activate_account/<uidb64>/<token>/', usersviews.activate_account, name='activate_account'),

    # core module
    path('api/get_settings/', coreviews.get_settings, name='get_settings'),
    path('api/save_settings/', coreviews.save_settings, name='save_settings'),
    path('api/save_entity/', coreviews.save_entity, name='save_entity'),
    path('api/get_entity/', coreviews.get_entity, name='get_entity'),    
    path('api/get_all_tag_categories/', coreviews.get_all_tag_categories, name='get_all_tag_categories'),    
    path('api/update_or_create_tag_category/', coreviews.update_or_create_tag_category, name='update_or_create_tag_category'),    
    path('api/delete_tag_category/', coreviews.delete_tag_category, name='delete_tag_category'),    
    path('api/get_administrations/', coreviews.get_administrations, name='get_administrations'),
    path('api/get_disciplines/', coreviews.get_disciplines, name='get_disciplines'),

    # carbon module
    path('api/fakeGHGI/', carbonviews.get_fakeGHGI, name='fakeGHGI'),
    path('api/get_counts/', carbonviews.get_counts, name='get_counts'),
    path('api/save_ghgi/', carbonviews.save_ghgi, name='save_ghgi'),
    path('api/delete_ghgi/', carbonviews.delete_ghgi, name='delete_ghgi'),
    path('api/get_all_ghgi/', carbonviews.get_all_ghgi, name='get_all_ghgi'),
    path('api/get_all_ghgi_without_consumptions/', carbonviews.get_all_ghgi_without_consumptions, name='get_all_ghgi'),
    path('api/get_all_ghgi_admin/', carbonviews.get_all_ghgi_admin, name='get_all_ghgi_admin'),
    path('api/get_ghgis_consumptions/', carbonviews.get_ghgis_consumptions, name='get_ghgis_consumptions'),
    path('api/get_ghgi_consumptions/', carbonviews.get_ghgi_consumptions, name='get_ghgi_consumptions'),
    path('api/get_ghgi_consumptions_by_uuid/', carbonviews.get_ghgi_consumptions_by_uuid, name='get_ghgi_consumptions_by_uuid'),
    path('api/save_vehicles/', carbonviews.save_vehicles, name='save_vehicles'),
    path('api/get_all_vehicles/', carbonviews.get_all_vehicles, name='get_all_vehicles'),
    path('api/save_buildings/', carbonviews.save_buildings, name='save_buildings'),
    path('api/get_all_buildings/', carbonviews.get_all_buildings, name='get_all_buildings'),
    path('api/save_commutes/', carbonviews.save_commutes, name='save_commutes'),
    path('api/save_foods/', carbonviews.save_foods, name='save_foods'),
    path('api/save_survey/', carbonviews.save_survey, name='save_survey'),
    path('api/save_travels/', carbonviews.save_travels, name='save_travels'),
    path('api/save_computer_devices/', carbonviews.save_computer_devices, name='save_computer_devices'),
    path('api/save_purchases/', carbonviews.save_purchases, name='save_purchases'),
    path('api/save_research_activities/', carbonviews.save_research_activities, name='save_research_activities'),
    path('api/get_astro_alphas/', carbonviews.get_astro_alphas, name='get_astro_alphas'),
    path('api/get_astro_years/', carbonviews.get_astro_years, name='get_astro_years'),
    path('api/update_submitted/', carbonviews.update_submitted, name='update_submitted'),
    path('api/update_ghgi_synthesis/', carbonviews.update_synthesis, name='update_ghgi_synthesis'),

    path('api/get_ghgi_survey_info/', carbonviews.get_ghgi_survey_info, name='get_ghgi_survey_info'),
    path('api/update_survey_active/', carbonviews.update_survey_active, name='update_survey_active'),
    path('api/save_survey_config/', carbonviews.save_survey_config, name='save_survey_config'),
    path('api/clone_survey/', carbonviews.clone_survey, name='clone_survey'),

    path('api/copy_data/', carbonviews.copy_data, name='copy_data'),

    # scenario module
    path('api/save_scenario/', scenarioviews.save_scenario, name='save_scenario'),
    path('api/get_scenario/', scenarioviews.get_scenario, name='get_scenario'),
    path('api/delete_scenario/', scenarioviews.delete_scenario, name='delete_scenario'),
    path('api/get_scenarios/', scenarioviews.get_scenarios, name='get_scenarios'),
    path('api/get_scenario_from_uuid/', scenarioviews.get_scenario_from_uuid, name='get_scenario_from_uuid'),
    path('api/get_scenarios_admin/', scenarioviews.get_scenarios_admin, name='get_scenarios_admin'),
    path('api/update_scenario_synthesis/', scenarioviews.update_synthesis, name='update_scenario_synthesis'),

    # transition1.5 module
    ## user: actions on plans
    path('api/transition/quota/', transitionviews.get_transition_quota, name='get_transition_quota'),

    path('api/transition/get_count/', transitionviews.get_count, name='get_count'),
    path('api/transition/has_reviewer_permissions/', transitionviews.has_reviewer_permissions, name='has_reviewer_permissions'),

    # user: actions on public actions
    path('api/transition/actions/', transitionviews.mgmt_public_actions, name='mgmt-public-actions'),
    path('api/transition/actions/<int:action_id>/', transitionviews.mgmt_public_action, name='mgmt-public-action'),
    path('api/transition/actions/<int:action_id>/files/', transitionviews.mgmt_public_files, name='mgmt-public-files'),
    path('api/transition/actions/<int:action_id>/files/<int:file_id>/', transitionviews.mgmt_public_file, name='mgmt-public-file'),

    ## public: read only on public actions
    path('api/public/actions/', transitionviews.public_actions_published, name='get_published_initiatives'),
    path('api/public/actions/<int:action_id>/', transitionviews.public_action_published, name='get_one_published_initiative'),
    path("api/public/entities/", transitionviews.public_entities, name="get_public_entities"),

    ## reviewer/admin actions
    path('api/reviewer/quota/<int:entity_id>/', transitionviews.reviewer_get_quota, name='review-get-quota'),
    path('api/reviewer/actions/', transitionviews.reviewer_actions_submitted, name='reviewer-actions'),
    path('api/reviewer/reminders/', transitionviews.reviewer_reminders, name='reviewer-reminders'),
    path('api/reviewer/actions/my/', transitionviews.reviewer_my_actions, name='reviewer-my-actions'),
    path('api/reviewer/actions/<int:action_id>/', transitionviews.reviewer_action, name='reviewer-one-action'),
    path('api/reviewer/actions/<int:action_id>/reminder/', transitionviews.reviewer_reminder, name='reviewer-reminder'),
    path('api/reviewer/actions/<int:action_id>/messages/', transitionviews.reviewer_messages, name='reviewer-messages'),
    path('api/reviewer/counts/', transitionviews.reviewer_counts, name='reviewer-counts'),

    # move me to carbon once merged into master
    path('api/transition/has_submitted_ghgis/', transitionviews.has_submitted_ghgis, name='has-submitted-ghgis'),


    # job api
    path('api/jobs/', jobviews.jobs, name='jobs'),
    path('api/jobs/<int:job_id>/', jobviews.one_job, name='one_job'),

    # authentication and JWT Token
    path('api/token/', usersviews.L1P5TokenObtainPairView.as_view(), name='token_obtain_pair'),
    #path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # Entry point of the VueJS web application
    re_path(r'^.*$', coreviews.index_view, name='entry_point')
]

if settings.ALLOW_ADMIN:
    urlpatterns = [path('django-admin/', admin.site.urls)] + urlpatterns

