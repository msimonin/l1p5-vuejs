from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from rest_framework.permissions import BasePermission

from backend.core.models import Settings
from backend.core.models.core import Entity, PositionTitle
from backend.core.models.laboratory import Laboratory


MANDATORY_CONFIGURATION_KEYS = [
    "L1P5_ACTIVE_MODULES",
    "L1P5_MODULES_COLOR",
    "L1P5_GEONAMES_USERNAME",
    "L1P5_ALLOWED_COUNTRIES",
    "L1P5_ENTITY_CLASS",
    "L1P5_NUMBER_OF_WORKED_WEEKS",
    "L1P5_FILE_FORMATS",
    "L1P5_ACCEPTED_MIME_TYPES",
    "L1P5_POSITION_TITLES",
    "L1P5_CF_LABEL_MAPPING",
    "L1P5_TRAVEL_POSITION_LABELS",
    "L1P5_TRAVEL_POSITION_DICTIONARY",
    "L1P5_I18N",
]


class EntityNotSet(Exception):
    ...


class EntityUnknown(Exception):
    ...


class hasSuperPower(BasePermission):
    """
    Allows access only to admin users.
    """

    def has_permission(self, request, view):
        return bool(
            request.user and (request.user.is_staff or request.user.is_superuser)
        )


def entity_get(**kwargs):
    entity = entity_filter(**kwargs)
    return entity.first()


def get_entity_or_404(**kwargs):
    """Util function that mimics get_object_or_404 function."""
    entity = entity_get(**kwargs)
    if entity is None:
        raise Http404("No entity found.")
    return entity


def get_entity_type():
    """Get the entity type based on the settings

    Raises if the entity can't be found
    """
    try:
        t = Settings.objects.get(name="ENTITY_CLASS", section="global").value
    except ObjectDoesNotExist:
        raise EntityNotSet()
    if t == "Laboratory":
        return Laboratory
    elif t == "Entity":
        return Entity
    else:
        raise EntityUnknown()


def entity_filter(**kwargs):
    EntityClass = get_entity_type()
    return EntityClass.objects.filter(**kwargs)


def init():
    """Initialize the app. Idempotent.

    Read the configuration and create the initial resources
    Must be called prior to starting the app.
    """

    # create/update the initial settings (idempotent)
    new_settings, updated_settings = [], []
    for setting in settings.L1P5_SETTINGS:
        _, created = Settings.objects.update_or_create(
            section=setting["section"], name=setting["name"], defaults=setting
        )
        if created:
            new_settings.append(setting["name"])
        else:
            updated_settings.append(setting["name"])

    new_pt, updated_pt = [], []
    for pt in Settings.get_value(name="POSITION_TITLES"):
        pt, created = PositionTitle.objects.update_or_create(
            name=pt["name"], defaults=dict(quotity=pt["quotity"])
        )
        if created:
            new_pt.append(pt.name)
        else:
            updated_pt.append(pt.name)

    return new_pt, updated_pt, new_settings, updated_settings
