from typing import Optional

from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.core.exceptions import PermissionDenied
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.contrib.contenttypes.models import ContentType

from .models import Scenario, Rule
from .serializers import ScenarioSerializer

from ..utils import entity_get
from ..carbon.models import GHGI
from ..carbon.serializers import GHGISerializer
from ..carbon.views import get_ghgi_consumption_data, validate_synthesis
from ..carbon.views import owns_ghgi
from ..users.models import L1P5User


def owns_scenario(data_key: Optional[str] = "id", model_key: Optional[str] = "id"):
    """Decorates function to ensure the user owns the scenario
    Args:
        data_key: where to look in the request.data to find
            the scenario identifier (e.g id, scenario_id, uuid)
        model_key: mapping of data_key in the Scenario model (e.g id, uuid)
    """

    def _owns_scenario(f):
        def wrapped(request, *args, **kwargs):
            identifier = request.data[data_key]
            kwds = {model_key: identifier}
            scenario = get_object_or_404(Scenario, **kwds)
            if scenario.owner != request.user:
                raise PermissionDenied()
            return f(request, scenario, *args, **kwargs)

        return wrapped

    return _owns_scenario


@api_view(["POST"])
@owns_ghgi(data_key="ghgi_id")
def save_scenario(request, ghgi):
    data = request.data.copy()
    validate_synthesis(data["scenario"]["synthesis"])
    scenario_data = data.pop("scenario")
    rules_data = scenario_data.pop("rules")
    scenario_data["ghgi"] = ghgi
    scenario_id = scenario_data.pop("id")
    if scenario_id is not None:
        # we're updating one scenario
        # we check that the user is the owner
        scenario = get_object_or_404(Scenario, id=scenario_id)
        if scenario.owner != request.user:
            raise PermissionDenied()
    scenario, created = Scenario.objects.update_or_create(
        id=scenario_id, defaults=scenario_data
    )
    Rule.objects.filter(scenario=scenario).delete()
    for rule_data in rules_data:
        rule_data["scenario"] = scenario
        Rule.objects.create(**rule_data)
    scenario = Scenario.objects.get(id=scenario.id)
    serializer = ScenarioSerializer(scenario)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(["POST"])
@owns_scenario(data_key="id")
def delete_scenario(request, scenario):
    scenario.delete()
    entity = entity_get(referent_id=request.user.id)
    ghgis = GHGI.objects.filter(
        object_id=entity.id,
        content_type=ContentType.objects.get_for_model(entity).id,
    )
    scenarios = Scenario.objects.filter(ghgi_id__in=ghgis.values_list("id", flat=True))
    serializer = ScenarioSerializer(scenarios, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
def get_scenarios(request):
    entity = entity_get(referent_id=request.user.id)
    ghgis = GHGI.objects.filter(
        object_id=entity.id,
        content_type=ContentType.objects.get_for_model(entity).id,
    )
    scenarios = Scenario.objects.filter(ghgi_id__in=ghgis.values_list("id", flat=True))
    serializer = ScenarioSerializer(scenarios, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
@owns_scenario(data_key="uuid", model_key="uuid")
def get_scenario(request, scenario):
    serializer = ScenarioSerializer(scenario, many=False)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([])
def get_scenario_from_uuid(request):
    uuid = request.data.pop("uuid")
    scenario = Scenario.objects.get(uuid=uuid)
    sserializer = ScenarioSerializer(scenario, many=False)
    gserializer = GHGISerializer(scenario.ghgi, many=False)
    ghgi_consumption = get_ghgi_consumption_data(scenario.ghgi.id)
    to_return = {}
    to_return["ghgi"] = gserializer.data
    to_return["ghgi"].update(ghgi_consumption)
    to_return["scenario"] = sserializer.data
    return Response(to_return, status=status.HTTP_200_OK)


@api_view(["POST"])
def get_scenarios_admin(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            scenarios = Scenario.objects.all()
            serializer = ScenarioSerializer(scenarios, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    return Response(None, status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
def update_synthesis(request):
    if request.user.is_authenticated:
        if L1P5User.objects.get(email=request.user).is_superuser:
            validate_synthesis(request.data["synthesis"])
            scenario_id = request.data.pop("scenario_id")
            # save synthesis
            Scenario.objects.filter(id=scenario_id).update(
                synthesis=request.data["synthesis"]
            )
            return Response(None, status=status.HTTP_204_NO_CONTENT)
