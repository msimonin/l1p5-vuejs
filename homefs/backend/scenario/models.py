import uuid
from django.db import models

from ..carbon.models import GHGI


class Scenario(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(null=True)
    timeline = models.IntegerField(default=2030)
    ghgi = models.ForeignKey(GHGI, related_name="scenario", on_delete=models.CASCADE)
    uuid = models.UUIDField(null=False, unique=True, editable=False)
    synthesis = models.JSONField(null=True)

    @property
    def owner(self):
        return self.ghgi.entity.referent

    def save(self, *args, **kwargs):
        """On save, update uuid and ovoid clash with existing code"""
        if not self.uuid:
            self.uuid = uuid.uuid4()
        return super(Scenario, self).save(*args, **kwargs)


class Rule(models.Model):
    name = models.CharField(max_length=50)
    level1 = models.JSONField(null=True)
    level2 = models.JSONField(null=True)
    scenario = models.ForeignKey(
        Scenario, related_name="rules", on_delete=models.CASCADE
    )
