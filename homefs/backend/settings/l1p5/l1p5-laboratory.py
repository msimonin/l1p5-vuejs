#
# This is the L1P5 application configuration
#
# you can configure the position titles used Boundary, Survey and Travel
# Mandatory keys are marked like this: (MANDATORY)
#
# As you need to translate all of these you can define here your custom translation
# that will be sent back to the frontend.

# (Mandatory) The URL of the log (supported string are those supported by the
# HTML img.src attribute)
L1P5_LOGO_URL = "/static/img/labos1p5_logo_white.png"

# (Mandatory) List of apps names to activate
# name is made of the type / appname method
L1P5_ACTIVE_APPS = [
    "application/carbon",
    "application/scenario",
    "application/transition",
    "simulator/commutes-simulator",
    "simulator/foods-simulator",
    "simulator/travels-simulator",
    "simulator/ecolabware",
]

# (Mandatory) List of module names to activate
# name is made of the type / thecollection method
L1P5_ACTIVE_MODULES = [
    "buildings/form",
    "purchases/form",
    "devices/form",
    "vehicles/form",
    "travels/form",
    "commutes/survey",
    "foods/survey",
    "ractivities/form",
]


# (Mandatory) Which username to use for geoname service
L1P5_GEONAMES_USERNAME = "labos1point5"

# (Mandatory) Which country the entity is located in
L1P5_ALLOWED_COUNTRIES = [
    "MF",
    "BL",
    "PF",
    "YT",
    "GF",
    "PM",
    "RE",
    "GP",
    "MQ",
    "WF",
    "TF",
    "NC",
    "FR",
]

# (Mandatory)
L1P5_ENTITY_CLASS = "Laboratory"

L1P5_NUMBER_OF_WORKED_WEEKS = {"default": 41, 2020: 27, 2021: 37}

L1P5_FILE_FORMATS = ["ges1p5", "geslab", "havas", "sifac"]

L1P5_ACCEPTED_MIME_TYPES = "application/pdf,image/*,.lss"

# The following is for factorization purpose within this file
# Note however that the values here will end up in the export files
## Boundary: the legacy 4 categories of the french version
_L1P5_PT_RESEARCHER = "pt.researcher"
_L1P5_PT_TEACHER = "pt.teacher"
_L1P5_PT_SUPPORT = "pt.support"
_L1P5_PT_STUDENTPOSTDOC = "pt.student-postdoc"

## Survey: the legacy 3 categories used in the survey of the french version
_L1P5_CF_RESEARCHERTEACHER = "cf.researcher-teacher"
_L1P5_CF_SUPPORT = "cf.support"
_L1P5_CF_STUDENTPOSTDOC = "cf.student-postdoc"


## Travel: the legacy 5 categories used in the travel model
_L1P5_TRAVEL_GUEST = "travel.guest"
_L1P5_TRAVEL_RESEARCHER = "travel.researcher"
_L1P5_TRAVEL_SUPPORT = "travel.support"
_L1P5_TRAVEL_STUDENTPOSTDOC = "travel.student-postdoc"

# this label is special
_L1P5_TRAVEL_UNKNOWN = "travel.unknown"


# (MANDATORY) position titles name to use
L1P5_POSITION_TITLES = [
    {"name": _L1P5_PT_RESEARCHER, "quotity": 1},
    {"name": _L1P5_PT_TEACHER, "quotity": 0.5},
    {"name": _L1P5_PT_SUPPORT, "quotity": 1},
    {"name": _L1P5_PT_STUDENTPOSTDOC, "quotity": 1},
]

# The following are the mandatory values that will be read and sent to the frontend

# (MANDATORY) survey might use a superset of labels
# this mapping is used to correctly computed the number of people in each group
L1P5_CF_LABEL_MAPPING = {
    _L1P5_PT_RESEARCHER: _L1P5_CF_RESEARCHERTEACHER,
    _L1P5_PT_TEACHER: _L1P5_CF_RESEARCHERTEACHER,
    _L1P5_PT_SUPPORT: _L1P5_CF_SUPPORT,
    _L1P5_PT_STUDENTPOSTDOC: _L1P5_CF_STUDENTPOSTDOC,
}

# (MANDATORY) travels uses it's own labels
# NOTE(msimonin): There's a remaining convention with the frontend regarding the UNKNOWN="unknown" label
L1P5_TRAVEL_POSITION_LABELS = [
    _L1P5_TRAVEL_GUEST,
    _L1P5_TRAVEL_RESEARCHER,
    _L1P5_TRAVEL_SUPPORT,
    _L1P5_TRAVEL_STUDENTPOSTDOC,
    _L1P5_TRAVEL_UNKNOWN,
]

# (MANDATORY) mapping from string to travels position labels
# By default the identity is generated but this can be augmented here
L1P5_TRAVEL_POSITION_DICTIONARY = {
    "personne invitee": _L1P5_TRAVEL_GUEST,
    "invited person": _L1P5_TRAVEL_GUEST,
    "chercheur.e-ec": _L1P5_TRAVEL_RESEARCHER,
    "ita": _L1P5_TRAVEL_SUPPORT,
    "engineer": _L1P5_TRAVEL_SUPPORT,
    "technician": _L1P5_TRAVEL_SUPPORT,
    "administrative": _L1P5_TRAVEL_SUPPORT,
    "doc-post doc": _L1P5_TRAVEL_STUDENTPOSTDOC,
    "phd-post doctoral fellow": _L1P5_TRAVEL_STUDENTPOSTDOC,
    "phd": _L1P5_TRAVEL_STUDENTPOSTDOC,
    "post-doc": _L1P5_TRAVEL_STUDENTPOSTDOC,
    "post doc": _L1P5_TRAVEL_STUDENTPOSTDOC,
    "": _L1P5_TRAVEL_UNKNOWN,
}

_L1P5_TRAVEL_PURPOSE_FIELD_STUDY = "field.study"
_L1P5_TRAVEL_PURPOSE_CONFERENCE = "conference"
_L1P5_TRAVEL_PURPOSE_SEMINAR = "seminar"
_L1P5_TRAVEL_PURPOSE_TEACHING = "teaching"
_L1P5_TRAVEL_PURPOSE_COLLABORATION = "collaboration"
_L1P5_TRAVEL_PURPOSE_VISIT = "visit"
_L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT = "research.management"
_L1P5_TRAVEL_PURPOSE_OTHER = "other"
_L1P5_TRAVEL_PURPOSE_UNKNOWN = "unknown"

L1P5_TRAVEL_PURPOSE_LABELS = [
    _L1P5_TRAVEL_PURPOSE_FIELD_STUDY,
    _L1P5_TRAVEL_PURPOSE_CONFERENCE,
    _L1P5_TRAVEL_PURPOSE_SEMINAR,
    _L1P5_TRAVEL_PURPOSE_TEACHING,
    _L1P5_TRAVEL_PURPOSE_COLLABORATION,
    _L1P5_TRAVEL_PURPOSE_VISIT,
    _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT,
    _L1P5_TRAVEL_PURPOSE_OTHER,
    _L1P5_TRAVEL_PURPOSE_UNKNOWN,
]

L1P5_TRAVEL_PURPOSE_DICTIONARY = {
    "field study": _L1P5_TRAVEL_PURPOSE_FIELD_STUDY,
    "etude de terrain": _L1P5_TRAVEL_PURPOSE_FIELD_STUDY,
    "etude terrain": _L1P5_TRAVEL_PURPOSE_FIELD_STUDY,
    "colloque-congres": _L1P5_TRAVEL_PURPOSE_CONFERENCE,
    "colloque": _L1P5_TRAVEL_PURPOSE_CONFERENCE,
    "congres": _L1P5_TRAVEL_PURPOSE_CONFERENCE,
    "seminaire": _L1P5_TRAVEL_PURPOSE_SEMINAR,
    "enseignement": _L1P5_TRAVEL_PURPOSE_TEACHING,
    "visite": _L1P5_TRAVEL_PURPOSE_VISIT,
    "research management": _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT,
    "administration de la recherche": _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT,
    "administration": _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT,
    "autre": _L1P5_TRAVEL_PURPOSE_OTHER,
    "": _L1P5_TRAVEL_PURPOSE_UNKNOWN,
    # geslab specific
    "Acq nouv connaissances&techniq": _L1P5_TRAVEL_PURPOSE_SEMINAR,
    "Administration de la recherche": _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT,
    "Autres": _L1P5_TRAVEL_PURPOSE_OTHER,
    "Colloques et congrés": _L1P5_TRAVEL_PURPOSE_CONFERENCE,
    "Enseignement dispensé": _L1P5_TRAVEL_PURPOSE_TEACHING,
    "Rech. doc. ou sur terrain": _L1P5_TRAVEL_PURPOSE_FIELD_STUDY,
    "Rech. en équipe, collaboration": _L1P5_TRAVEL_PURPOSE_COLLABORATION,
    "Visite, contact pour projet": _L1P5_TRAVEL_PURPOSE_VISIT,
}


# Specific translation to send to the frontend
L1P5_I18N = {
    "en": {
        _L1P5_PT_RESEARCHER: "Researchers",
        _L1P5_PT_TEACHER: "Professors",
        _L1P5_PT_SUPPORT: "Engineers",
        _L1P5_PT_STUDENTPOSTDOC: "Phds or Post-Docs",
        # survey
        _L1P5_CF_RESEARCHERTEACHER: "a researcher or a professor",
        _L1P5_CF_RESEARCHERTEACHER + ".short": "Professors / Researchers",
        _L1P5_CF_SUPPORT: "an engineer, a technician, an assistant engineer, research support staff",
        _L1P5_CF_SUPPORT + ".short": "ITA",
        _L1P5_CF_STUDENTPOSTDOC: "a PhD student or a Post-Doc",
        _L1P5_CF_STUDENTPOSTDOC + ".short": "PhD / Post-Doc",
        # travel position labels
        _L1P5_TRAVEL_GUEST: "Guest",
        _L1P5_TRAVEL_RESEARCHER: "Researcher or professor",
        _L1P5_TRAVEL_SUPPORT: "ITA",
        _L1P5_TRAVEL_STUDENTPOSTDOC: "PhD student or Post-Doc",
        _L1P5_TRAVEL_UNKNOWN: "unknown",
        # travel purpose labels
        _L1P5_TRAVEL_PURPOSE_FIELD_STUDY: "Field study",
        _L1P5_TRAVEL_PURPOSE_CONFERENCE: "Conference",
        _L1P5_TRAVEL_PURPOSE_SEMINAR: "Seminar",
        _L1P5_TRAVEL_PURPOSE_TEACHING: "Teaching",
        _L1P5_TRAVEL_PURPOSE_COLLABORATION: "Collaboration",
        _L1P5_TRAVEL_PURPOSE_VISIT: "Visit",
        _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT: "Research management",
        _L1P5_TRAVEL_PURPOSE_OTHER: "Other",
        _L1P5_TRAVEL_PURPOSE_UNKNOWN: "Unknown",
    },
    "fr": {
        _L1P5_PT_RESEARCHER: "Chercheurs",
        _L1P5_PT_TEACHER: "Enseignants-Ch.",
        _L1P5_PT_SUPPORT: "ITA",
        _L1P5_PT_STUDENTPOSTDOC: "Doctorants/Post-Docs",
        # survey
        _L1P5_CF_RESEARCHERTEACHER: "chercheur.e ou enseignant.e chercheur.e",
        f"{_L1P5_CF_RESEARCHERTEACHER}.short": "Enseignant.e / Chercheur.e",
        _L1P5_CF_SUPPORT: "ingénieur.e, technicien.ne, assistant.e ingénieur.e, personnel d'appui à la recherche",
        _L1P5_CF_SUPPORT + ".short": "ITA",
        _L1P5_CF_STUDENTPOSTDOC: "doctorant.e ou postdoctorant.e",
        _L1P5_CF_STUDENTPOSTDOC + ".short": "Doc / Post-Doc",
        # travel position labels
        _L1P5_TRAVEL_GUEST: "Personne invitée",
        _L1P5_TRAVEL_RESEARCHER: "Chercheur.e-EC",
        _L1P5_TRAVEL_SUPPORT: "ITA",
        _L1P5_TRAVEL_STUDENTPOSTDOC: "Doctorant ou Post-Doc",
        _L1P5_TRAVEL_UNKNOWN: "Inconnu",
        # travel purpose labels
        _L1P5_TRAVEL_PURPOSE_FIELD_STUDY: "Etude terrain",
        _L1P5_TRAVEL_PURPOSE_CONFERENCE: "Colloque-Congrès",
        _L1P5_TRAVEL_PURPOSE_SEMINAR: "Séminaire",
        _L1P5_TRAVEL_PURPOSE_TEACHING: "Enseignement",
        _L1P5_TRAVEL_PURPOSE_COLLABORATION: "Collaboration",
        _L1P5_TRAVEL_PURPOSE_VISIT: "Visite",
        _L1P5_TRAVEL_PURPOSE_RESEARCH_MANAGEMENT: "Administration de la recherche",
        _L1P5_TRAVEL_PURPOSE_OTHER: "Autre",
        _L1P5_TRAVEL_PURPOSE_UNKNOWN: "Inconnu",
    },
}
