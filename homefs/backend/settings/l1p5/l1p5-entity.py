#
# This is the L1P5 application configuration
#
# you can configure the position titles used Boundary, Survey and Travel
# Mandatory keys are marked like this: (MANDATORY)
#
# As you need to translate all of these you can define here your custom translation
# that will be sent back to the frontend.

# (Mandatory) The URL of the log (supported string are those supported by the
# HTML img.src attribute)
L1P5_LOGO_URL = "/static/img/labos1p5_logo_white.png"

# (Mandatory) List of module names to activate
# name is made of the type / thecollection method
L1P5_ACTIVE_MODULES = [
    "buildings/form",
    "purchases/form",
    "devices/form",
    "vehicles/form",
    "travels/form",
    "commutes/survey",
    "foods/survey",
    "ractivities/form",
]


# (Mandatory) Mapping between module type and color
L1P5_MODULES_COLOR = {
    "water": "#931c63",
    "construction": "#af4284",
    "heatings": "#c84896",
    "refrigerants": "#fdc2e5",
    "vehicles": "#fd8e62",
    "travels": "#67c3a5",
    "commutes": "#8ea0cc",
    "devices": "#a6d953",
    "purchases": "#ffda2c",
    "ractivities": "#bf4040",
}


# (Mandatory) Which username to use for geoname service
L1P5_GEONAMES_USERNAME = "labos1point5"

# (Mandatory) Which country the entity is located in
L1P5_ALLOWED_COUNTRIES = [
    "MF",
    "BL",
    "PF",
    "YT",
    "GF",
    "PM",
    "RE",
    "GP",
    "MQ",
    "WF",
    "TF",
    "NC",
    "FR",
]

# (Mandatory)
L1P5_ENTITY_CLASS = "Entity"

L1P5_NUMBER_OF_WORKED_WEEKS = {"default": 41, 2020: 27, 2021: 37}

L1P5_FILE_FORMATS = ["ges1p5", "geslab", "havas", "sifac"]

L1P5_ACCEPTED_MIME_TYPES = "application/pdf,image/*,.lss"


## Boundary: the legacy 4 categories of the french version
_L1P5_PT_MEMBER = "pt.member"

## Survey: the legacy 3 categories used in the survey of the french version
_L1P5_CF_MEMBER = "cf.member"


## Travel: the legacy 5 categories used in the travel model
_L1P5_TRAVEL_GUEST = "travel.guest"
_L1P5_TRAVEL_MEMBER = "travel.member"

# this label is special
_L1P5_TRAVEL_UNKNOWN = "travel.unknown"


# (MANDATORY) position titles name to use
L1P5_POSITION_TITLES = [
    {"name": _L1P5_PT_MEMBER, "quotity": 1},
]


# The following are the mandatory values that will be read and sent to the frontend

# (MANDATORY) survey might use a superset of labels
# this mapping is used to correctly computed the number of people in each group
L1P5_CF_LABEL_MAPPING = {
    _L1P5_PT_MEMBER: _L1P5_CF_MEMBER,
}

# (MANDATORY) travels uses it's own labels
# NOTE(msimonin): There's a remaining convention with the frontend regarding the UNKNOWN="unknown" label
L1P5_TRAVEL_POSITION_LABELS = [
    _L1P5_TRAVEL_GUEST,
    _L1P5_TRAVEL_MEMBER,
    _L1P5_TRAVEL_UNKNOWN,
]

# (MANDATORY) mapping from string to travels position labels
# By default the identity is generated but this can be augmented here
L1P5_TRAVEL_POSITION_DICTIONARY = {
    "personne invitee": _L1P5_TRAVEL_GUEST,
    "invited person": _L1P5_TRAVEL_GUEST,
    "membre": _L1P5_TRAVEL_MEMBER,
    "member": _L1P5_TRAVEL_MEMBER,
    "unknown": _L1P5_TRAVEL_UNKNOWN,
}


# a single purpose :)
_L1P5_TRAVEL_PURPOSE_UNKNOWN = "unknown"
L1P5_TRAVEL_PURPOSE_LABELS = [
    _L1P5_TRAVEL_PURPOSE_UNKNOWN,
]


L1P5_TRAVEL_PURPOSE_DICTIONARY = {
    _L1P5_TRAVEL_PURPOSE_UNKNOWN: _L1P5_TRAVEL_PURPOSE_UNKNOWN,
}


# Specific translation to send to the frontend
L1P5_I18N = {
    "en": {
        _L1P5_PT_MEMBER: "Members",
        # survey
        _L1P5_CF_MEMBER: "a member",
        _L1P5_CF_MEMBER + ".short": "Member",
        # travel position labels
        _L1P5_TRAVEL_GUEST: "Guest",
        _L1P5_TRAVEL_MEMBER: "Member",
        _L1P5_TRAVEL_UNKNOWN: "unknown",
        # travel purpose labels
        _L1P5_TRAVEL_PURPOSE_UNKNOWN: "Unknown",
    },
    "fr": {
        _L1P5_PT_MEMBER: "Member",
        # survey
        _L1P5_CF_MEMBER: "membre",
        f"{_L1P5_CF_MEMBER}.short": "membre",
        # travel position labels
        _L1P5_TRAVEL_GUEST: "Personne invitée",
        _L1P5_TRAVEL_MEMBER: "Membre",
        _L1P5_TRAVEL_UNKNOWN: "Inconnu",
        # travel purpose labels
        _L1P5_TRAVEL_PURPOSE_UNKNOWN: "Inconnu",
    },
}
