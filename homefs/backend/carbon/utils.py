import copy
from pathlib import Path
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q, Count

from backend.carbon.models import (
    GHGI,
    Building,
    ComputerDevice,
    Electricity,
    Heating,
    Purchase,
    Refrigerant,
    ResearchActivity,
    SelfConsumption,
    SurveyAnswer,
    Travel,
    TravelNames,
    TravelSection,
    Vehicle,
    VehicleConsumption,
    Water,
)
from backend.carbon.serializers import (
    BuildingSerializer,
    CommuteSerializer,
    ComputerDeviceSerializer,
    FoodSerializer,
    GHGISerializer,
    PurchaseSerializer,
    ResearchActivitySerializer,
    TravelSerializer,
    VehicleSerializer,
)
from backend.core.models.core import Tag, TaggedItem


HERE = Path(__file__).parent


def remove_keys(c, keys):
    """Copy a dictionary and remove some keys"""
    return {k: v for k, v in c.items() if k not in keys}


def do_save_buildings(entity, ghgi, buildings):
    all_electricities_id = []
    all_heating_ids = []
    all_refrigerants_ids = []
    all_waters_id = []
    for building_data in buildings:
        building_id = building_data.pop("id")
        building_data["entity"] = entity
        # Remove consumption data from building data to be able to create building
        electricity_data = building_data.pop("electricity")
        heatings_data = building_data.pop("heatings")
        refrigerants = building_data.pop("refrigerants")
        water_data = building_data.pop("water")
        selfconsumption_data = building_data.pop("selfConsumption")
        tags = building_data.pop("tags")
        # Create building if not exists
        building, created = Building.objects.update_or_create(
            id=building_id, defaults=building_data
        )
        # Electricity
        electricity_id = electricity_data.pop("id")
        electricity_data["ghgi"] = ghgi
        electricity_data["building"] = building
        electricity, created = Electricity.objects.update_or_create(
            id=electricity_id, defaults=electricity_data
        )
        all_electricities_id.append(electricity.id)
        # Heatings
        for heating_data in heatings_data:
            heating_id = heating_data.pop("id")
            heating_data["ghgi"] = ghgi
            heating_data["building"] = building
            heating, created = Heating.objects.update_or_create(
                id=heating_id, defaults=heating_data
            )
            all_heating_ids.append(heating.id)
        # Refrigerant gas
        for refrigerant_data in refrigerants:
            refrigerant_id = refrigerant_data.pop("id")
            refrigerant_data["ghgi"] = ghgi
            refrigerant_data["building"] = building
            refrigerant, created = Refrigerant.objects.update_or_create(
                id=refrigerant_id, defaults=refrigerant_data
            )
            all_refrigerants_ids.append(refrigerant.id)
        # Water
        water_id = water_data.pop("id")
        water_data["ghgi"] = ghgi
        water_data["building"] = building
        water, created = Water.objects.update_or_create(
            id=water_id, defaults=water_data
        )
        all_waters_id.append(water.id)
        # Selfconsumption
        SelfConsumption.objects.filter(building=building, ghgi=ghgi).delete()
        selfconsumption = SelfConsumption.objects.create(
            building=building, ghgi=ghgi, total=selfconsumption_data
        )
        # Tags
        TaggedItem.objects.filter(object_id=building.id).delete()
        for tag in tags:
            t = Tag.objects.get(id=tag["id"])
            TaggedItem.objects.create(content_object=building, tag=t)
    # Remove refrigerants and heatings that are no longer associated with the building and ghgi
    refrigerants = Refrigerant.objects.filter(ghgi=ghgi)
    for refrigerant in refrigerants:
        if not refrigerant.id in all_refrigerants_ids:
            refrigerant.delete()
    heatings = Heating.objects.filter(ghgi=ghgi)
    for heating in heatings:
        if not heating.id in all_heating_ids:
            heating.delete()
    # Remove electricity consumptions no longer present in the data being saved for the given ghgi
    electricities = Electricity.objects.filter(ghgi_id=ghgi.id)
    for electricity in electricities:
        if not electricity.id in all_electricities_id:
            electricity.delete()
    # Remove water consumptions no longer present in the data being saved for the given ghgi
    waters = Water.objects.filter(ghgi_id=ghgi.id)
    for water in waters:
        if not water.id in all_waters_id:
            water.delete()
    # Remove buildings no longer linked to a consumption thus to a given ghgi
    buildings = Building.objects.filter(object_id=entity.id)
    for building in buildings:
        electricity = Electricity.objects.filter(building_id=building.id)
        water = Water.objects.filter(building_id=building.id)
        heating = Heating.objects.filter(building_id=building.id)
        if len(electricity) == 0 and len(heating) == 0 and len(water) == 0:
            building.delete()


def serialize_buildings(ghgi_id):
    buildings = (
        Building.objects.prefetch_related(
            "heatings",
            "electricity",
            "water",
            "refrigerants",
            "selfConsumption",
            "tags",
        )
        .filter(
            Q(electricity__ghgi_id=ghgi_id)
            | Q(heatings__ghgi_id=ghgi_id)
            | Q(refrigerants__ghgi_id=ghgi_id)
        )
        .distinct()
        .order_by("name")
    )
    buildings_data = BuildingSerializer(buildings, many=True).data
    for building_data in buildings_data:
        building_data["heatings"] = filter(
            lambda heating: heating["ghgi"] == ghgi_id, building_data["heatings"]
        )
        filtered_electricity = list(
            filter(
                lambda electricity: electricity["ghgi"] == ghgi_id,
                building_data["electricity"],
            )
        )
        if len(filtered_electricity) > 0:
            building_data["electricity"] = filtered_electricity[0]
        building_data["refrigerants"] = filter(
            lambda refrigerant: refrigerant["ghgi"] == ghgi_id,
            building_data["refrigerants"],
        )
        filtered_water = list(
            filter(lambda water: water["ghgi"] == ghgi_id, building_data["water"])
        )
        if len(filtered_water) > 0:
            building_data["water"] = filtered_water[0]
        filtered_selfconsumption = building_data["selfConsumption"] = list(
            filter(
                lambda sconsumption: sconsumption["ghgi"] == ghgi_id,
                building_data["selfConsumption"],
            )
        )
        building_data["selfConsumption"] = None
        if len(filtered_selfconsumption) > 0:
            building_data["selfConsumption"] = filtered_selfconsumption[0]["total"]
    return buildings_data


def serialize_commutes(ghgi_id):
    commutes = (
        SurveyAnswer.objects.prefetch_related("sections", "meals", "tags")
        .filter(ghgi_id=ghgi_id)
        .annotate(nsections=Count("sections"))
        .filter(Q(nsections__gt=0) | Q(nWorkingDay=0))
        .order_by("seqID")
    )
    s = CommuteSerializer(commutes, many=True)
    return s.data


def do_save_computer_devices(ghgi, devices):
    ComputerDevice.objects.filter(ghgi_id=ghgi.id).delete()
    # ajouter les nouveaux
    for computer_device_data in devices:
        tags = computer_device_data.pop("tags", [])
        c = remove_keys(computer_device_data, ["ghgi", "tags"])
        c["ghgi_id"] = ghgi.id
        device = ComputerDevice.objects.create(**c)
        for tag in tags:
            t = Tag.objects.get(id=tag["id"])
            TaggedItem.objects.create(content_object=device, tag=t)


def serialize_devices(ghgi_id):
    computer_devices = (
        ComputerDevice.objects.prefetch_related("tags")
        .filter(ghgi_id=ghgi_id)
        .order_by("id")
    )
    s = ComputerDeviceSerializer(computer_devices, many=True)
    return s.data


def serialize_foods(ghgi_id):
    foods = (
        SurveyAnswer.objects.prefetch_related("sections", "meals", "tags")
        .filter(ghgi_id=ghgi_id)
        .annotate(nmeals=Count("meals"))
        .filter(Q(nmeals__gt=0) | Q(nWorkingDay=0))
    ).order_by("seqID")
    s = FoodSerializer(foods, many=True)
    return s.data


def do_save_purchases(ghgi, purchases):
    Purchase.objects.filter(ghgi_id=ghgi.id).delete()
    for purchase_data in purchases:
        tags = purchase_data.get("tags", [])
        p = remove_keys(purchase_data, ["ghgi", "tags"])
        p["ghgi_id"] = ghgi.id
        purchase = Purchase.objects.create(**p)
        for tag in tags:
            t = Tag.objects.get(id=tag["id"])
            TaggedItem.objects.create(content_object=purchase, tag=t)


def serialize_purchases(ghgi_id):
    purchases = (
        Purchase.objects.prefetch_related("tags")
        .filter(ghgi_id=ghgi_id)
        .order_by("code")
    )
    s = PurchaseSerializer(purchases, many=True)
    return s.data


def serialize_ractivities(ghgi_id):
    ractivities = (
        ResearchActivity.objects.prefetch_related("tags")
        .filter(ghgi_id=ghgi_id)
        .order_by("id")
    )
    s = ResearchActivitySerializer(ractivities, many=True)
    return s.data


def make_travel(ghgi, data_travel):
    tags = data_travel.get("tags", [])
    data = remove_keys(data_travel, ["sections", "names", "ghgi", "tags"])
    data["ghgi_id"] = ghgi.id
    travel = Travel.objects.create(**data)
    for tag in tags:
        t = Tag.objects.get(id=tag["id"])
        TaggedItem.objects.create(content_object=travel, tag=t)
    return travel


def make_sections(travel, data):
    data_sections = data["sections"]
    sections = [
        TravelSection(
            travel=travel,
            distance=section["distance"],
            transportation=section["transportation"],
            carpooling=section["carpooling"],
            type=section["type"],
            isRoundTrip=section["isRoundTrip"],
            departure_city=section.get("departure_city"),
            departure_latitude=section.get("departure_latitude"),
            departure_longitude=section.get("departure_longitude"),
            departure_country=section.get("departure_country"),
            destination_city=section.get("destination_city"),
            destination_latitude=section.get("destination_latitude"),
            destination_longitude=section.get("destination_longitude"),
            destination_country=section.get("destination_country"),
        )
        for section in data_sections
    ]
    return sections


def make_names(travel, data):
    data_names = data["names"]
    names = [TravelNames(travel=travel, name=name) for name in data_names]
    return names


def do_save_travels(ghgi, travels):

    # Delete all travels
    Travel.objects.filter(ghgi_id=ghgi.id).delete()

    # build / save all the travels
    all_travels = []
    for d in travels:
        all_travels.append(make_travel(ghgi, d))

    # build all sections
    all_sections = []
    for (t, d) in zip(all_travels, travels):
        all_sections.extend(make_sections(t, d))
    TravelSection.objects.bulk_create(all_sections)

    # build names
    all_names = []
    for (t, d) in zip(all_travels, travels):
        all_names.extend(make_names(t, d))
    TravelNames.objects.bulk_create(all_names)


def serialize_travels(ghgi_id):
    travels = (
        Travel.objects.prefetch_related("names", "sections", "tags")
        .filter(ghgi_id=ghgi_id)
        .order_by("id")
    )
    s = TravelSerializer(travels, many=True)
    return s.data


def do_save_vehicles(entity, ghgi, vehicles):
    ghgi_consumptions = []
    for vehicle_data in vehicles:
        vehicle_id = vehicle_data.pop("id")
        consumption_data = vehicle_data.pop("consumption")
        consumption_id = consumption_data.pop("id")
        tags = vehicle_data.pop("tags")
        vehicle_data["entity"] = entity
        vehicle, created = Vehicle.objects.update_or_create(
            id=vehicle_id, defaults=vehicle_data
        )
        consumption_data["ghgi"] = ghgi
        consumption_data["vehicle"] = vehicle
        consumption, created = VehicleConsumption.objects.update_or_create(
            id=consumption_id, defaults=consumption_data
        )
        TaggedItem.objects.filter(object_id=vehicle.id).delete()
        for tag in tags:
            t = Tag.objects.get(id=tag["id"])
            TaggedItem.objects.create(content_object=vehicle, tag=t)
        ghgi_consumptions.append(consumption.id)
    # Delete consumptions no longer defined
    consumptions = VehicleConsumption.objects.filter(ghgi_id=ghgi.id)
    for consumption in consumptions:
        if not consumption.id in ghgi_consumptions:
            consumption.delete()
    # Delete vehicles with no consumption
    vehicles = Vehicle.objects.filter(object_id=entity.id)
    for vehicle in vehicles:
        consumptions = VehicleConsumption.objects.filter(vehicle_id=vehicle.id)
        if len(consumptions) == 0:
            vehicle.delete()


def serialize_vehicles(ghgi_id):
    vehicles = (
        Vehicle.objects.prefetch_related("consumption", "tags")
        .filter(consumption__ghgi_id=ghgi_id)
        .distinct()
        .order_by("name")
    )
    vehicles_data = VehicleSerializer(vehicles, many=True).data
    for vehicle_data in vehicles_data:
        filtered_vehicle = vehicle_data["consumption"] = list(
            filter(
                lambda vehicle: vehicle["ghgi"] == ghgi_id,
                vehicle_data["consumption"],
            )
        )
        if len(filtered_vehicle) > 0:
            vehicle_data["consumption"] = filtered_vehicle[0]
    return vehicles_data


def get_ghgi_consumption_data(ghgi_id, module=None):
    ghgi = GHGI.objects.get(id=ghgi_id)
    to_return = {
        "vehicles": [],
        "buildings": [],
        "commutes": [],
        "travels": [],
        "devices": [],
        "purchases": [],
        "foods": [],
        "ractivities": [],
    }

    if module == "buildings" or module is None:
        # add buildings and modify serializer representation from many consumptions to one consumption
        data = serialize_buildings(ghgi_id)
        to_return["buildings"] = data

    if module == "vehicles" or module is None:
        # add vehicles and modify serializer representation from many consumptions to one consumption
        data = serialize_vehicles(ghgi_id)
        to_return["vehicles"] = data

    if module == "commutes" or module is None:
        # add commutes considering if its a clone survey
        if ghgi.surveyCloneYear is None:
            data = serialize_commutes(ghgi_id)
            to_return["commutes"] = data
        else:
            try:
                ghgiSurvey = GHGI.objects.get(
                    year=ghgi.surveyCloneYear,
                    object_id=ghgi.entity.id,
                    content_type=ContentType.objects.get_for_model(ghgi.entity).id,
                )
                data = serialize_commutes(ghgiSurvey.id)
                to_return["commutes"] = data
            except GHGI.DoesNotExist as exc:
                # TODO : modify year for the ghgi id
                # if no GHGI linked found, remove it
                GHGI.objects.filter(id=ghgi_id).update(surveyCloneYear=None)

    if module == "foods" or module is None:
        # add foods considering if its a clone survey
        if ghgi.surveyCloneYear is None:
            data = serialize_foods(ghgi_id)
            to_return["foods"] = data
        else:
            try:
                ghgiSurvey = GHGI.objects.get(
                    year=ghgi.surveyCloneYear,
                    object_id=ghgi.entity.id,
                    content_type=ContentType.objects.get_for_model(ghgi.entity).id,
                )
                data = serialize_foods(ghgiSurvey.id)
                to_return["foods"] = data
            except GHGI.DoesNotExist as exc:
                # TODO : modify year for the ghgi id
                # if no GHGI linked found, remove it
                GHGI.objects.filter(id=ghgi_id).update(surveyCloneYear=None)

    if module == "travels" or module is None:
        # add travels
        data = serialize_travels(ghgi_id)
        to_return["travels"] = data

    if module == "devices" or module is None:
        data = serialize_devices(ghgi_id)
        to_return["devices"] = data

    if module == "purchases" or module is None:
        data = serialize_purchases(ghgi_id)
        to_return["purchases"] = data

    # add research activities
    data = serialize_ractivities(ghgi_id)
    to_return["ractivities"] = data

    return to_return


def serialize_ghgi_with_consumptions(ghgi):
    all_data = get_ghgi_consumption_data(ghgi.id)
    s = GHGISerializer(ghgi)
    response = copy.deepcopy(s.data)
    for module in all_data.keys():
        response[module] = copy.deepcopy(all_data[module])

    return response
