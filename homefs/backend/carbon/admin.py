from types import BuiltinMethodType
from django.contrib import admin
from .models import GHGI, SurveyAnswer, Vehicle, Building, Heating, TravelSection


@admin.register(GHGI, SurveyAnswer, Building, Heating)
class GHGIAdmin(admin.ModelAdmin):
    pass


@admin.register(TravelSection)
class TravelSectionAdmin(admin.ModelAdmin):
    pass
