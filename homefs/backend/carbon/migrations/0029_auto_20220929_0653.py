# Generated by Django 3.2.7 on 2022-09-29 06:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carbon', '0028_auto_20220921_1332'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='consommation',
            name='batimentChauffage',
        ),
        migrations.RemoveField(
            model_name='consommation',
            name='batimentElectricite',
        ),
        migrations.RemoveField(
            model_name='consommation',
            name='bges',
        ),
        migrations.RemoveField(
            model_name='gazrefrigerant',
            name='batimentGazRefrigerant',
        ),
        migrations.RemoveField(
            model_name='gazrefrigerant',
            name='bges',
        ),
        migrations.RemoveField(
            model_name='selfconsumption',
            name='building',
        ),
        migrations.RemoveField(
            model_name='selfconsumption',
            name='ghgi',
        ),
        migrations.DeleteModel(
            name='Batiment',
        ),
        migrations.DeleteModel(
            name='Consommation',
        ),
        migrations.DeleteModel(
            name='GazRefrigerant',
        ),
        migrations.DeleteModel(
            name='SelfConsumption',
        ),
    ]
