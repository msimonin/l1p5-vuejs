# Generated by Django 3.2.7 on 2023-05-15 20:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carbon', '0040_auto_20230511_0555'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ghgi',
            old_name='heatingCarbonIntensity',
            new_name='heatingsCarbonIntensity',
        ),
    ]
