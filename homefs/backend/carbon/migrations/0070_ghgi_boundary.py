# Generated by Django 4.2.14 on 2024-10-15 07:46

from django.db import migrations, models
import django.db.models.deletion
from django.db.models.functions import Concat
from django.db.models import Value, F


def copy_boundary(apps, schema_director):
    GHGI = apps.get_model("carbon", "GHGI")
    PositionTitle = apps.get_model("core", "PositionTitle")
    Members = apps.get_model("core", "Members")

    Boundary = apps.get_model("core", "Boundary")
    ContentType = apps.get_model("contenttypes", "contenttype")

    LaboratoryBoundary = apps.get_model("core", "LaboratoryBoundary")
    lab_content_type = ContentType.objects.get_for_model(LaboratoryBoundary)

    # /!\ We force the position title in (pt.researcher, pt.teacher, pt.support, pt.student.postdoc)
    # to fit with the french lab use case here
    # FIXME(msimonin): do it for the other entities (Entity only)
    # we hardcode the target labels here to be self-contained

    lab_ghgi = GHGI.objects.filter(boundary_type=lab_content_type.id)

    if lab_ghgi.count() == 0:
        # nothing to do for lab
        # rationale: don't create the position title for lab if that's not needed
        return 
    
    # need to migrate the data
    researcher, _ = PositionTitle.objects.update_or_create(name="pt.researcher", defaults=dict(quotity=1))
    teacher, _ = PositionTitle.objects.update_or_create(name="pt.teacher", defaults=dict(quotity=0.5))
    support, _ = PositionTitle.objects.update_or_create(name="pt.support", defaults=dict(quotity=1))
    student_postdoc, _ = PositionTitle.objects.update_or_create(name="pt.student-postdoc", defaults=dict(quotity=1))

    for ghgi in GHGI.objects.filter(boundary_type=lab_content_type.id):

        old_boundary = LaboratoryBoundary.objects.get(id=ghgi.boundary_id)
        b = Boundary(budget=old_boundary.budget)
        b.save()
        ghgi.new_boundary = b
        ghgi.save()
        # create new boundary
        Members.objects.create(position=researcher, boundary=b, number=old_boundary.nResearcher)
        Members.objects.create(position=teacher, boundary=b, number=old_boundary.nProfessor)
        Members.objects.create(position=support, boundary=b, number=old_boundary.nEngineer)
        Members.objects.create(position=student_postdoc, boundary=b, number=old_boundary.nStudent)

    # /!\ we are also namespacing the position labels used for commute and food 
    # This way it's more clear that the position labels used in the commutes are
    # somehow decorrelated from the structure of the entity (see above position titles)
    SurveyAnswer = apps.get_model("carbon", "SurveyAnswer")
    # make the label clearer before prefixing them
    # researcher -> cf.researcher-teacher
    # engineer -> cf.support
    # student -> cf.student-postdoc
    SurveyAnswer.objects.filter(position="researcher").update(position="researcher-teacher")
    SurveyAnswer.objects.filter(position="engineer").update(position="support")
    SurveyAnswer.objects.filter(position="student").update(position="student-postdoc")
    # actually adds the prefix
    SurveyAnswer.objects.all().update(position=Concat(Value("cf."), F("position")))
    # Note that None values aren't allowed in SurveyAnswer

    # /!\ we are also namespacing the position labels used for travels
    # same reason as above

    Travel = apps.get_model("carbon", "Travel")
    # make the label clearer before prefixing them

    # researcher -> travel.researcher-teacher
    # engineer -> travel.support
    # student -> travel.student-postdoc
    # guest -> travel.guest
    # unknown -> travel.unknown
    Travel.objects.filter(status="researcher").update(status="researcher")
    Travel.objects.filter(status="engineer").update(status="support")
    Travel.objects.filter(status="student").update(status="student-postdoc")

    Travel.objects.all().update(status=Concat(Value("travel."), F("status")))
    # recover any None values
    Travel.objects.filter(status="travel.").update(status=None)


    # Makes the same for another entity if needed
    # with the assumption that only one entity has GHGIs

class Migration(migrations.Migration):

    dependencies = [
        ('carbon', '0068_travelsection_departure_city_and_more'),
        ('core', '0012_positiontitle_boundary'),
    ]

    operations = [
        migrations.AddField(
            model_name='ghgi',
            name='new_boundary',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.boundary'),
        ),
        migrations.RunPython(copy_boundary),
        
    ]
