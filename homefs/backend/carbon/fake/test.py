from abc import ABCMeta
from django.test import TestCase

from backend.test_utils import modify_conf
from backend.utils import get_entity_type, force_init
from backend.core.models.core import Entity

from . import find_good_enough_label_mapping, get_or_set_fakeGHGI


class TestLabelMapping(TestCase):
    def test_label_mapping(self):
        m = find_good_enough_label_mapping(["a"], ["a"])
        self.assertDictEqual({"a": "a"}, m)

        m = find_good_enough_label_mapping(["a", "b"], ["a"])
        self.assertDictEqual({"a": "a", "b": "a"}, m)

        m = find_good_enough_label_mapping(["a"], ["a", "b"])
        self.assertDictEqual({"a": "a"}, m)

        m = find_good_enough_label_mapping(["a"], ["b", "c"])
        self.assertDictEqual({"a": "b"}, m)

    def test_label_mapping_l1p5_aura(self):
        # match between l1p5-ish and aura-ish labels
        m = find_good_enough_label_mapping(
            ["pt.researcher", "pt.student-postdoc", "pt.teacher", "pt.support"],
            [
                "pt.researcher",
                "pt.student-postdoc",
                "pt.manager",
                "pt.administration",
                "pt.educationoutreach",
                "pt.software_eng",
                "pt.technical_eng",
                "pt.technical_sci",
            ],
        )
        self.assertDictEqual(
            {
                "pt.researcher": "pt.researcher",
                "pt.student-postdoc": "pt.student-postdoc",
                "pt.support": "pt.student-postdoc",
                "pt.teacher": "pt.researcher",
            },
            m,
        )


class TestGetOrSetFakeGHGIWrapper:
    class TestGetOrSetFakeGHGI(TestCase, metaclass=ABCMeta):
        def setUp(self):
            force_init()

        def test_idempotency(self):
            g1 = get_or_set_fakeGHGI()
            g2 = get_or_set_fakeGHGI()
            # idempotency test
            self.assertEqual(g1, g2)


@modify_conf(ENTITY_CLASS="Laboratory")
class TestGetOrSetFakeGHGILaboratory(TestGetOrSetFakeGHGIWrapper.TestGetOrSetFakeGHGI):
    ...


@modify_conf(
    ENTITY_CLASS="Entity",
    POSITION_TITLES=[{"name": "pt.test", "quotity": 1}],
    CF_LABEL_MAPPING={"pt.test": "cf.test"},
    TRAVEL_POSITION_LABELS=["travel.test"],
    TRAVEL_PURPOSE_LABELS=["purpose.test"],
)
class TestGetOrSetFakeGHGIEntity(TestGetOrSetFakeGHGIWrapper.TestGetOrSetFakeGHGI):
    def test_specific_stuffs(self):
        # explicit test
        g = get_or_set_fakeGHGI()
        # check that the entity is correct in the generated fakeGHGI
        self.assertEqual(get_entity_type(), Entity)
        # check that the labels used in position titles are correct
        pt_labels = [m.position.name for m in g.boundary.members.all()]
        self.assertCountEqual(pt_labels, ["pt.test"])
        # check that the cf labels are correct
        # all answers (meals + commutes)
        cf_labels = [a.position for a in g.answers.iterator()]
        self.assertCountEqual(["cf.test"], set(cf_labels))
        # same for travels labels + purposes
        travel_status = [t.status for t in g.travel.iterator()]
        self.assertCountEqual(["travel.test"], set(travel_status))
        travel_puposes = [t.purpose for t in g.travel.iterator()]
        self.assertCountEqual(["purpose.test"], set(travel_puposes))
