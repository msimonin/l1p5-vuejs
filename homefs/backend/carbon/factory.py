from django.apps import apps
from django.contrib.contenttypes.models import ContentType

from backend.utils import get_entity_type

from ..core.models import Settings
from .models import GHGI


def ghgi_filter(**kwargs):
    EntityClass = get_entity_type()
    return GHGI.objects.filter(
        **kwargs,
        content_type=ContentType.objects.get_for_model(EntityClass).id,
    )
