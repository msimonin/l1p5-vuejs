from rest_framework import serializers

from backend.core.serializers.factory import EntityRelatedField
from backend.core.serializers.core import BoundarySerializer, MembersSerializer
from .models import (
    GHGI,
    Electricity,
    Water,
    Heating,
    Vehicle,
    VehicleConsumption,
    Building,
    SelfConsumption,
    Refrigerant,
    CommuteSection,
    Meal,
    SurveyAnswer,
    Travel,
    TravelSection,
    ComputerDevice,
    Purchase,
    ResearchActivity,
)

from ..core.serializers import (
    TaggedItemSerializer,
    TagSerializer,
)


class HeatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Heating
        fields = "__all__"


class ElectricitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Electricity
        fields = "__all__"


class WaterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Water
        fields = "__all__"


class RefrigerantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Refrigerant
        fields = "__all__"


class SelfConsumptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SelfConsumption
        fields = "__all__"


class BuildingSerializer(serializers.ModelSerializer):
    heatings = HeatingSerializer(many=True)
    electricity = ElectricitySerializer(many=True)
    water = WaterSerializer(many=True)
    refrigerants = RefrigerantSerializer(many=True)
    selfConsumption = SelfConsumptionSerializer(many=True)
    tags = TaggedItemSerializer(many=True, read_only=True)

    class Meta:
        model = Building
        exclude = ["content_type", "object_id"]


class BuildingWCSerializer(serializers.ModelSerializer):
    class Meta:
        model = Building
        fields = "__all__"


class CommuteSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommuteSection
        fields = "__all__"


class MealSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meal
        fields = "__all__"


class CommuteSerializer(serializers.ModelSerializer):
    meals = MealSerializer(many=True, read_only=True)
    sections = CommuteSectionSerializer(many=True, read_only=True)
    tags = TaggedItemSerializer(many=True, read_only=True)

    class Meta:
        model = SurveyAnswer
        fields = "__all__"


class FoodSerializer(serializers.ModelSerializer):
    meals = MealSerializer(many=True, read_only=True)
    sections = CommuteSectionSerializer(many=True, read_only=True)
    tags = TaggedItemSerializer(many=True, read_only=True)

    class Meta:
        model = SurveyAnswer
        fields = "__all__"


class TravelSectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TravelSection
        fields = "__all__"


class TravelSerializer(serializers.ModelSerializer):
    sections = TravelSectionSerializer(many=True, read_only=True)
    names = serializers.SlugRelatedField(many=True, read_only=True, slug_field="name")
    tags = TaggedItemSerializer(many=True, read_only=True)

    class Meta:
        model = Travel
        fields = "__all__"


class VehicleConsumptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleConsumption
        fields = "__all__"


class VehicleSerializer(serializers.ModelSerializer):
    tags = TaggedItemSerializer(many=True, read_only=True)
    consumption = VehicleConsumptionSerializer(many=True)

    class Meta:
        model = Vehicle
        exclude = ["content_type", "object_id"]


class VehicleWCSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        exclude = ["content_type", "object_id"]


class PurchaseSerializer(serializers.ModelSerializer):
    tags = TaggedItemSerializer(many=True, read_only=True)

    class Meta:
        model = Purchase
        fields = "__all__"


class ComputerDeviceSerializer(serializers.ModelSerializer):
    tags = TaggedItemSerializer(many=True, read_only=True)

    class Meta:
        model = ComputerDevice
        fields = "__all__"


class ResearchActivitySerializer(serializers.ModelSerializer):
    tags = TaggedItemSerializer(many=True, read_only=True)

    class Meta:
        model = ResearchActivity
        fields = "__all__"


class GHGISerializer(serializers.ModelSerializer):
    entity = EntityRelatedField(many=False, read_only=True)
    surveyTags = TagSerializer(many=True)
    boundary = BoundarySerializer(many=False)

    class Meta:
        model = GHGI
        exclude = ["content_type", "object_id"]
