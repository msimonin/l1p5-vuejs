# utils fonction to populate the db with some predefined resources
from contextlib import contextmanager
import random
from typing import List, Tuple

from django.test import Client, TestCase, modify_settings
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status

from backend.core.models import Laboratory
from backend.users.models import L1P5Group, L1P5User
from backend.core.models.core import Entity


L1P5_USER_EMAIL = "l1p5-test@l1p5.org"
L1P5_USER_PASSWORD = "l1p5-test"
L1P5_LABORATORY = "lab-test"
L1P5_LABORATORY_LNG = 42.0
L1P5_LABORATORY_LAT = 42.0


class modify_conf(modify_settings):
    """Modify the L1P5_SETTINGS during for a test duration.

    Rationale: when running the test the default l1p5 file is read and the
    L1P5_SETTINGS variable is computed. However we want to test variation of the
    L1P5_SETTINGS: this function allows such variations by overriding some of
    settings.

    It extends the Django ``modify_settings``: https://docs.djangoproject.com/en/5.1/topics/testing/tools/#django.test.modify_settings
    """

    def __init__(self, *args, **kwargs):
        from django.conf import settings

        to_remove = []
        to_append = []
        for k, v in kwargs.items():
            # get setting
            original = [
                s for s in getattr(settings, "L1P5_SETTINGS") if s["name"] == k
            ][0]
            to_remove.append(original)
            # copy it
            s = dict(**original)
            s["value"] = v
            to_append.append(s)
        super().__init__(L1P5_SETTINGS=dict(remove=to_remove, append=to_append))


@contextmanager
def test_user_ctx():
    """Decorator that yields a fresh test user (deletes the previously created one)."""
    email = "l1p5-test@l1p5.org"
    # hardcoded :(
    password = "l1p5-test"

    # delete the user and all the associated resource in cascade
    # lab, initiatives...
    delete_user([email])
    u = ensure_user_created(email)
    yield u, email, password


def with_test_user(f):
    """Inject a fresh user (and email, and password) in the function parameters."""

    def wrapped(*args, **kwargs):
        with test_user_ctx() as (u, email, password):
            return f(u, email, password, *args, **kwargs)

    return wrapped


def delete_user(emails: List[str]):
    """Delete all the users(s) associated with the email."""
    L1P5User.objects.filter(email__in=emails).delete()


def ensure_user_created(
    email: str = L1P5_USER_EMAIL, password=L1P5_USER_PASSWORD
) -> L1P5User:
    """Ensure that the test user is created.

    Idempotent
    """
    u, _ = L1P5User.objects.get_or_create(email=email, defaults=dict(is_active=True))
    u.set_password(password)
    u.save()
    return u


def ensure_users_create(emails: List[str]) -> List[L1P5User]:
    """Create user in bulk."""
    from django.contrib.auth.base_user import make_password

    password = make_password(L1P5_USER_PASSWORD)
    objs = [
        L1P5User(email=email, is_active=True, password=password) for email in emails
    ]
    users = L1P5User.objects.bulk_create(objs)
    return users


def ensure_admin_created(email: str = L1P5_USER_EMAIL) -> L1P5User:
    """Ensure to create a super user

    Idempotent
    """
    u = ensure_user_created(email=email)
    u.is_staff = True
    u.is_superuser = True
    u.save()
    return u


def _ensure_entity_created(
    klass,
    referent: L1P5User,
    name: str = L1P5_LABORATORY,
    latitude=L1P5_LABORATORY_LAT,
    longitude=L1P5_LABORATORY_LNG,
    **kwargs,
):

    e, _ = klass.objects.get_or_create(
        name=name,
        referent=referent,
        defaults=dict(latitude=latitude, longitude=longitude, **kwargs),
    )
    return e


def ensure_entity_created(referent, **kwargs):
    return _ensure_entity_created(Entity, referent, **kwargs)


def ensure_lab_created(referent, **kwargs):
    return _ensure_entity_created(Laboratory, referent, **kwargs)


def create_test_users(
    admins_nb: int = 1,
    users_nb: int = 1,
) -> Tuple[List[L1P5User], List[L1P5User], List[L1P5User]]:
    """Creates a bunch of test users.

    l1p5-test-admin@l1p5.org: get superuser permissions
    l1p5-test-lab-XXX@l1p5.org: standard user with a lab associated and an initial GHGI

    All of these users have the same password: l1p5-test
    """
    admins = []
    for i in range(admins_nb):
        email = f"l1p5-test-admin-{i}-@l1p5.org"
        delete_user([email])
        u = ensure_admin_created(email)
        admins.append(u)

    users = []
    emails = [f"l1p5-test-{i}@l1p5.org" for i in range(users_nb)]
    delete_user(emails)
    users = ensure_users_create(emails)
    return admins, users


def make_authenticated_client(email: str) -> Tuple[L1P5User, Client]:
    user_authenticated = ensure_user_created(email)
    refresh = RefreshToken.for_user(user_authenticated)
    # in django >= 4.2 we could use the headers for that
    api_authenticated = Client(
        HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
    )
    return user_authenticated, api_authenticated


class PermissionsTestCase(TestCase):
    """Attempt to factorize permissions testing logic.

    This creates 4 api clients
    - one unauthenticated (api_unauthenticated)
    - one authenticated but doesn't own any resource (api_authenticated)
    - one authenticated that own some resources (api_user)
    - one reviewer (api_reviewer)
    - one admin (tbd) (api_admin)

    This will eventually lands in a common place and used by various apps' tests.
    """

    def setUp(self) -> None:
        # unauthenticated
        self.api_unauthenticated = Client()

        # just a user (no resource)
        self.user_authenticated, self.api_authenticated = make_authenticated_client(
            "authenticated@l1p5.org"
        )

        # a user with some resources
        self.user, self.api_user = make_authenticated_client("user@l1p5.org")

        # a reviewer
        self.user_reviewer, self.api_reviewer = make_authenticated_client(
            "reviewer@l1p5.org"
        )
        reviewer_group = L1P5Group.objects.get(name="reviewer")
        self.user_reviewer.groups.set([reviewer_group])
        # refresh permission cache
        self.user_reviewer = L1P5User.objects.get(email=self.user_reviewer.email)

        # an admin
        self.user_admin, self.api_admin = make_authenticated_client("admin@l1p5.org")
        self.user_admin.is_superuser = True
        self.user_admin.save()

    def assert_permissions(
        self,
        url,
        method="get",
        data=None,
        unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
        unauthenticated_cb=None,
        authenticated_rc=status.HTTP_403_FORBIDDEN,
        authenticated_cb=None,
        user_rc=status.HTTP_200_OK,
        user_cb=None,
        reviewer_rc=status.HTTP_200_OK,
        reviewer_cb=None,
        admin_rc=status.HTTP_200_OK,
        admin_cb=None,
        **kwargs,
    ):
        local_kwargs = dict(**kwargs)
        content_type = kwargs.pop("content_type", None)
        if content_type is None:
            local_kwargs.update(content_type="application/json")
        if data is not None:
            # don't send data if there isn't any
            local_kwargs.update(data=data)

        for (client, rc, cb, desc) in zip(
            [
                self.api_unauthenticated,
                self.api_authenticated,
                self.api_user,
                self.api_reviewer,
                self.api_admin,
            ],
            [unauthenticated_rc, authenticated_rc, user_rc, reviewer_rc, admin_rc],
            [unauthenticated_cb, authenticated_cb, user_cb, reviewer_cb, admin_cb],
            ["unauthenticated", "authenticated", "user", "reviewer", "admin"],
        ):
            if rc is None:
                # skip test
                continue
            r = getattr(client, method)(url, **local_kwargs)
            self.assertEqual(
                rc, r.status_code, f"[{desc}] {method} on {url} must returns {rc}"
            )
            if cb is not None:
                cb(self, r, dict(desc=desc, method=method, url=url))

    def tearDown(self) -> None:
        self.user_authenticated.delete()
        self.user.delete()
        self.user_reviewer.delete()
        self.user_admin.delete()
