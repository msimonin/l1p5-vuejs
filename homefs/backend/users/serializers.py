from rest_framework import serializers
from django.contrib.auth.forms import PasswordResetForm
from backend.users.models import L1P5User, L1P5Group
from backend.core.models import Settings
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.conf import settings

from ..utils import entity_get


class PasswordResetSerializer(serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """

    email = serializers.EmailField()

    def validate_email(self, value):
        # Create PasswordResetForm with the serializer
        self.reset_form = PasswordResetForm(data=self.initial_data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError(self.reset_form.errors)

        return value

    def save(self):
        request = self.context.get("request")
        # Set some values to trigger the send_email method.
        signature = Settings.get_value(name="SIGNATURE")
        opts = {
            "use_https": True,
            "from_email": getattr(settings, "DEFAULT_FROM_EMAIL"),
            "request": request,
            "extra_email_context": {
                "signature": signature,
            },
        }

        self.reset_form.save(**opts)


class L1P5UserRegistrationSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    class Meta:
        model = L1P5User
        fields = ("email", "password1", "password2", "is_active")

    def validate_email(self, email):
        if email and L1P5User.objects.filter(email=email).exists():
            raise serializers.ValidationError(
                "Un utilisateur est déjà enregistré avec cette adresse e-mail"
            )
        return email

    def validate(self, data):
        if data["password1"] != data["password2"]:
            raise serializers.ValidationError(
                "Les deux mots de passes ne sont pas identiques"
            )
        return data

    def save(self, request):
        cleaned_data = {
            "email": self.validated_data.get("email", ""),
            "password": self.validated_data.get("password1", ""),
        }
        user = L1P5User.objects.create_user(**cleaned_data)
        return user


class L1P5UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = L1P5User
        fields = ("email", "password", "is_active")


class L1P5GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = L1P5Group
        fields = "__all__"


class L1P5UserSerializer(serializers.ModelSerializer):
    groups = L1P5GroupSerializer(many=True)
    entity = serializers.SerializerMethodField()
    nghgi = serializers.SerializerMethodField()

    def get_entity(self, obj):
        try:
            entity = entity_get(referent_id=obj.id)
            return entity.name
        except:
            return None

    def get_nghgi(self, obj):
        try:
            entity = entity_get(referent_id=obj.id)
            return entity.nghgi
        except:
            return 0

    class Meta:
        model = L1P5User
        fields = (
            "email",
            "entity",
            "nghgi",
            "date_joined",
            "last_login",
            "is_superuser",
            "is_active",
            "groups",
            "roles",
        )


class L1P5TokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data["refresh"] = str(refresh)
        data["access"] = str(refresh.access_token)
        return data
