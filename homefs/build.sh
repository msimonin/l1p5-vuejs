#!/bin/bash

# Build labo1.5 website

# @author  Gerald Salin
# @author  Fabrice Jammes

set -euo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)

usage() {
    cat << EOD
Usage: $(basename "$0") [options]
Available options:
  -h            This message

Build labo1.5 website

EOD
}

while getopts h c ; do
    case $c in
        h) usage ; exit 0 ;;
        \?) usage ; exit 2 ;;
    esac
done
shift "$((OPTIND-1))"

if [ $# -ne 0 ] ; then
    usage
    exit 2
fi

echo "Build l1p5 website"
echo "Move to $DIR"
cd "$DIR"

# Uncomment for debugging purpose
# sleep infinity

echo "Install npm packages"
npm install --save-dev @types/leaflet@^1.5.7
npm i

echo "Install @vue/cli-service npm package"
npm i @vue/cli-service

echo "Build npm vue app"
npm run build

