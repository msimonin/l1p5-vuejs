module.exports = {
  outputDir: 'dist',
  assetsDir: 'static',
  // baseUrl: IS_PRODUCTION
  // ? 'http://cdn123.com'
  // : '/',
  // For Production, replace set baseUrl to CDN
  // And set the CDN origin to `yourdomain.com/static`
  // Whitenoise will serve once to CDN which will then cache
  // and distribute
  devServer: {
    proxy: {
      '/api*': {
        // Forward frontend dev server request for /api to django dev server
        target: 'http://localhost:8000/'
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: '@import "src/assets/scss/core.scss";'
      }
    }
  },
  chainWebpack: (config) => {
    config.optimization.splitChunks({
      chunks: 'all'
    })
    config.module
      .rule('i18n')
      .resourceQuery(/blockType=i18n/)
      .type('javascript/auto')
      .use('i18n')
      .loader('@kazupon/vue-i18n-loader')
      .end()
    config.resolve.alias.set('pinia$', 'pinia/index.js')
  }
}
