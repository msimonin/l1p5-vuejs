FROM debian:11.10-slim

ARG wsgi_dir="/home/labo15-au1/"


ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
EXPOSE 80
ENV PATH="${PATH}:/opt/l1p5/bin"
COPY rootfs/opt/l1p5/bin /opt/l1p5/bin

RUN install-prereq.sh

# Install node.js and npm
RUN install-node.sh

COPY rootfs /
RUN install-pydeps.sh
RUN configure-apache.sh

# make a symlink to python for quality of life
RUN ln -s $(which python3) "$(dirname $(which python3))/python"

ENV WSGI_DIR=${wsgi_dir}
COPY homefs ${wsgi_dir}

WORKDIR ${wsgi_dir}
RUN ./build.sh

CMD ["sh","-c","$WSGI_DIR/start.sh"]

# TODO for debugging purpose, comment line above and uncomment line below
#CMD ["sleep", "infinity"]

# Send apache2 logs to stderr/stdout
# FIXME unable to send error log to stderr
RUN ln -sf /proc/self/fd/1 /var/log/apache2/access.log
# RUN ln -sf /proc/self/fd/1 /var/log/apache2/access.log && \
#     ln -sf /proc/self/fd/1 /var/log/apache2/error.log
